package com.samnative.pineapple.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.samnative.pineapple.adapter.MemberRecordModel;
import com.samnative.pineapple.http.HttpRequest;
import com.samnative.pineapple.http.JsonResult;
import com.samnative.pineapple.instance.AppPrefer;
import com.samnative.pineapple.utils.GsonUtils;
import com.google.gson.reflect.TypeToken;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import com.samnative.pineapple.adapter.MyMemberAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 我的会员
 */
public class MyMemberActivity extends BaseActivity {

    private MyMemberAdapter adapter;
    private RecyclerView rlv_content;
    private TextView tv_null_data;
    private SmartRefreshLayout srl_fresh;
    private int page = 1;

    private TextView tv_text1, tv_text2;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_my_member;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        find(R.id.rl_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tv_text1 = find(R.id.tv_text1);
        tv_text2 = find(R.id.tv_text2);

        srl_fresh = find(R.id.srl_fresh);
        tv_null_data = find(R.id.tv_null_data);
        rlv_content = find(R.id.rlv_content);
        adapter = new MyMemberAdapter(R.layout.item_my_member_record);
        rlv_content.setLayoutManager(new LinearLayoutManager(mContext));
        rlv_content.setAdapter(adapter);

        srl_fresh.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                requestData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                page = 1;
                requestData();
            }
        });
        requestData();
    }


    /**
     * 请求数据
     */
    private void requestData() {
        Map<String, String> map = new HashMap<>();
        map.put("_apiname", "user.amount.flowUserServer");
        map.put("page", String.valueOf(page));
        map.put("mtoken", AppPrefer.getInstance(mContext).getString(AppPrefer.mtoken, ""));
        HttpRequest.post(this, null, map, new JsonResult() {
            @Override
            public void reqestSuccess(JSONObject result) {
                try {

                    JSONObject jsonData = result.getJSONObject("data");
                    JSONArray jsonArray = jsonData.getJSONArray("list");
                    List<MemberRecordModel> list = GsonUtils.fromJsonList(jsonArray.toString(), new TypeToken<List<MemberRecordModel>>() {
                    }.getType());

                    if (page == 1) {
                        String sum = jsonData.optString("sum");
                        String endtime = jsonData.optString("endtime");
                        tv_text1.setText(sum);
                        tv_text2.setText("有效期：" + endtime);
                        adapter.getData().clear();
                    }
                    adapter.addData(list);
                    if (list.size() > 0) {
                        page++;
                        srl_fresh.finishLoadMore();
                    } else {
                        srl_fresh.finishLoadMoreWithNoMoreData();
                    }
                    if (adapter.getData().size() > 0) {
                        tv_null_data.setVisibility(View.GONE);
                    } else {
                        tv_null_data.setVisibility(View.VISIBLE);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void requestFinish() {
                super.requestFinish();
                srl_fresh.finishRefresh();
            }
        });
    }
}
