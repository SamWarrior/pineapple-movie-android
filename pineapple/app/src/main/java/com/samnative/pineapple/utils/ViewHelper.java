package com.samnative.pineapple.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * 视图帮助类
 */
public class ViewHelper {

    /**
     * 更改View的高度(按屏幕宽度比例)
     *
     * @param v           视图对象
     * @param ratioHeight 缩放比例
     */
    public static void setHeight(Context context, View v, float ratioHeight) {
        LayoutParams lp = v.getLayoutParams();
        int height = (int) (context.getResources().getDisplayMetrics().widthPixels * ratioHeight);
        lp.height = height;
        v.setLayoutParams(lp);
    }

    /**
     * 更改View的高度(按屏幕宽度比例)
     *
     * @param v          视图对象
     * @param ratioWidth 缩放比例
     */
    public static void setWidth(Context context, View v, float ratioWidth) {
        LayoutParams lp = v.getLayoutParams();
        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * ratioWidth);
        lp.width = width;
        v.setLayoutParams(lp);
    }

    /**
     * 关闭键盘
     *
     * @param context
     * @param et
     */
    public static void hideKeyboard(Context context, EditText et) {
        InputMethodManager imm = (InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isActive()) {
            imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
        }
    }

    /**
     * 显示键盘
     *
     * @param context
     * @param et
     */
    public static void showKeyBoard(Context context, EditText et) {
        InputMethodManager imm = (InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(et, 0);
    }

    /**
     * 关闭键盘
     *
     * @param context
     * @param view
     */
    public static void hideKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isActive()) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * 显示键盘
     *
     * @param context
     */
    public static void showKeyBoard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, 0);
    }

    /**
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
     */
    public static int dp2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    /**
     * 根据手机的分辨率从 px(像素) 的单位 转成为 dp
     */
    public static int px2dp(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

    /**
     * 获取屏幕分辨率,不含虚拟按键
     *
     * @param activity
     * @return
     */
    public static Point getWindowSize(Activity activity) {
        DisplayMetrics dm = activity.getResources().getDisplayMetrics();
        Point p = new Point();
        p.x = dm.widthPixels;
        p.y = dm.heightPixels;
        return p;
    }

    /**
     * 获取真正的屏幕分辨率
     *
     * @param activity
     * @return
     */
    public static Point getWindowRealize(Activity activity) {
        DisplayMetrics metric = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getRealMetrics(metric);
        int width = metric.widthPixels; // 宽度（PX）
        int height = metric.heightPixels; // 高度（PX）
//        float density = metric.density; // 密度（0.75 / 1.0 / 1.5）
//        int densityDpi = metric.densityDpi;
        Point p = new Point();
        p.x = width;
        p.y = height;
        return p;
    }


}
