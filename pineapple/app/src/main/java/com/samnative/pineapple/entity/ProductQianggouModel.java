package com.samnative.pineapple.entity;

/**
 * 商品详情，换购model
 *
 * @author WangPeng
 */
public class ProductQianggouModel {


    private int qianggou_flag;
    private int status;
    private String nowtime;
    private String starttime;
    private String endtime;
    private String prouctprice;
    private String bullamount;
    private String productstorage = "0";
    private int limitbuy = -1;


    public int getQianggou_flag() {
        return qianggou_flag;
    }

    public void setQianggou_flag(int qianggou_flag) {
        this.qianggou_flag = qianggou_flag;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getNowtime() {
        return nowtime;
    }

    public void setNowtime(String nowtime) {
        this.nowtime = nowtime;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public String getProuctprice() {
        return prouctprice;
    }

    public void setProuctprice(String prouctprice) {
        this.prouctprice = prouctprice;
    }

    public String getBullamount() {
        return bullamount;
    }

    public void setBullamount(String bullamount) {
        this.bullamount = bullamount;
    }

    public String getProductstorage() {
        return productstorage;
    }

    public void setProductstorage(String productstorage) {
        this.productstorage = productstorage;
    }

    public int getLimitbuy() {
        return limitbuy;
    }

    public void setLimitbuy(int limitbuy) {
        this.limitbuy = limitbuy;
    }
}
