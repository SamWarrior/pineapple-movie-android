package com.samnative.pineapple.widget;

import android.app.Dialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.blankj.utilcode.util.ToastUtils;
import com.samnative.pineapple.entity.ShoppingCarModel;
import com.samnative.pineapple.main.R;
import com.samnative.pineapple.utils.ViewHelper;


public class EditBuyNumPop extends Dialog implements OnClickListener,
        TextWatcher {

    public EditBuyNumPop(Context context) {
        super(context, R.style.MyDialogStyle);
        // TODO Auto-generated constructor stub

        this.context = context;
        init();
    }

    public ShoppingCarModel.GoodsData getNumView() {
        return item;
    }

    public void setNumView(ShoppingCarModel.GoodsData item) {
        this.item = item;
    }

    public int getMaxBuyNum() {
        return maxBuyNum;
    }

    public void setMaxBuyNum(int maxBuyNum) {
        this.maxBuyNum = maxBuyNum;
    }

    public int getCurrentNum() {
        return currentNum;
    }

    public void setCurrentNum(int currentNum) {
        this.currentNum = currentNum;

        dialog_et.setText("" + currentNum);
    }

    public int getMinBuyNum() {
        return minBuyNum;
    }

    /**
     * @param minBuyNum 设置起购数
     */
    public void setMinBuyNum(int minBuyNum) {
        this.minBuyNum = minBuyNum;
    }

    private Context context;
    private ShoppingCarModel.GoodsData item;
    private View view;
    private EditText dialog_et;
    /**
     * 最大库存量
     */
    private int maxBuyNum = 1;
    /**
     * 最小购买量
     */
    private int minBuyNum = 1;
    /**
     * 当前数量
     */
    private int currentNum = 1;

    private void init() {

        view = LayoutInflater.from(context).inflate(R.layout.pop_edit_buy_num,
                null, false);
        view.findViewById(R.id.ebn_reduce).setOnClickListener(this);
        view.findViewById(R.id.ebn_plus).setOnClickListener(this);
        view.findViewById(R.id.tv_cancel).setOnClickListener(this);
        view.findViewById(R.id.tv_ok).setOnClickListener(this);
        dialog_et = (EditText) view.findViewById(R.id.dialog_et);
        dialog_et.addTextChangedListener(this);

        // setWidth(LayoutParams.MATCH_PARENT);
        // setHeight(LayoutParams.MATCH_PARENT);
        // setFocusable(true);
        // setAnimationStyle(R.style.AnimBottom);
        // setBackgroundDrawable(new ColorDrawable(0x4f000000));

        setContentView(view);

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

        switch (v.getId()) {
            case R.id.ebn_reduce:

                buyNumChange(false);

                break;

            case R.id.ebn_plus:

                buyNumChange(true);

                break;
            case R.id.tv_cancel:

                ViewHelper.hideKeyboard(context, dialog_et);
                dismiss();

                break;

            case R.id.tv_ok:
                ViewHelper.hideKeyboard(context, dialog_et);
                dismiss();
                String str = dialog_et.getText().toString();

                if (str == null || "".equals(str)) {
                    if (item != null) {
                        item.setProductnum(minBuyNum + "");
                    }
                    return;
                }

                int num = Integer.parseInt(str);
                if (item != null) {
                    item.setProductnum(num + "");
                }
                if (result != null) {
                    result.ok(num);
                }
                break;
            default:
                break;
        }

    }

    /**
     * @param isPlus 判断是否是加号 是加还是减
     */
    private void buyNumChange(boolean isPlus) {

        String str = dialog_et.getText().toString();

        if (str == null || "".equals(str)) {
            dialog_et.setText("1");
            return;
        }

        int num = Integer.parseInt(str);

        if (isPlus) {

            num++;

        } else {
            num--;

        }

        setNum1(num);
    }

    private void setNum1(int num) {

        if (num > maxBuyNum) {

            ToastUtils.showShort("限购数" + maxBuyNum  );
            dialog_et.setText("" + maxBuyNum);

        } else if (num < minBuyNum) {

            ToastUtils.showShort("起购数" + minBuyNum );
            dialog_et.setText("" + minBuyNum);

        } else if (num <= 0) {
            ToastUtils.showShort("数量不能小于1");
            dialog_et.setText("1");

        } else {
            dialog_et.setText("" + num);
        }

    }

    private void setNum(int num) {

        if (num > maxBuyNum) {

            ToastUtils.showShort("限购数" + maxBuyNum );
            dialog_et.setText("" + maxBuyNum);

            return;
        }

        if (num < minBuyNum) {
            ToastUtils.showShort("起购数" + minBuyNum );
            dialog_et.setText("" + minBuyNum);
            return;

        }

        if (num <= 0) {
            ToastUtils.showShort("数量不能小于1");
            dialog_et.setText("1");
            return;
        }

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count,
                                  int after) {
        // TODO Auto-generated method stub

        // beforeNum = dialog_et.getText().toString();

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        // TODO Auto-generated method stub

    }

    @Override
    public void afterTextChanged(Editable s) {
        // TODO Auto-generated method stub

        String str = dialog_et.getText().toString();

//        LogUtils.showLog("-------------" + str);

        if (str == null || "".equals(str)) {

            // dialog_et.setText("1");

            return;
        }

        int num = Integer.parseInt(str);
        setNum(num);
        dialog_et.setSelection(dialog_et.getText().toString().length());
    }

    /**
     * 点击确定回调
     *
     * @author WangPeng
     */
    public interface OnResult {
        public void ok(int num);
    }

    private OnResult result;

    public void setOnResult(OnResult result) {
        this.result = result;
    }

}
