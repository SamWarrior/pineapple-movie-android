package com.samnative.pineapple.entity;

public class MovieDetalCommentModel {


    /**
     * _id : 6118e2dba61b683ed4cfeee3
     * movieId : m007
     * customid : 61181279c5497532749a31c0
     * content : 使用mongoose保存当前时间到数据库时，一开始我在schema中定义默认时间的方式是下面这样子的
     * startTime : 2021-08-15 17:48:11
     * __v : 0
     * headicon : http://localhost:3030/uploads/ac5a86e62b856774127446172a722d81
     * nickname : 大大大波
     */

    private String _id;
    private String movieId;
    private String customid;
    private String content;
    private String startTime;
    private String headicon;
    private String nickname;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getMovieId() {
        return movieId;
    }

    public void setMovieId(String movieId) {
        this.movieId = movieId;
    }

    public String getCustomid() {
        return customid;
    }

    public void setCustomid(String customid) {
        this.customid = customid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }


    public String getHeadicon() {
        return headicon;
    }

    public void setHeadicon(String headicon) {
        this.headicon = headicon;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
