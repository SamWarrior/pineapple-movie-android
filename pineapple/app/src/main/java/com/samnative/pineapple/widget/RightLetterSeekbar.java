package com.samnative.pineapple.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;


import com.samnative.pineapple.utils.ViewHelper;

import java.util.ArrayList;
import java.util.List;

public class RightLetterSeekbar extends View {

    /**
     * 索引字母列表
     */
    private List<String> texts;

    /**
     * 画笔样式
     */
    private Paint paint_1;

    /**
     * 字体间距
     */
    private int TextRange;

    /**
     * 监听接口
     */
    private OnclikViewRight onclikViewRight;

    public RightLetterSeekbar(Context context, AttributeSet attrs) {
        super(context, attrs);
        Init();
    }

    public void Init() {
        texts = new ArrayList<String>();
        for (int i = 0; i < 26; i++) {
            texts.add(((char) ((int) 'A' + i)) + "");
        }

        paint_1 = new Paint();
        paint_1.setColor(0xFF1082FF);
        paint_1.setTextSize(ViewHelper.dp2px(getContext(), 12));
        paint_1.setAntiAlias(true);
    }

    /**
     * 设置自定义数据
     *
     * @param tests
     */
    public void setData(List<String> tests) {
        this.texts = tests;
        postInvalidate();
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);

        int screen_w = getWidth();
        int screen_h = getHeight();
        TextRange = screen_h / texts.size();

        for (int i = 0; i < texts.size(); i++) {
            int X = screen_w - getTextWidth(texts.get(i)) >> 1;
            canvas.drawText(texts.get(i), X, (i + 1) * TextRange, paint_1);
        }

    }

    /**
     * 触屏监听
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction()) {

            case MotionEvent.ACTION_UP:

                setBackgroundColor(0x00000000);

                if (onclikViewRight != null) {
                    onclikViewRight.seteventUP();
                }

                break;

            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_MOVE:

                setBackgroundColor(0x3f000000);

                float Y = event.getY(); // 得到当前触点

                int YY = (int) Y / TextRange; // 计算当前位置的字母

                if (YY <= 0) {
                    YY = 0;
                }

                if (YY >= texts.size()) {
                    YY = texts.size() - 1;
                }

                String str = texts.get(YY);

                if (onclikViewRight != null) {
                    onclikViewRight.seteventDownAndMove(str);
                }

                break;

        }

        return true;
    }

    /**
     * 返回字体宽度
     *
     * @param str
     * @return
     */
    public int getTextWidth(String str) {

        Rect rect = new Rect();
        paint_1.getTextBounds(str, 0, str.length(), rect);
        return rect.width();

    }

    /**
     * 设置监听
     */
    public void setOnclikViewRight(OnclikViewRight onclikViewRight) {
        this.onclikViewRight = onclikViewRight;
    }

    /**
     * 自定接口监听字母索引View
     *
     * @author Hasee
     */
    public interface OnclikViewRight {

        /**
         * 松开触屏
         */
        public void seteventUP();

        /**
         * 按下或滑动触屏传回当前索引的字母
         */
        public void seteventDownAndMove(String str);

    }

}
