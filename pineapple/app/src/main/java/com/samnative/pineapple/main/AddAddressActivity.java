package com.samnative.pineapple.main;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.samnative.pineapple.entity.UserModel;
import com.samnative.pineapple.http.HttpRequest;
import com.samnative.pineapple.http.JsonResult;
import com.samnative.pineapple.instance.AppPrefer;
import com.samnative.pineapple.utils.ViewHelper;
import com.samnative.pineapple.widget.CityPicker;
import com.google.gson.Gson;
import com.shashank.sony.fancytoastlib.FancyToast;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AddAddressActivity extends BaseActivity implements OnClickListener {

    private Context context;
    /**
     * 输入姓名
     */
    private EditText etName;

    /**
     * 输入电话
     */
    private EditText etPhone;
    /**
     * 城市选择框
     */
    private RelativeLayout address_area_layout;

    private TextView address_area_tv;// 地区显示框

//	/**
//	 * 街道输入框
//	 */
//	private EditText etStreet;

    /**
     * 输入详细地址
     */
    private EditText etAddressDetail;

    private CheckBox add_address_setderault;

    private CityPicker cityPicker;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_add_address;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        context = this;
        findViewById(R.id.tv_back).setOnClickListener(this);
        findViewById(R.id.address_area_layout).setOnClickListener(this);
        findViewById(R.id.save_tv).setOnClickListener(this);
        etName = (EditText) findViewById(R.id.et_address_name);
        etPhone = (EditText) findViewById(R.id.et_address_phone);
        address_area_layout = (RelativeLayout) findViewById(R.id.address_area_layout);
        address_area_layout.setOnClickListener(this);
        address_area_tv = (TextView) findViewById(R.id.address_area_tv);
        etAddressDetail = (EditText) findViewById(R.id.add_address_detail);
        add_address_setderault = (CheckBox) findViewById(R.id.add_address_setderault);

        cityPicker = new CityPicker(context);

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.tv_back:
                finish();
                break;
            case R.id.address_area_layout:
                ViewHelper.hideKeyboard(context, etName);
                cityPicker.showCityPicker();
                cityPicker.setOnChangeCity(new CityPicker.OnChangeCity() {
                    @Override
                    public void callCityInfo(String province, String city, String county, String areacode) {

                        f_city = "" + province + city + county;
                        address_area_tv.setText(f_city);
                        f_areacode = areacode;
                    }
                });
                break;
            case R.id.save_tv:
                addAddress();
                break;
            default:
                break;
        }
    }

    /***
     * 添加地址
     */
    private void addAddress() {
        // TODO Auto-generated method stub
        if (checkData()) {
            Map<String, String> map = new HashMap<String, String>();

            map.put("mobile", f_mobile);
            map.put("realname", "" + f_receivename);
            map.put("city_id", f_areacode);
            map.put("city", f_city);
            map.put("address", f_address);


            boolean isCheck = add_address_setderault.isChecked();

            if (isCheck) {
                map.put("isdefault", "1");
            } else {
                map.put("isdefault", "-1");
            }

            map.put("_apiname", "user.logistics.addCustomerLogistic");
            map.put("mtoken", AppPrefer.getInstance(mContext).getString(AppPrefer.mtoken, ""));
            HttpRequest.post(this, "", map, new JsonResult() {
                @Override
                public void reqestSuccess(JSONObject result) {
                    FancyToast.showToast(mContext, "添加成功", FancyToast.SUCCESS, false);
                    //因后台未做身份验证，需手动设置已认证
                    UserModel model = AppPrefer.getUserModel(context);
//                    model.getData().getUserinfo().setLogisticsDec("1");
                    AppPrefer.getInstance(context).put(AppPrefer.userinfo, new Gson().toJson(model));
                    finish();
                }
            });

        }
    }


    private String f_receivename;
    private String f_mobile;
    private String f_city;
    private String f_address;
    private String f_areacode;
//	private String f_streetcode;

    private boolean checkData() {
        // TODO Auto-generated method stub

        f_receivename = etName.getText().toString();
        f_mobile = etPhone.getText().toString();
        f_address = etAddressDetail.getText().toString();

        if (f_receivename == null || f_receivename.equals("")) {
            FancyToast.showToast(mContext, "收货人不能为空", FancyToast.WARNING, false);
            return false;
        } else if (f_mobile == null || f_mobile.equals("")) {
            FancyToast.showToast(mContext, "电话不能为空", FancyToast.WARNING, false);
            return false;
        } else if (f_city == null || f_city.equals("")) {
            FancyToast.showToast(mContext, "请选择地区", FancyToast.WARNING, false);
            return false;
        } else if (f_address == null || f_address.equals("")) {
            FancyToast.showToast(mContext, "详细地址不能为空", FancyToast.WARNING, false);
            return false;
        }

        return true;
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
    }
}
