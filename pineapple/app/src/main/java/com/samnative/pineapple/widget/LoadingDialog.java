package com.samnative.pineapple.widget;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.MultiplePulse;
import com.samnative.pineapple.main.R;


/**
 * 加载中效果
 */
public class LoadingDialog extends Dialog {

    /**
     * 上下文
     */
    private Context context;
    /**
     * 视图
     */
    private View view;
    /**
     * 显示文本
     */
    private TextView tvMessage;
    /**
     * 加载效果
     */
    private ProgressBar progressBar;

    private LinearLayout ll_bg;

    public static LoadingDialog instance;

    public static void dismissDialog() {
        if (instance != null && instance.isShowing()) {
            try {
                instance.dismiss();
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
        }
    }

    public static LoadingDialog getInstance(Context context) {

        if (instance != null && instance.isShowing()) {
            instance.dismiss();
            instance = null;
        }
        instance = new LoadingDialog(context);
        return instance;
    }

    public LoadingDialog(Context context) {
        super(context, R.style.DialogTheme);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        this.setCanceledOnTouchOutside(false);
        this.setCancelable(false);
        this.context = context;
        view = getLayoutInflater().inflate(R.layout.dialog_progress, null);
        ll_bg = view.findViewById(R.id.ll_bg);
        tvMessage = (TextView) view.findViewById(R.id.textview_message);

        progressBar = (ProgressBar) view.findViewById(R.id.spin_kit);
        Sprite sprite = new MultiplePulse();
        progressBar.setIndeterminateDrawable(sprite);

        this.setContentView(view);
    }

    public LoadingDialog setMessage(String message) {
        if (message != null) {
            tvMessage.setText(message);
        }
        return instance;
    }

    @Override
    public void show() {

        if (tvMessage.getText() == null || tvMessage.getText().toString().isEmpty()) {
            ll_bg.setBackground(null);
        } else {
            ll_bg.setBackground(ContextCompat.getDrawable(context, R.drawable.bg_loading));
        }

        if (instance != null && context != null) {
            super.show();
        } else {
            instance = null;

        }
    }

}
