package com.samnative.pineapple.instance;

import com.samnative.pineapple.entity.ShoppingCarModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 用户单例
 */
public class UserInfo {

    public static UserInfo mDeviceInfo = null;

    /**
     * 获取单例对象
     */
    public static UserInfo getInstance() {
        if (mDeviceInfo == null) {
            mDeviceInfo = new UserInfo();
        }
        return mDeviceInfo;
    }



    //=============================================================


    //=====================================================================

    /**
     * 购物车数据
     */
    private List<ShoppingCarModel> listShoppingCar;

    /**
     * 返回购物车数据
     *
     * @return
     */
    public List<ShoppingCarModel> getListShoppingCar() {
        if (listShoppingCar == null) {
            listShoppingCar = new ArrayList<ShoppingCarModel>();
        }
        return listShoppingCar;
    }


}
