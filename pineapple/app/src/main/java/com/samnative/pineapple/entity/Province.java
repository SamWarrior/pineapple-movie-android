package com.samnative.pineapple.entity;

import java.util.ArrayList;

public class Province {

	private int id;
	private String name;
	private String f_type;
	private ArrayList<City> area;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getF_type() {
		return f_type;
	}

	public void setF_type(String f_type) {
		this.f_type = f_type;
	}

	public ArrayList<City> getArea() {
		return area;
	}

	public void setArea(ArrayList<City> area) {
		this.area = area;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return name;
	}
}
