package com.samnative.pineapple.adapter;

import android.graphics.drawable.ColorDrawable;
import android.support.annotation.LayoutRes;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.samnative.pineapple.entity.BankcardModel;
import com.samnative.pineapple.main.R;
import com.makeramen.roundedimageview.RoundedImageView;


/**
 * 银行卡列表
 */
public class BankcardAdapter extends BaseQuickAdapter<BankcardModel, BaseViewHolder> {

    private int[] color = {0xffF57CA2, 0xffFC8E79, 0xff85C1CF, 0xff99A9DC};
    private int index = -1;
    private boolean manager = false;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
        notifyDataSetChanged();
    }

    public boolean isManager() {
        return manager;
    }

    public void setManager(boolean manager) {
        this.manager = manager;
        notifyDataSetChanged();
    }

    public BankcardAdapter(@LayoutRes int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, final BankcardModel item) {


        ImageView iv_bankcard_ic = helper.getView(R.id.iv_bankcard_ic);
        ImageView iv_check_bankcard = helper.getView(R.id.iv_check_bankcard);
        TextView tv_bankname = helper.getView(R.id.tv_bankname);
        TextView tv_cardtype = helper.getView(R.id.tv_cardtype);
        TextView tv_cardnum = helper.getView(R.id.tv_cardnum);
        RoundedImageView rciv_bg = helper.getView(R.id.rciv_bg);


        tv_bankname.setText(item.getBank_name());
        tv_cardtype.setText(item.getAccount_type().equals("1") ? "个人账户"
                : "对公账户");
        tv_cardnum.setText(item.getAccount_number());
        int colorPosition = helper.getAdapterPosition() % 4;
        rciv_bg
                .setImageDrawable(new ColorDrawable(color[colorPosition]));
        iv_bankcard_ic.setImageResource(getIcon(item.getBank_name()));
        if (manager) {
            iv_check_bankcard.setVisibility(View.VISIBLE);
        } else {
            iv_check_bankcard.setVisibility(View.GONE);
        }
        if (index == helper.getAdapterPosition()) {
            iv_check_bankcard.setImageResource(R.mipmap.ic_bank_card_check);
        } else {
            iv_check_bankcard.setImageResource(R.mipmap.ic_bank_card_not_check);
        }

    }


    /**
     * 根据名称返回对应图标
     *
     * @param name
     * @return
     */
    public int getIcon(String name) {

        if (name.contains("工商银行")) {
            return R.mipmap.ic_bank_gongshang;
        }
        if (name.contains("光大银行")) {
            return R.mipmap.ic_bank_guangda;
        }
        if (name.contains("建设银行")) {
            return R.mipmap.ic_bank_jianshe;
        }
        if (name.contains("交通银行")) {
            return R.mipmap.ic_bank_jiaotong;
        }
        if (name.contains("民生银行")) {
            return R.mipmap.ic_bank_minsheng;
        }
        if (name.contains("农业银行")) {
            return R.mipmap.ic_bank_nongye;
        }
        if (name.contains("平安银行")) {
            return R.mipmap.ic_bank_pingan;
        }
        if (name.contains("浦发银行")) {
            return R.mipmap.ic_bank_pufa;
        }
        if (name.contains("兴业银行")) {
            return R.mipmap.ic_bank_xingye;
        }
        if (name.contains("招商银行")) {
            return R.mipmap.ic_bank_zhaoshang;
        }
        if (name.contains("中国银行")) {
            return R.mipmap.ic_bank_zhongguo;
        }
        if (name.contains("中信银行")) {
            return R.mipmap.ic_bank_zhongxin;
        }
        if (name.contains("深圳发展银行")) {
            return R.mipmap.ic_bank_shenzhenfazhan;
        }

        return R.mipmap.ic_bank_zhongguo;
    }
}