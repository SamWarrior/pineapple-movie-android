package com.samnative.pineapple.entity;

/**
 * 消息通知
 */
public class MineNoticeModel {

    /**
     * _id : 613dbaec9d3aad160036d5e5
     * title : 董明珠笑了！格力新手机曝光，王自如加盟后首款产品！面板颜色罕见！支持5G，高通芯片加持……
     * content : 格力空调女掌门人董明珠不服输的劲儿又来了！这次体现在手机上。工信部最新信息显示，格力旗下一款新机入网，核心搭载骁龙870处理器，极为罕见的白色面板设计。早在2015年，格力便已发布第一款手机，但截至目前，格力手机仍未见显著起色，如今格力再“重拳”出击，或意在铺垫格力智能家居生态。格力新机曝光9月11日，博主@熊猫很禿然最新爆料，新款格力手机（大松子品牌）工信部入网照曝光，该手机采用挖孔直屏，前部面板和后盖中框均为白色。
     * 这款手机将搭载骁龙870处理器，配备 6.67 英寸 OLED 屏幕，采用居中挖孔方案。
     * startTime : 2021-09-12 16:31:40
     */

    private String _id;
    private String title;
    private String content;
    private String startTime;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
}
