package com.samnative.pineapple.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.samnative.pineapple.adapter.WentiListAdapter;
import com.samnative.pineapple.entity.WentiListModel;
import com.samnative.pineapple.http.HttpRequest;
import com.samnative.pineapple.http.JsonResult;
import com.samnative.pineapple.instance.AppPrefer;
import com.samnative.pineapple.utils.GsonUtils;
import com.google.gson.reflect.TypeToken;
import com.gyf.immersionbar.ImmersionBar;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 帮助说明
 */
public class HelpCenterActivity extends BaseActivity implements View.OnClickListener {

    private SmartRefreshLayout srl_fresh;
    private RecyclerView rlv_content;
    private WentiListAdapter wentiListAdapter;
    private TextView tv_null_data;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_help_center;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        ImmersionBar.with(this).statusBarDarkFont(false).init();
        find(R.id.rl_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        srl_fresh = find(R.id.srl_fresh);
        srl_fresh.setEnableLoadMore(false);
        srl_fresh.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                requestData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                requestData();
            }
        });
        tv_null_data = find(R.id.tv_null_data);
        wentiListAdapter = new WentiListAdapter(R.layout.item_wenti_list);
        wentiListAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Intent intent = new Intent(mContext, Webview2Activity.class);
                intent.putExtra("url", wentiListAdapter.getData().get(position).getUrl());
                startActivity(intent);
            }
        });
        rlv_content = find(R.id.rlv_content);
        LinearLayoutManager manager2 = new LinearLayoutManager(mContext);
        rlv_content.setLayoutManager(manager2);
        rlv_content.setAdapter(wentiListAdapter);


        requestData();
    }

    /**
     * 获取问题分类下的列表
     */
    public void requestData() {
        Map<String, String> map = new HashMap<>();
        map.put("_apiname", "sys.question.getQuestionList");
        map.put("classify_id", "18");
        map.put("mtoken", AppPrefer.getInstance(mContext).getString(AppPrefer.mtoken, ""));
        HttpRequest.post(this, "", map, new JsonResult() {
            @Override
            public void reqestSuccess(JSONObject result) {
                try {
                    JSONArray jsonArray = result.getJSONArray("data");
                    List<WentiListModel> lists = GsonUtils.fromJsonList(jsonArray.toString(), new TypeToken<List<WentiListModel>>() {
                    }.getType());
                    wentiListAdapter.getData().clear();
                    wentiListAdapter.addData(lists);
                    if (lists.size() > 0) {
                        srl_fresh.finishLoadMore();
                    } else {
                        srl_fresh.finishLoadMoreWithNoMoreData();
                    }
                    if (wentiListAdapter.getData().size() > 0) {
                        tv_null_data.setVisibility(View.GONE);
                    } else {
                        tv_null_data.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void requestFinish() {
                super.requestFinish();
                srl_fresh.finishRefresh();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                break;
        }
    }
}
