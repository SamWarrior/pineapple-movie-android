package com.samnative.pineapple.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.samnative.pineapple.adapter.MyTeamAdapter;
import com.samnative.pineapple.entity.TeamModel;
import com.samnative.pineapple.http.HttpRequest;
import com.samnative.pineapple.http.JsonResult;
import com.samnative.pineapple.instance.AppPrefer;
import com.samnative.pineapple.utils.GsonUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * 我的分享
 */
public class MyTeamActivity extends BaseActivity implements BaseQuickAdapter.OnItemClickListener {

    private MyTeamAdapter teamAdapter;
    private RecyclerView rlv_content;
    private TextView tv_null_data;
    private SmartRefreshLayout srl_fresh;
    private int page = 1;

    private TextView tv_text1, tv_text2, tv_text3, tv_text4;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_my_team;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        find(R.id.rl_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        srl_fresh = find(R.id.srl_fresh);
        tv_null_data = find(R.id.tv_null_data);
        rlv_content = find(R.id.rlv_content);
        teamAdapter = new MyTeamAdapter(R.layout.item_my_team);
        teamAdapter.setOnItemClickListener(this);
        rlv_content.setLayoutManager(new LinearLayoutManager(mContext));
        rlv_content.setAdapter(teamAdapter);

        srl_fresh.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                requestData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                page = 1;
                requestData();
            }
        });

        tv_text1 = find(R.id.tv_text1);
        tv_text2 = find(R.id.tv_text2);
        tv_text3 = find(R.id.tv_text3);
        tv_text4 = find(R.id.tv_text4);
        requestData();
    }


    /**
     * 请求数据
     */
    private void requestData() {
        Map<String, String> map = new HashMap<>();
        map.put("_apiname", "user.user.relationStatistics");
        map.put("page", String.valueOf(page));
        map.put("mtoken", AppPrefer.getInstance(mContext).getString(AppPrefer.mtoken, ""));
        HttpRequest.post(this, null, map, new JsonResult() {
            @Override
            public void reqestSuccess(JSONObject result) {
                try {
                    TeamModel model = GsonUtils.fromJson(result.toString(), TeamModel.class);
                    if (page == 1) {
                        tv_text1.setText(model.getData().getDirect_enterprise_number());
                        tv_text2.setText(model.getData().getTotal_direct_number());
                        tv_text3.setText(model.getData().getIndirect_enterprise_number());
                        tv_text4.setText(model.getData().getTotal_indirect_number());
                        teamAdapter.getData().clear();
                    }
                    teamAdapter.addData(model.getData().getList());
                    if (model.getData().getList().size() > 0) {
                        page++;
                        srl_fresh.finishLoadMore();
                    } else {
                        srl_fresh.finishLoadMoreWithNoMoreData();
                    }
                    if (teamAdapter.getData().size() > 0) {
                        tv_null_data.setVisibility(View.GONE);
                    } else {
                        tv_null_data.setVisibility(View.VISIBLE);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void requestFinish() {
                super.requestFinish();
                srl_fresh.finishRefresh();
            }
        });
    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
    }
}
