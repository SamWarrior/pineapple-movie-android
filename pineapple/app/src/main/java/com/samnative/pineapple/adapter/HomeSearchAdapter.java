package com.samnative.pineapple.adapter;

import android.support.annotation.LayoutRes;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.samnative.pineapple.entity.HomeListModel;
import com.samnative.pineapple.main.R;
import com.samnative.pineapple.utils.GlideRequestOptions;
import com.samnative.pineapple.value.Config;

/**
 * 首页-搜索列表
 */
public class HomeSearchAdapter extends BaseQuickAdapter<HomeListModel, BaseViewHolder> {

    public HomeSearchAdapter(@LayoutRes int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, final HomeListModel item) {

        ImageView image = helper.getView(R.id.image);
        TextView tv_title = helper.getView(R.id.tv_title);
        tv_title.setText(item.getMovieName());
        Glide.with(mContext)
                .load(Config._API_URL + item.getCover())//图片地址
                .apply(GlideRequestOptions.OPTIONS_400_400)
                .into(image);

    }

}
