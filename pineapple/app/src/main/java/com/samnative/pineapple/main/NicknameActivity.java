package com.samnative.pineapple.main;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.shashank.sony.fancytoastlib.FancyToast;
import com.samnative.pineapple.entity.UserModel;
import com.samnative.pineapple.http.HttpRequest;
import com.samnative.pineapple.http.JsonResult;
import com.samnative.pineapple.instance.AppPrefer;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * 修改昵称
 */
public class NicknameActivity extends BaseActivity implements OnClickListener {

    private EditText et_nickname;
    private TextView confirm_btn;
    private UserModel userModel;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_set_nickname;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        findViewById(R.id.iv_back).setOnClickListener(this);
        et_nickname = findViewById(R.id.et_nickname);
        confirm_btn = findViewById(R.id.confirm_btn);
        confirm_btn.setOnClickListener(this);
        showData();
    }

    private void showData() {
        // TODO Auto-generated method stub
        userModel = AppPrefer.getUserModel(mContext);
        et_nickname.setText(userModel.getData().getNickname());
        et_nickname.setSelection(et_nickname.getText().toString().length());
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.confirm_btn:
                if (et_nickname.getText().toString().length() < 1) {
                    FancyToast.showToast(mContext, "请输入昵称", FancyToast.WARNING, false);
                    return;
                }
                changePersonData();
                break;
            default:
                break;
        }
    }

    private void changePersonData() {
        try {
            userModel.getData().setNickname(et_nickname.getText().toString());
            JSONObject json = new JSONObject();

            String result = new Gson().toJson(userModel.getData());
            JSONObject jinfo = new JSONObject(result);

            json.put("userinfo", jinfo);

            HttpRequest.request(this, "/client/user/upuserinfo", "", String.valueOf(json), "POST", new JsonResult() {
                @Override
                public void reqestSuccess(JSONObject result) {
                    FancyToast.showToast(mContext, "修改成功", FancyToast.SUCCESS, false);
                    finish();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
