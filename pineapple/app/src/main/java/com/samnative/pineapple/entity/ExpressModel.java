package com.samnative.pineapple.entity;

public class ExpressModel {

	private int id;
	private String f_companyname;
	private int f_isarriveallarea;
	private String f_companyurl;
	private String f_kuaidi100_code;
	private String f_kuaidiliao_code;
	private String f_companyphone;
	private int f_isenable;
	private String f_remark;
	private int f_isdelete;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getF_companyname() {
		return f_companyname;
	}
	public void setF_companyname(String f_companyname) {
		this.f_companyname = f_companyname;
	}
	public int getF_isarriveallarea() {
		return f_isarriveallarea;
	}
	public void setF_isarriveallarea(int f_isarriveallarea) {
		this.f_isarriveallarea = f_isarriveallarea;
	}
	public String getF_companyurl() {
		return f_companyurl;
	}
	public void setF_companyurl(String f_companyurl) {
		this.f_companyurl = f_companyurl;
	}
	public String getF_kuaidi100_code() {
		return f_kuaidi100_code;
	}
	public void setF_kuaidi100_code(String f_kuaidi100_code) {
		this.f_kuaidi100_code = f_kuaidi100_code;
	}
	public String getF_kuaidiliao_code() {
		return f_kuaidiliao_code;
	}
	public void setF_kuaidiliao_code(String f_kuaidiliao_code) {
		this.f_kuaidiliao_code = f_kuaidiliao_code;
	}
	public String getF_companyphone() {
		return f_companyphone;
	}
	public void setF_companyphone(String f_companyphone) {
		this.f_companyphone = f_companyphone;
	}
	public int getF_isenable() {
		return f_isenable;
	}
	public void setF_isenable(int f_isenable) {
		this.f_isenable = f_isenable;
	}
	public String getF_remark() {
		return f_remark;
	}
	public void setF_remark(String f_remark) {
		this.f_remark = f_remark;
	}
	public int getF_isdelete() {
		return f_isdelete;
	}
	public void setF_isdelete(int f_isdelete) {
		this.f_isdelete = f_isdelete;
	}
	
	
	

}
