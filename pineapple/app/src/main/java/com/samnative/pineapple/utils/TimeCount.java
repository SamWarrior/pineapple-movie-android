package com.samnative.pineapple.utils;

import android.graphics.Color;
import android.os.CountDownTimer;
import android.widget.TextView;


/** 发送验证码定时器 */
public class TimeCount extends CountDownTimer {

    private ITimeCountListener m_ITimeCountListener;

    /**
     * 构造函数
     * 
     * @param millisInFuture
     *            倒计时总时间 毫秒
     * @param countDownInterval
     *            倒计时间隔时间 毫秒
     * */
    public TimeCount(long millisInFuture, long countDownInterval) {
	super(millisInFuture, countDownInterval);
    }

    /**
     * 发送验证码定时器onTick方法
     * 
     * @param m_Btn
     *            点击发送验证码按钮
     * @param p_MillisUntilFinished
     *            间隔可重新发送时间
     * */
    public void sendValidCodeOnTick(TextView m_Btn, long p_MillisUntilFinished) {
	m_Btn.setBackgroundColor(Color.parseColor("#c9caca"));
	m_Btn.setClickable(false);
	m_Btn.setText(p_MillisUntilFinished / 1000 + "秒后可重新发送");
    }

//    /**
//     * 发送验证码定时器onFinish方法
//     *
//     * @param m_Btn
//     *            点击发送验证码按钮
//     * */
//    public void sendValidCodeOnFinish(TextView m_Btn) {
//	m_Btn.setText("重新获取验证码");
//	m_Btn.setClickable(true);
//	m_Btn.setBackgroundResource(R.drawable.btn_bg_blue_stoke_n);
//    }

    /** 定时器事件监听接口注入 */
    public void setTimerCountListener(ITimeCountListener p_ITimeCountListener) {
	m_ITimeCountListener = p_ITimeCountListener;
    }

    @Override
    public void onTick(long millisUntilFinished) {
	if (m_ITimeCountListener != null) {
	    m_ITimeCountListener.onTick(millisUntilFinished);
	}
    }

    @Override
    public void onFinish() {
	if (m_ITimeCountListener != null) {
	    m_ITimeCountListener.onFinish();
	}
    }

    /** 定时器事件监听接口 */
    public interface ITimeCountListener {

	/** 间隔发生事件 */
	void onTick(long millisUntilFinished);

	/** 结束事件 */
	void onFinish();
    }

}
