package com.samnative.pineapple.adapter;

import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.samnative.pineapple.entity.MineCommentModel;
import com.samnative.pineapple.main.R;
import com.samnative.pineapple.utils.GlideRequestOptions;
import com.samnative.pineapple.value.Config;

/**
 * 我的评论列表
 */
public class MineCommentAdapter extends BaseQuickAdapter<MineCommentModel, BaseViewHolder> {

    public MineCommentAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, MineCommentModel item) {
        TextView tv_text1 = helper.getView(R.id.tv_text1);
        TextView tv_text2 = helper.getView(R.id.tv_text2);
        TextView tv_text3 = helper.getView(R.id.tv_text3);

        tv_text1.setText(item.getContent());
        tv_text2.setText(item.getStartTime());
        tv_text3.setText(item.getMovieName());

        ImageView iv_img = helper.getView(R.id.iv_img);
        Glide.with(mContext)
                .load(Config._API_URL + item.getCover())//图片地址
                .apply(GlideRequestOptions.OPTIONS_400_400)
                .into(iv_img);


    }
}