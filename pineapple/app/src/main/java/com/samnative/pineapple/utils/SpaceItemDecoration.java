package com.samnative.pineapple.utils;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Administrator on 2019/6/27.
 */

public class SpaceItemDecoration extends RecyclerView.ItemDecoration {

    private int space;
    private int col;

    public SpaceItemDecoration(int space, int col) {
        this.space = space;
        this.col = col;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        //不是第一个的格子都设一个左边和底部的间距
        outRect.left = space;
        outRect.bottom = space;
        //由于每行都只有col个，所以第一个都是col的倍数，把左边距设为0
        if (parent.getChildLayoutPosition(view) % col == 0) {
            outRect.left = 0;
        }
    }

}