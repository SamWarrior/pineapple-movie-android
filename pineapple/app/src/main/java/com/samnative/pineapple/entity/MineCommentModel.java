package com.samnative.pineapple.entity;

/**
 * 我的评论
 */
public class MineCommentModel {


    /**
     * _id : 6118e2dba61b683ed4cfeee3
     * movieId : m007
     * customid : 61181279c5497532749a31c0
     * content : 使用mongoose保存当前时间到数据库时，一开始我在schema中定义默认时间的方式是下面这样子的
     * startTime : 2021-08-15 17:48:11
     * movieName : 你好世界
     * cover : http://localhost:3030/uploads/e087e5531d7e3fec126794f5f9743a9f
     */

    private String _id;
    private String movieId;
    private String customid;
    private String content;
    private String startTime;
    private String movieName;
    private String cover;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getMovieId() {
        return movieId;
    }

    public void setMovieId(String movieId) {
        this.movieId = movieId;
    }

    public String getCustomid() {
        return customid;
    }

    public void setCustomid(String customid) {
        this.customid = customid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }
}
