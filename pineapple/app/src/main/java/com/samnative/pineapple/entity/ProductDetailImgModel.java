package com.samnative.pineapple.entity;

import com.stx.xhb.xbanner.entity.SimpleBannerInfo;

/**
 * 商品图片model
 * 
 * @author WangPeng
 * 
 */
public class ProductDetailImgModel extends SimpleBannerInfo {

	private String istop;
	private String productpic;
	private String sort;

	public String getIstop() {
		return istop;
	}

	public void setIstop(String istop) {
		this.istop = istop;
	}

	public String getProductpic() {
		return productpic;
	}

	public void setProductpic(String productpic) {
		this.productpic = productpic;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	@Override
	public Object getXBannerUrl() {
		return productpic;
	}
}
