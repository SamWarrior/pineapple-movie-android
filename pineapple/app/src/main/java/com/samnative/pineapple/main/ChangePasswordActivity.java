package com.samnative.pineapple.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.samnative.pineapple.http.HttpRequest;
import com.samnative.pineapple.http.JsonResult;
import com.samnative.pineapple.instance.AppPrefer;
import com.samnative.pineapple.utils.MD5Util;
import com.shashank.sony.fancytoastlib.FancyToast;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * 修改密码
 */

public class ChangePasswordActivity extends BaseActivity implements View.OnClickListener {

    /**
     * 密码
     */
    private EditText et_old_pw, etPw, etPwAgain;


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_back:
                finish();
                break;
            case R.id.tv_sure:
                if (et_old_pw.getText().toString().isEmpty() || etPw.getText().toString().length() < 6) {
                    FancyToast.showToast(mContext, getString(R.string.input_pwd), FancyToast.WARNING, false);
                    return;
                }
                if (etPw.getText().toString().isEmpty() || etPw.getText().toString().length() < 6) {
                    FancyToast.showToast(mContext, getString(R.string.input_pwd), FancyToast.WARNING, false);
                    return;
                }
                if (etPwAgain.getText().toString().isEmpty() || etPwAgain.getText().toString().length() < 6) {
                    FancyToast.showToast(mContext, getString(R.string.input_pwd), FancyToast.WARNING, false);
                    return;
                }
                if (!(etPw.getText().toString().trim()).equals(etPwAgain.getText().toString().trim())) {
                    FancyToast.showToast(mContext, getString(R.string.match_fail), FancyToast.WARNING, false);
                    return;
                }
                requestData();
                break;
            default:
        }
    }


    @Override
    protected int getLayoutId() {
        return R.layout.activity_password_set;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        find(R.id.rl_back).setOnClickListener(this);
        find(R.id.tv_sure).setOnClickListener(this);

        et_old_pw = find(R.id.et_old_pw);
        etPw = find(R.id.et_pw);
        etPwAgain = find(R.id.et_pw_again);

    }

    public void requestData() {
        Map<String, String> map = new HashMap<>();
        map.put("oldpwd", MD5Util.getMD5Str(et_old_pw.getText().toString()));
        map.put("newpwd", MD5Util.getMD5Str(etPw.getText().toString()));
        HttpRequest.request((Activity) mContext, "/client/user/changepwd", "", map, "POST", new JsonResult() {
            @Override
            public void reqestSuccess(JSONObject result) {
                FancyToast.showToast(mContext, "修改成功", FancyToast.SUCCESS, false);
                finish();
            }
        });
    }
}
