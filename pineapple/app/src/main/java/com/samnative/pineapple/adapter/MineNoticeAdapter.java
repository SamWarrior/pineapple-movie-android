package com.samnative.pineapple.adapter;

import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.samnative.pineapple.entity.MineCommentModel;
import com.samnative.pineapple.entity.MineNoticeModel;
import com.samnative.pineapple.main.R;
import com.samnative.pineapple.utils.GlideRequestOptions;
import com.samnative.pineapple.value.Config;

/**
 * 消息公告
 */
public class MineNoticeAdapter extends BaseQuickAdapter<MineNoticeModel, BaseViewHolder> {

    public MineNoticeAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, MineNoticeModel item) {
        TextView tv_text1 = helper.getView(R.id.tv_text1);
        TextView tv_text2 = helper.getView(R.id.tv_text2);
        TextView tv_text3 = helper.getView(R.id.tv_text3);

        tv_text1.setText(item.getTitle());
        tv_text2.setText(item.getContent());
        tv_text3.setText(item.getStartTime());



    }
}