package com.samnative.pineapple.widget;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;

import com.samnative.pineapple.main.R;


/**
 * 指示器页面
 */
public class IndicatorView extends View {

    int count = 0;
    int index = 0;
    int childWidth = 15;
    Paint paint;
    DisplayMetrics metrics;

    /**
     * 圆点默认
     */
    private int pointN;
    /**
     * 加点选中
     */
    private int pointP;

    public IndicatorView(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
        init();
    }

    public IndicatorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        init();
    }

    public IndicatorView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        // TODO Auto-generated constructor stub
        init();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (count < 2) { // 小于两页的不显示指示器
            return;
        }
        int cy = getHeight() / 2;
        int width = getWidth();
        int radius = width / 85;
        childWidth = width / 70;
        int left = (width - (count * childWidth) - (count * 5)) / 2;
        int density = (int) metrics.density;
        int cx;
        for (int i = 0; i < count; i++) {
            // if (density > 2.0f) {
            cx = left + childWidth * (i * density) - (childWidth / 2 + i);
            // } else{
            // cx = left + childWidth * (i + 1) - childWidth / 2;
            // }
            if (index == i) {
                paint.setColor(getResources().getColor(pointP));
                paint.setAntiAlias(true);
                paint.setStyle(Paint.Style.FILL);
                canvas.drawCircle(cx, cy, radius, paint);
            } else {
                paint.setColor(getResources().getColor(pointN));
                paint.setAntiAlias(true);
                paint.setStyle(Paint.Style.FILL);
                // paint.setStyle(Paint.Style.STROKE);
                // paint.setStrokeWidth(2.0f);
                canvas.drawCircle(cx, cy, radius, paint);
            }

        }
        super.onDraw(canvas);
    }

    private void init() {
        paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setAntiAlias(true);
        metrics = getResources().getDisplayMetrics();
        pointN = R.color.main_tab_text_n2;
        pointP = R.color.main_color;
    }

    /**
     * 设置圆点颜色
     *
     * @param pointN
     * @param pointP
     */
    public void setPointColor(int pointN, int pointP) {
        this.pointN = pointN;
        this.pointP = pointP;
    }

    public void setCount(int count) {
        this.count = count;
        this.index = 0;
        invalidate();
    }

    public void scrollTo(int index) {
        this.index = index;
        invalidate();
    }
}
