package com.samnative.pineapple.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

/**
 * 
 * @Description: 可监听滑动距离的scrollview
 * 
 * @date 2017-2-28
 * 
 * @version V1.0
 * 
 */
public class CustomScrollview extends ScrollView {

    public CustomScrollview(Context context, AttributeSet attrs,
                            int defStyleAttr) {
	super(context, attrs, defStyleAttr);
	// TODO Auto-generated constructor stub
    }

    public CustomScrollview(Context context, AttributeSet attrs) {
	super(context, attrs);
	// TODO Auto-generated constructor stub
    }

    // 添加滑动监听

    private ScrollViewListener scrollViewListener = null;

    /**
     * 监听接口
     * 
     */
    public interface ScrollViewListener {

	void onScrollChanged(ScrollView scrollView, int x, int y, int oldx,
                         int oldy);

    }

    public void setScrollViewListener(ScrollViewListener scrollViewListener) {
	this.scrollViewListener = scrollViewListener;
    }

    @Override
    protected void onScrollChanged(int x, int y, int oldx, int oldy) {
	super.onScrollChanged(x, y, oldx, oldy);
	if (scrollViewListener != null) {
	    scrollViewListener.onScrollChanged(this, x, y, oldx, oldy);
	}
    }
}
