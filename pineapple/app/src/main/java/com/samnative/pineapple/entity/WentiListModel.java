package com.samnative.pineapple.entity;

/**
 * 问题列表
 */
public class WentiListModel {


    /**
     * id : 4
     * title : 分类8的问题title9
     * sort : 21
     * is_show : 1
     * content : 详情
     * classify_id : 8
     */

    private String id;
    private String title;
    private String url = "";
    private String sort;
    private String is_show;
    private String content;
    private String classify_id;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getIs_show() {
        return is_show;
    }

    public void setIs_show(String is_show) {
        this.is_show = is_show;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getClassify_id() {
        return classify_id;
    }

    public void setClassify_id(String classify_id) {
        this.classify_id = classify_id;
    }
}
