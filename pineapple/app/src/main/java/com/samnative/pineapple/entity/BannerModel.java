package com.samnative.pineapple.entity;

import com.samnative.pineapple.value.Config;
import com.stx.xhb.xbanner.entity.SimpleBannerInfo;

/**
 * banner数据
 */
public class BannerModel extends SimpleBannerInfo {


    private String _id;
    private String movieId= "";
    private String movieName = "";
    private String imageUrl = "";

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getMovieId() {
        return movieId;
    }

    public void setMovieId(String movieId) {
        this.movieId = movieId;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public Object getXBannerUrl() {
        return Config._API_URL+imageUrl;
    }

    @Override
    public String getXBannerTitle() {
        return movieName;
    }


}
