package com.samnative.pineapple.widget;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ToastUtils;
import com.samnative.pineapple.main.R;
import com.samnative.pineapple.utils.ViewHelper;


/**
 * 加减数值控件
 *
 * @author WangPeng
 */
public class AddSubtractNumView extends RelativeLayout implements
        OnClickListener {

    /**
     * 加减按钮
     */
    private ImageButton ibDecreace, ibIncreace;
    /**
     * 数值显示
     */
    private TextView tvNum;
    /**
     * 默认最大数值99
     */
    private int maxNum = 999;
    /**
     * 默认最小数值1
     */
    private int minNum = 1;
    /**
     * 是否显示提醒语句
     */
    private boolean isAlert = true;
    /**
     * context
     */
    private Context context;

    public AddSubtractNumView(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
        init(context);
    }

    public AddSubtractNumView(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        init(context);
    }

    public AddSubtractNumView(Context context, AttributeSet attrs,
                              int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        // TODO Auto-generated constructor stub
        init(context);
    }

    /**
     * 初始化
     *
     * @param context
     */
    public void init(Context context) {
        this.context = context;
        LayoutInflater.from(context).inflate(R.layout.view_add_subtract, this,
                true);
        ibDecreace = (ImageButton) findViewById(R.id.asnv_ib_decreace);
        ibDecreace.setOnClickListener(this);
        ibIncreace = (ImageButton) findViewById(R.id.asnv_ib_increnace);
        ibIncreace.setOnClickListener(this);
        tvNum = (TextView) findViewById(R.id.asnv_tv_num);
        tvNum.setOnClickListener(this);
    }

    /**
     * 设置是否显示提示
     *
     * @param isAlert
     */
    public void setAlert(boolean isAlert) {
        this.isAlert = isAlert;
    }

    /**
     * 设置最大限制数
     *
     * @param max
     */
    public void setMaxNum(int max) {
        maxNum = max;

        String numStr = tvNum.getText().toString();
        try {
            int num = Integer.parseInt(numStr);

            if (num > max) {
                num = max;
                tvNum.setText(num > 0 ? num + "" : "1");
            }

//			if (num == minNum) {
            ibDecreace.setImageResource(R.mipmap.ic_num_minus);
//			} else {
//				ibDecreace.setImageResource(R.drawable.ic_minus_normal);
//			}

//			if (num == maxNum) {
            ibIncreace.setImageResource(R.mipmap.ic_num_increase);
//			} else {
//				ibIncreace.setImageResource(R.drawable.ic_add_normal);
//			}

        } catch (Exception e) {
            // TODO: handle exception
        }

    }

    /**
     * 设置最少限制数
     *
     * @param min
     */
    public void setMinNum(int min) {
        minNum = min;

        String numStr = tvNum.getText().toString();
        try {
            int num = Integer.parseInt(numStr);

            tvNum.setText(String.valueOf(num));

//            if (num == minNum) {
            ibDecreace.setImageResource(R.mipmap.ic_num_minus);
//            } else {
//                ibDecreace.setImageResource(R.drawable.ic_minus_normal);
//            }

//            if (num == maxNum) {
            ibIncreace.setImageResource(R.mipmap.ic_num_increase);
//            } else {
//                ibIncreace.setImageResource(R.drawable.ic_add_normal);
//            }

        } catch (Exception e) {
            // TODO: handle exception
        }

    }

    /**
     * 获取数据
     */
    public int getNum() {
        int num = 0;
        String numStr = tvNum.getText().toString();
        try {
            num = Integer.parseInt(numStr);
        } catch (Exception e) {
            // TODO: handle exception
        }

        return num;
    }

    /**
     * 设置默认值
     *
     * @param num
     */
    public void setNumDefault(int num) {
        tvNum.setText(String.valueOf(num));
//        if (num == minNum) {
        ibDecreace.setImageResource(R.mipmap.ic_num_minus);
//        } else {
//            ibDecreace.setImageResource(R.drawable.ic_minus_normal);
//        }

//        if (num == maxNum) {
        ibIncreace.setImageResource(R.mipmap.ic_num_increase);
//        } else {
//            ibIncreace.setImageResource(R.drawable.ic_add_normal);
//        }
    }

    /**
     * 数值++
     */
    public void setNumIncreace() {
        String numStr = tvNum.getText().toString();
        try {
            int num = Integer.parseInt(numStr);
            if (num < maxNum) {
                num = num + 1;
            } else {
                if (isAlert) {
                    if (maxNum == 0) {
                        ToastUtils.showShort("库存不足");
                    } else {
                        ToastUtils.showShort("该商品限购" + maxNum + "件");
                    }
                }
                return;

            }
            tvNum.setText(String.valueOf(num));
            if (asnwChecked != null) {
                asnwChecked.getNum(num);
            }

//            if (num == minNum) {
            ibDecreace.setImageResource(R.mipmap.ic_num_minus);
//            } else {
//                ibDecreace.setImageResource(R.drawable.ic_minus_normal);
//            }

//            if (num == maxNum) {
            ibIncreace.setImageResource(R.mipmap.ic_num_increase);
//            } else {
//                ibIncreace.setImageResource(R.drawable.ic_add_normal);
//            }

        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    /**
     * 数值--
     */
    public void setNumdecreace() {
        String numStr = tvNum.getText().toString();
        try {
            int num = Integer.parseInt(numStr);
            if (num > minNum) {
                num = num - 1;
            } else {
                if (isAlert) {
                    ToastUtils.showShort("该商品" + minNum + "件起购!");
                }
                return;
            }

            tvNum.setText(String.valueOf(num));
            if (asnwChecked != null) {
                asnwChecked.getNum(num);
            }

//            if (num == minNum) {
            ibDecreace.setImageResource(R.mipmap.ic_num_minus);
//            } else {
//                ibDecreace.setImageResource(R.drawable.ic_minus_normal);
//            }

//            if (num == maxNum) {
            ibIncreace.setImageResource(R.mipmap.ic_num_increase);
//            } else {
//                ibIncreace.setImageResource(R.drawable.ic_add_normal);
//            }

        } catch (Exception e) {
            // TODO: handle exception
        }

    }

    // 点击监听
    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.asnv_ib_decreace:
                setNumdecreace();
                break;
            case R.id.asnv_ib_increnace:
                setNumIncreace();
                break;
            case R.id.asnv_tv_num:
                EditBuyNumPop pop = new EditBuyNumPop(context);
                WindowManager.LayoutParams params = pop.getWindow().getAttributes();
                params.width = (int) (ViewHelper.getWindowRealize((Activity) context).x * 0.65);
                pop.getWindow().setAttributes(params);
                pop.setMaxBuyNum(maxNum);
                pop.setCurrentNum(getNum());
                pop.setMinBuyNum(minNum);
                pop.setOnResult(new EditBuyNumPop.OnResult() {
                    @Override
                    public void ok(int num) {
                        // TODO Auto-generated method stub
                        tvNum.setText("" + num);
                        if (asnwChecked != null) {
                            asnwChecked.getNum(num);
                        }
                    }
                });
                pop.show();

                break;
        }

    }

    public OnAsnwChecked asnwChecked;

    public void setOnAsnwChecked(OnAsnwChecked asnwChecked) {
        this.asnwChecked = asnwChecked;
    }

    ;

    public interface OnAsnwChecked {
        public void getNum(int num);
    }

}
