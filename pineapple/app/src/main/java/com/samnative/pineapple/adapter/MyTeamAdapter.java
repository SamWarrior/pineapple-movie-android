package com.samnative.pineapple.adapter;

import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.samnative.pineapple.entity.TeamModel;
import com.samnative.pineapple.main.R;

public class MyTeamAdapter extends BaseQuickAdapter<TeamModel.DataBean.ListBean, BaseViewHolder> {

    public MyTeamAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, TeamModel.DataBean.ListBean item) {
        TextView tv_name = helper.getView(R.id.tv_name);
        TextView tv_date = helper.getView(R.id.tv_date);
        TextView tv_nickname = helper.getView(R.id.tv_nickname);

        tv_name.setText(item.getUsername());
        tv_date.setText(item.getAddtime());
        tv_nickname.setText(item.getNickname());

    }
}