package com.samnative.pineapple.main;

import android.app.Application;
import android.os.StrictMode;
import android.support.v4.content.ContextCompat;

import com.blankj.utilcode.util.Utils;
import com.kongzue.dialog.util.DialogSettings;
import com.umeng.commonsdk.UMConfigure;
import com.samnative.pineapple.instance.AppPrefer;
import com.samnative.pineapple.value.Config;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.UUID;

/**
 * Created by Administrator on 2019/5/13.
 */

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        // android 7.0系统解决拍照的问题
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        builder.detectFileUriExposure();

        closeAndroid10Dialog();

        Utils.init(this); //工具库初始化

        saveUUID(); //生成UUID

        //公用对话框初始化
        DialogSettings.isUseBlur = false;                   //是否开启模糊效果，默认关闭
        DialogSettings.style = DialogSettings.STYLE.STYLE_IOS;          //全局主题风格，提供三种可选风格，STYLE_MATERIAL, STYLE_KONGZUE, STYLE_IOS
        DialogSettings.theme = DialogSettings.THEME.LIGHT;          //全局对话框明暗风格，提供两种可选主题，LIGHT, DARK
        DialogSettings.tipTheme = DialogSettings.THEME.DARK;       //全局提示框明暗风格，提供两种可选主题，LIGHT, DARK
//        DialogSettings.titleTextInfo = (TextInfo);              //全局标题文字样式
//        DialogSettings.contentTextInfo = (TextInfo);            //全局正文文字样式
//        DialogSettings.buttonTextInfo = (TextInfo);             //全局默认按钮文字样式
//        DialogSettings.buttonPositiveTextInfo = (TextInfo);     //全局焦点按钮文字样式（一般指确定按钮）
//        DialogSettings.inputInfo = (InputInfo);                 //全局输入框文本样式
        DialogSettings.backgroundColor = (ContextCompat.getColor(getApplicationContext(), R.color.white));            //全局对话框背景颜色，值0时不生效
//        DialogSettings.cancelable = (boolean);                  //全局对话框默认是否可以点击外围遮罩区域或返回键关闭，此开关不影响提示框（TipDialog）以及等待框（TipDialog）
//        DialogSettings.cancelableTipDialog = (boolean);         //全局提示框及等待框（WaitDialog、TipDialog）默认是否可以关闭
//        DialogSettings.DEBUGMODE = (boolean);                   //是否允许打印日志
//        DialogSettings.blurAlpha = (120);                       //开启模糊后的透明度（0~255）
//        DialogSettings.systemDialogStyle = (styleResId);        //自定义系统对话框style，注意设置此功能会导致原对话框风格和动画失效
//        DialogSettings.dialogLifeCycleListener = (DialogLifeCycleListener);  //全局Dialog生命周期监听器

        // 友盟SDK预初始化函数 //在隐私政策通过后，才可以真正初始化,如不同意隐私政策则不能初始化和使用友盟
        // preInit预初始化函数耗时极少，不会影响App首次冷启动用户体验
        UMConfigure.preInit(this, Config.umengKey, "umeng");

    }


    /**
     * 首次进入生成UUID
     *
     * @return
     */
    public void saveUUID() {
        String uuid = AppPrefer.getInstance(this).getString(AppPrefer._uuid, "");
        if (uuid.isEmpty()) {
            uuid = UUID.randomUUID().toString();
            AppPrefer.getInstance(this).put(AppPrefer._uuid, uuid);
        }
        Config._uuid = uuid;
    }


    /**
     * Android 9.0 提示“Detected problems with API compatibility”的解决方案
     */
    public void closeAndroid10Dialog() {
        try {
            Class aClass = Class.forName("android.content.pm.PackageParser$Package");
            Constructor declaredConstructor = aClass.getDeclaredConstructor(String.class);
            declaredConstructor.setAccessible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            Class cls = Class.forName("android.app.ActivityThread");
            Method declaredMethod = cls.getDeclaredMethod("currentActivityThread");
            declaredMethod.setAccessible(true);
            Object activityThread = declaredMethod.invoke(null);
            Field mHiddenApiWarningShown = cls.getDeclaredField("mHiddenApiWarningShown");
            mHiddenApiWarningShown.setAccessible(true);
            mHiddenApiWarningShown.setBoolean(activityThread, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
