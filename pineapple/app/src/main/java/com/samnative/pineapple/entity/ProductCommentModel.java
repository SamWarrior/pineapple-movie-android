package com.samnative.pineapple.entity;

/**
 * 商品评价model
 * 
 * @author WangPeng
 * 
 */
public class ProductCommentModel {

	private String id;
	private String productid;
	private String businessid;
	private String productname;
	private String scores;
	private String content;
	private String desccredit;
	private String frommemberid;
	private String frommembername;
	private String headpic;
	private String servicecredit;
	private String deliverycredit;
	private String evaluateauto;
	private String addtime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProductid() {
		return productid;
	}

	public void setProductid(String productid) {
		this.productid = productid;
	}

	public String getBusinessid() {
		return businessid;
	}

	public void setBusinessid(String businessid) {
		this.businessid = businessid;
	}

	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}

	public String getScores() {
		return scores;
	}

	public void setScores(String scores) {
		this.scores = scores;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getDesccredit() {
		return desccredit;
	}

	public void setDesccredit(String desccredit) {
		this.desccredit = desccredit;
	}

	public String getFrommemberid() {
		return frommemberid;
	}

	public void setFrommemberid(String frommemberid) {
		this.frommemberid = frommemberid;
	}

	public String getFrommembername() {
		return frommembername;
	}

	public void setFrommembername(String frommembername) {
		this.frommembername = frommembername;
	}

	public String getHeadpic() {
		return headpic;
	}

	public void setHeadpic(String headpic) {
		this.headpic = headpic;
	}

	public String getServicecredit() {
		return servicecredit;
	}

	public void setServicecredit(String servicecredit) {
		this.servicecredit = servicecredit;
	}

	public String getDeliverycredit() {
		return deliverycredit;
	}

	public void setDeliverycredit(String deliverycredit) {
		this.deliverycredit = deliverycredit;
	}

	public String getEvaluateauto() {
		return evaluateauto;
	}

	public void setEvaluateauto(String evaluateauto) {
		this.evaluateauto = evaluateauto;
	}

	public String getAddtime() {
		return addtime;
	}

	public void setAddtime(String addtime) {
		this.addtime = addtime;
	}

}
