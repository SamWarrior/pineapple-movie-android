package com.samnative.pineapple.entity;

/**
 * Created by Wy on 2019/3/19.
 */

public class AboutModel {


    /**
     * title : 客服QQ
     * text : 339452146
     */

    private String title;
    private String text;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
