package com.samnative.pineapple.entity;

import java.io.Serializable;

/**
 * 分享数据
 */
public class ShareData implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 标题
     */
    private String title;
    /**
     * 摘要
     */
    private String description;
    /**
     * 链接
     */
    private String url;
    /**
     * 图片链接
     */
    private String image;
    /**
     * 是否是本地图片
     */
    private boolean is_loca_img;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean is_loca_img() {
        return is_loca_img;
    }

    public void setIs_loca_img(boolean is_loca_img) {
        this.is_loca_img = is_loca_img;
    }


    @Override
    public String toString() {
        return "ShareData{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", url='" + url + '\'' +
                ", image='" + image + '\'' +
                ", is_loca_img=" + is_loca_img +
                '}';
    }
}
