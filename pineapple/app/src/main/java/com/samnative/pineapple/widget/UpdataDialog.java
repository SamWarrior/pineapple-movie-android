package com.samnative.pineapple.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.samnative.pineapple.entity.UpdataModel;
import com.samnative.pineapple.main.R;


/**
 * 升级提示框
 */
public class UpdataDialog extends AlertDialog {

    /**  */
    private Context context;

    private UpdataModel model;
    private View.OnClickListener onClickListener;
    private TextView tv_cancel;
    private TextView tv_ok;
    private TextView updata_content, updata_title, updata_remark;
    private ProgressBar progressBar1;

    public UpdataDialog(Context context, UpdataModel model,
                        View.OnClickListener onClickListener) {
        super(context);
        this.context = context;
        this.model = model;
        this.onClickListener = onClickListener;
    }

    @Override
    public void show() {
        super.show();
        Window window = this.getWindow();
        View view = LayoutInflater.from(context).inflate(
                R.layout.dialog_updata, null);

        updata_content = (TextView) view.findViewById(R.id.updata_content);
        updata_title = (TextView) view.findViewById(R.id.updata_title);
        updata_remark = (TextView) view.findViewById(R.id.updata_remark);
        progressBar1 = (ProgressBar) view.findViewById(R.id.progressBar1);
        tv_cancel = (TextView) view.findViewById(R.id.tv_cancel);
        tv_cancel.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
        tv_ok = (TextView) view.findViewById(R.id.tv_ok);
        tv_ok.setOnClickListener(onClickListener);
        tv_cancel.setOnClickListener(onClickListener);
        dataInit();

        window.setContentView(view);
        window.setBackgroundDrawableResource(android.R.color.transparent);
    }


    private void dataInit() {
        // TODO Auto-generated method stub
        if (model.getBtnEnterText() != null && !model.getBtnEnterText().isEmpty()) {
            tv_ok.setText(model.getBtnEnterText());
        }
        if (model.getBtnCancelText() != null) {
            tv_cancel.setText(model.getBtnCancelText());
        }

        updata_content.setText(model.getInfo());
        updata_title.setText(model.getTitle());
        if (model.getRemark() == null || model.getRemark().equals("")) {
            updata_remark.setVisibility(View.GONE);
        } else {
            updata_remark.setVisibility(View.VISIBLE);
            updata_remark.setText(model.getRemark());
        }
        if (model.getUpdate().equals("1")) {
            tv_cancel.setVisibility(View.VISIBLE);
        } else if (model.getUpdate().equals("2")) {
            tv_cancel.setVisibility(View.GONE);
            this.setCancelable(false);
            this.setCanceledOnTouchOutside(false);
        }
    }


    /**
     * 下载中,传入下载进度
     *
     * @param progressBar
     */
    public void setProgressBar(int progressBar) {
        if (progressBar1 != null) {
            if (progressBar1.getVisibility() == View.GONE) {
                progressBar1.setVisibility(View.VISIBLE);
            }
            progressBar1.setProgress(progressBar);
            tv_ok.setText("下载中" + progressBar + "%");
            tv_ok.setEnabled(false);
        }
    }

    /**
     * 下载结束
     */
    public void setDownLoadEnd() {
        tv_ok.setText("立即更新");
        tv_ok.setEnabled(true);
    }

}
