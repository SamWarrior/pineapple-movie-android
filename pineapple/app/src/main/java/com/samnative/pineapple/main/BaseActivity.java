package com.samnative.pineapple.main;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;

import com.blankj.swipepanel.SwipePanel;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.gyf.immersionbar.ImmersionBar;
import com.gyf.immersionbar.OnNavigationBarListener;
import com.samnative.pineapple.utils.ViewHelper;
import com.samnative.pineapple.value.Config;

/**
 * Activity基类
 */
public abstract class BaseActivity extends AppCompatActivity {

    public Context mContext;

    protected abstract int getLayoutId();

    protected void initView(Bundle savedInstanceState) {
    }

    protected void initData() {
    }

    protected <T extends View> T find(int id) {
        return (T) super.findViewById(id);
    }

    protected ImmersionBar immersionBar;

    protected SwipePanel swipePanel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(getLayoutId());
        immersionBar = ImmersionBar.with(this);

        immersionBar.statusBarDarkFont(true)
                .keyboardEnable(true)
                .fullScreen(false)
                .setOnNavigationBarListener(new OnNavigationBarListener() {
                    @Override
                    public void onNavigationBarChange(boolean show) {
                        if (!show) {
                            immersionBar.fullScreen(true);
                        }
                    }
                }); //导航栏显示隐藏监听，目前只支持华为和小米手机

        boolean isNb = immersionBar.hasNavigationBar(this);
        if (!isNb) {
            immersionBar.fullScreen(true);
            LogUtils.e("无导航栏");
        } else {
            immersionBar.fullScreen(false);
            LogUtils.e("有导航栏");
        }

        immersionBar.init();

        swipePanel = new SwipePanel(this);
        swipePanel.setLeftEdgeSize(ViewHelper.dp2px(this, 10));// 设置左侧触发阈值
        swipePanel.setLeftDrawable(ContextCompat.getDrawable(this, R.mipmap.ic_sliding_back));// 设置左侧 icon
        swipePanel.wrapView(((ViewGroup) findViewById(android.R.id.content)).getChildAt(0));// 设置嵌套在 rootLayout 外层
        swipePanel.setOnFullSwipeListener(new SwipePanel.OnFullSwipeListener() {// 设置完全划开松手后的监听
            @Override
            public void onFullSwipe(int direction) {
                finish();
                swipePanel.close(direction);// 关闭
            }
        });

        initView(savedInstanceState);
        initData();
    }

    /**
     * 获取当前网络类型
     */
    public void getNetworkType() {
        Config._network = NetworkUtils.getNetworkType().name();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Thread() {
            @Override
            public void run() {
                super.run();
                getNetworkType();
            }
        }.start();
    }

    @Override
    public Resources getResources() {
        //设置系统字体放大倍数,现在强制使用标准1倍大小,用户在手机设置里放大字体,APP也不会影响到
        Resources res = super.getResources();
        Configuration configuration = res.getConfiguration();
        if (configuration.fontScale != 1.0f) {
            configuration.fontScale = 1.0f;
            res.updateConfiguration(configuration, res.getDisplayMetrics());
        }
        return res;
    }


    /**
     * 打开新Activity
     *
     * @param intent intent
     */
    public void startActivity(Intent intent) {
        startActivity(intent, true);
    }

    /**
     * 打开新Activity
     *
     * @param intent  intent
     * @param animIn  新Activity进入的动画
     * @param animOut 当前Activity退出的动画
     */
    public void startActivity(Intent intent, int animIn, int animOut) {
        super.startActivity(intent);
        if (false == this instanceof MainActivity)
            overridePendingTransition(animIn, animOut);
    }

    /**
     * 打开新的Activity
     *
     * @param intent intent
     * @param isAnim 是否开启过渡动画
     */
    public void startActivity(Intent intent, boolean isAnim) {
        if (isAnim) {
            startActivity(intent, R.anim.act_right_in, R.anim.act_left_out);
        } else {
            super.startActivity(intent);
        }
    }

    //

    /**
     * 打开新Activity
     *
     * @param intent intent
     */
    public void startActivityForResult(Intent intent, int requestCode) {
        startActivityForResult(intent, requestCode, true);
    }

    /**
     * 打开新Activity
     *
     * @param intent  intent
     * @param animIn  新Activity进入的动画
     * @param animOut 当前Activity退出的动画
     */
    public void startActivityForResult(Intent intent, int requestCode, int animIn, int animOut) {
        super.startActivityForResult(intent, requestCode);
        if (false == this instanceof MainActivity)
            overridePendingTransition(animIn, animOut);
    }

    /**
     * 打开新的Activity
     *
     * @param intent intent
     * @param isAnim 是否开启过渡动画
     */
    public void startActivityForResult(Intent intent, int requestCode, boolean isAnim) {
        if (isAnim) {
            startActivityForResult(intent, requestCode, R.anim.act_right_in, R.anim.act_left_out);
        } else {
            super.startActivityForResult(intent, requestCode);
        }
    }


    @Override
    public void finish() {
        finish(true);
    }

    /**
     * 退出Activity
     *
     * @param animIn  老Activity进入的动画
     * @param animOut 当前Activity退出的动画
     */
    public void finish(int animIn, int animOut) {
        super.finish();
        if (false == this instanceof MainActivity)
            overridePendingTransition(animIn, animOut);
    }

    /**
     * 退出Activity
     *
     * @param isAnim 是否开启过渡动画
     */
    public void finish(boolean isAnim) {
        if (isAnim) {
            finish(R.anim.act_left_in, R.anim.act_right_out);
        } else {
            super.finish();
        }
    }

}
