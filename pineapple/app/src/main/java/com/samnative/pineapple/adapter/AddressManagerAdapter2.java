package com.samnative.pineapple.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.samnative.pineapple.entity.ReceiveAddressModel;
import com.samnative.pineapple.entity.UserModel;
import com.samnative.pineapple.http.HttpRequest;
import com.samnative.pineapple.http.JsonResult;
import com.samnative.pineapple.instance.AppPrefer;
import com.samnative.pineapple.interfaces.OnDataNotice;
import com.samnative.pineapple.main.R;
import com.kongzue.dialog.interfaces.OnDialogButtonClickListener;
import com.kongzue.dialog.util.BaseDialog;
import com.kongzue.dialog.v3.MessageDialog;
import com.shashank.sony.fancytoastlib.FancyToast;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 地址列表适配器
 */
public class AddressManagerAdapter2 extends BaseAdapter {


    private Context mContext;
    private List<ReceiveAddressModel> list;
    private String source;
    private OnDataNotice mOnNotice;

    public AddressManagerAdapter2(Context context, List<ReceiveAddressModel> list, String source, OnDataNotice onNotice) {
        mContext = context;
        this.list = list;
        this.source = source;
        this.mOnNotice = onNotice;
    }

    public List<ReceiveAddressModel> getList() {
        return list;
    }

    public void setList(List<ReceiveAddressModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list != null ? list.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return list != null ? list.get(position) : 0;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        final ReceiveAddressModel model = list.get(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(
                    R.layout.item_address_manager2, null);
            holder = new ViewHolder();
            holder.item_address_detail = (TextView) convertView
                    .findViewById(R.id.item_address_detail);
            holder.item_address_name = (TextView) convertView
                    .findViewById(R.id.item_address_name);
            holder.item_address_phone = (TextView) convertView
                    .findViewById(R.id.item_address_phone);
            holder.operate_rl = (RelativeLayout) convertView.findViewById(R.id.operate_rl);
            holder.default_layout = (LinearLayout) convertView
                    .findViewById(R.id.default_layout);
            holder.no_default_iv = (ImageView) convertView
                    .findViewById(R.id.no_default_iv);
            holder.default_iv = (ImageView) convertView
                    .findViewById(R.id.default_iv);
            holder.editLayout = (LinearLayout) convertView
                    .findViewById(R.id.edit_layout);
            holder.deleteLayout = (LinearLayout) convertView
                    .findViewById(R.id.delete_layout);
            holder.tv_normal = (TextView) convertView.findViewById(R.id.tv_normal);
            convertView.setTag(holder);

        } else {

            holder = (ViewHolder) convertView.getTag();
        }
        holder.item_address_name.setText(model.getRealname());
        holder.item_address_phone.setText(model.getMobile());
        //部份字体颜色
        String str = "[默认地址]";
        SpannableStringBuilder style = new SpannableStringBuilder(str + model.getCity() + model.getAddress());
        if (model.getIsdefault().equals("1")) {
            style.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.main_color)), 0, str.length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
            holder.item_address_detail.setText(style);
            holder.default_iv.setVisibility(View.VISIBLE);
            holder.no_default_iv.setVisibility(View.GONE);
        } else {
            holder.item_address_detail.setText(model.getCity()
                    + model.getAddress());
            holder.default_iv.setVisibility(View.GONE);
            holder.no_default_iv.setVisibility(View.VISIBLE);
        }
        if (source.equals("select")) {
            holder.operate_rl.setVisibility(View.GONE);
        } else {
            holder.operate_rl.setVisibility(View.VISIBLE);
        }
        holder.default_layout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                setDafaultAddress(position);
            }

        });
        holder.deleteLayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                MessageDialog.show((AppCompatActivity) mContext, null, "确认删除收货地址!", mContext.getString(R.string.confirm), mContext.getString(R.string.cancel)).setOnOkButtonClickListener(
                        new OnDialogButtonClickListener() {
                            @Override
                            public boolean onClick(BaseDialog baseDialog, View v) {
                                deleteAddress(position);
                                return false;
                            }
                        });

            }
        });

        holder.editLayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                Intent intent = new Intent(mContext, ModifyAddressActivity.class);
//                intent.putExtra("address_id", model.getAddress_id());
//                intent.putExtra("realname", model.getRealname());
//                intent.putExtra("mobile", model.getMobile());
//                intent.putExtra("city_id", model.getCity_id());
//                intent.putExtra("city", model.getCity());
//                intent.putExtra("address", model.getAddress());
//                intent.putExtra("default", model.getIsdefault());
//                mContext.startActivity(intent);
            }
        });

        return convertView;
    }

    public class ViewHolder {
        private TextView item_address_name;
        private TextView item_address_phone;
        private TextView item_address_detail;
        private RelativeLayout operate_rl;
        private ImageView no_default_iv;
        private ImageView default_iv;
        private LinearLayout default_layout;
        private LinearLayout editLayout;
        private LinearLayout deleteLayout;
        private TextView tv_normal;
    }

    //删除地址
    private void deleteAddress(final int position) {
        // TODO Auto-generated method stub
        Map<String, String> map = new HashMap<String, String>();
        map.put("logisticid", list.get(position).getAddress_id());
        map.put("_apiname", "user.logistics.delCustomerLogistic");
        map.put("mtoken", AppPrefer.getInstance(mContext).getString(AppPrefer.mtoken, ""));
        HttpRequest.post((Activity) mContext, "", map, new JsonResult() {
            @Override
            public void reqestSuccess(JSONObject result) {
                list.remove(position);
                notifyDataSetChanged();
                FancyToast.showToast(mContext, "删除成功", FancyToast.SUCCESS, false);
                if (list.size() < 1) {
                    UserModel model = AppPrefer.getUserModel(mContext);
//                    model.getData().getUserinfo().setLogisticsDec("0");
                    mOnNotice.upData();
                }
            }
        });

    }

    //设置默认地址
    private void setDafaultAddress(int position) {
        // TODO Auto-generated method stub
        Map<String, String> map = new HashMap<String, String>();
        map.put("_apiname", "user.logistics.setDefaultlogistic");
        map.put("logisticid", list.get(position).getAddress_id());
        map.put("mtoken", AppPrefer.getInstance(mContext).getString(AppPrefer.mtoken, ""));
        HttpRequest.post((Activity) mContext, "", map, new JsonResult() {
            @Override
            public void reqestSuccess(JSONObject result) {
                FancyToast.showToast(mContext, "设置成功", FancyToast.SUCCESS, false);
                mOnNotice.upData();
            }
        });
    }
}
