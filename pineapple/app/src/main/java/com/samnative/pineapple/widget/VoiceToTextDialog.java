package com.samnative.pineapple.widget;

/**
 * 讯飞听写,自定义UI
 */
//public class VoiceToTextDialog extends Dialog implements View.OnClickListener, View.OnTouchListener {
//
//    private ImageView tv;
//    private TextView tv_msg;
//    private SpinKitView spin_kit;
//    // 语音听写对象
//    private SpeechRecognizer mIat;
//    // 用HashMap存储听写结果
//    private HashMap<String, String> mIatResults;
//    //音频振幅动画
//    private AnimatedRecordingView mRecordingView;
//
//    private Context context;
//
//    public VoiceToTextDialog(Context context) {
//        super(context);
//        this.context = context;
//    }
//
//    private OnVoiceResult onVoiceResult;
//
//    public void setOnVoiceResult(OnVoiceResult onVoiceResult) {
//        this.onVoiceResult = onVoiceResult;
//    }
//
//    public interface OnVoiceResult {
//        void getResult(String result);
//    }
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
//
//        mIatResults = new LinkedHashMap<String, String>();
//
//        Window window = this.getWindow();
//        View view = LayoutInflater.from(getContext()).inflate(
//                R.layout.voice_to_text_dialog, null);
//
//        mRecordingView = (AnimatedRecordingView) view.findViewById(R.id.recording);
////        // start recording animation
////        mRecordingView.start();
////        // set the mic volume
////        mRecordingView.setVolume(5);
////        // start loading animation
////        mRecordingView.loading();
////        // start finished animation
////        mRecordingView.stop();
//
//        tv = view.findViewById(R.id.tv);
//        tv.setLongClickable(true);
//        tv.setOnTouchListener(this);
//        tv_msg = view.findViewById(R.id.tv_msg);
//
//        spin_kit = view.findViewById(R.id.spin_kit);
//        Sprite sprite = new MultiplePulse();
//        spin_kit.setIndeterminateDrawable(sprite);
//
//        view.findViewById(R.id.iv_close).setOnClickListener(this);
//
//
//        //讯飞语音听写
//        // 将“12345678”替换成您申请的APPID，申请地址：http://www.xfyun.cn
//        // 请勿在“=”与appid之间添加任何空字符或者转义符
//        SpeechUtility.createUtility(getContext(), SpeechConstant.APPID + "=" + Config.xunfeiKey);
//
//        // 初始化识别无UI识别对象
//        // 使用SpeechRecognizer对象，可根据回调消息自定义界面；
//        mIat = SpeechRecognizer.createRecognizer(getContext(), new InitListener() {
//            @Override
//            public void onInit(int code) {
//                LogUtils.e("SpeechRecognizer init() code = " + code);
//                if (code != ErrorCode.SUCCESS) {
//                    LogUtils.e("初始化失败，错误码：" + code + ",请点击网址https://www.xfyun.cn/document/error-code查询解决方案");
//                }
//            }
//        });
//        // 设置参数
//        setParam();
//
//
//        window.setContentView(view);
//        window.setGravity(Gravity.BOTTOM);
//        WindowManager.LayoutParams lp = window.getAttributes();
//        lp.width = ViewHelper.getWindowSize((Activity) context).x;
//        //设置宽度
//        window.setAttributes(lp);
//        window.setBackgroundDrawableResource(android.R.color.transparent);
//        this.setCanceledOnTouchOutside(true);
//    }
//
//
//    @Override
//    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.iv_close:
//                cancel();
//                break;
//        }
//    }
//
//    /**
//     * 听写监听器。
//     */
//    private RecognizerListener mRecognizerListener = new RecognizerListener() {
//
//        @Override
//        public void onBeginOfSpeech() {
//            // 此回调表示：sdk内部录音机已经准备好了，用户可以开始语音输入
//            LogUtils.e("讯飞听写准备完毕,开始录音");
//
//            tv_msg.setText("松开结束");
//            spin_kit.setVisibility(View.VISIBLE);
//
//            // start recording animation
//            mRecordingView.start();
//
//
//        }
//
//        @Override
//        public void onError(SpeechError error) {
//            // Tips：
//            // 错误码：10118(您没有说话)，可能是录音机权限被禁，需要提示用户打开应用的录音权限。
//            LogUtils.e(error.getPlainDescription(true));
//
//            endReset();
//        }
//
//        @Override
//        public void onEndOfSpeech() {
//            // 此回调表示：检测到了语音的尾端点，已经进入识别过程，不再接受语音输入
//            LogUtils.e("讯飞听写,录音结束");
//
//            endReset();
//        }
//
//        @Override
//        public void onResult(RecognizerResult results, boolean isLast) {
//            printResult(results);
//
//            endReset();
//        }
//
//        @Override
//        public void onVolumeChanged(int volume, byte[] data) {
//            LogUtils.e("当前正在录音，音量大小：" + volume);
////            LogUtils.e("返回音频数据：" + data.length);
//
//            mRecordingView.setVolume(volume * 4);
//
//        }
//
//        @Override
//        public void onEvent(int eventType, int arg1, int arg2, Bundle obj) {
//            // 以下代码用于获取与云端的会话id，当业务出错时将会话id提供给技术支持人员，可用于查询会话日志，定位出错原因
//            // 若使用本地能力，会话id为null
//            //	if (SpeechEvent.EVENT_SESSION_ID == eventType) {
//            //		String sid = obj.getString(SpeechEvent.KEY_EVENT_SESSION_ID);
//            //		Log.d(TAG, "session id =" + sid);
//            //	}
//        }
//    };
//
//    /**
//     * 听写结束重置,页面
//     */
//    private void endReset() {
//        tv_msg.setText("按住说话");
//        spin_kit.setVisibility(View.GONE);
//
//        mRecordingView.loading();
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                // start finished animation
//                mRecordingView.stop();
//
//                cancel();
//            }
//        }, 500);
//    }
//
//    private void printResult(RecognizerResult results) {
//
//        String text = JsonParser.parseIatResult(results.getResultString());
//
//        String sn = null;
//        boolean ls = false;
//        try {
//            JSONObject resultJson = new JSONObject(results.getResultString());
//            sn = resultJson.optString("sn");
//            ls = resultJson.optBoolean("ls");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        mIatResults.put(sn, text);
//
//        StringBuffer resultBuffer = new StringBuffer();
//        for (String key : mIatResults.keySet()) {
//            resultBuffer.append(mIatResults.get(key));
//        }
////        LogUtils.e(results.getResultString());
//        if (ls) {
//            //本次听写最后组合结果
//            LogUtils.e(resultBuffer.toString());
//            if (onVoiceResult != null) {
//                onVoiceResult.getResult(resultBuffer.toString());
//            }
//        }
//
//
//    }
//
//    /**
//     * 参数设置
//     *
//     * @return
//     */
//    public void setParam() {
//        // 清空参数
//        mIat.setParameter(SpeechConstant.PARAMS, null);
//
//        // 设置听写引擎
//        mIat.setParameter(SpeechConstant.ENGINE_TYPE, SpeechConstant.TYPE_CLOUD);
//        // 设置返回结果格式
//        mIat.setParameter(SpeechConstant.RESULT_TYPE, "json");
//
//        // 设置语言
//        mIat.setParameter(SpeechConstant.LANGUAGE, "zh_cn");
//        // 设置语言区域
//        mIat.setParameter(SpeechConstant.ACCENT, "mandarin");
//
//        //此处用于设置dialog中不显示错误码信息
//        //mIat.setParameter("view_tips_plain","false");
//
//        // 设置语音前端点:静音超时时间，即用户多长时间不说话则当做超时处理
//        mIat.setParameter(SpeechConstant.VAD_BOS, "4000");
//
//        // 设置语音后端点:后端点静音检测时间，即用户停止说话多长时间内即认为不再输入， 自动停止录音
//        mIat.setParameter(SpeechConstant.VAD_EOS, "1000");
//
//        // 设置标点符号,设置为"0"返回结果无标点,设置为"1"返回结果有标点
//        mIat.setParameter(SpeechConstant.ASR_PTT, "0");
//
//        // 设置音频保存路径，保存音频格式支持pcm、wav，设置路径为sd卡请注意WRITE_EXTERNAL_STORAGE权限
//        mIat.setParameter(SpeechConstant.AUDIO_FORMAT, "wav");
//        mIat.setParameter(SpeechConstant.ASR_AUDIO_PATH, Environment.getExternalStorageDirectory() + "/msc/iat.wav");
//    }
//
//
//    //===================================================
//
//    @Override
//    public boolean onTouch(View v, MotionEvent event) {
//
//        if (event.getAction() == MotionEvent.ACTION_DOWN) {
//            mIatResults.clear();
//            // 自定义调用听写
//            int ret = mIat.startListening(mRecognizerListener);
//            if (ret != ErrorCode.SUCCESS) {
//                LogUtils.e("听写失败,错误码：" + ret + ",请点击网址https://www.xfyun.cn/document/error-code查询解决方案");
//            }
//        }
//
//        if (event.getAction() == MotionEvent.ACTION_UP) {
//            mIat.stopListening();
//        }
//
//        return false;
//    }
//
//
//    @Override
//    public void show() {
//        super.show();
//        getPermissions();
//    }
//
//
//    /**
//     * 检查并申请权限
//     */
//    public void getPermissions() {
//
//        if (!EasyPermissions.hasPermissions(context, Manifest.permission.RECORD_AUDIO)) {
//            EasyPermissions.requestPermissions((Activity) context, "\"" + context.getString(R.string.app_name) + "\"需要获取相关权限才能正常使用,请确认",
//                    110, Manifest.permission.RECORD_AUDIO);
//        }
//
//    }
//}
