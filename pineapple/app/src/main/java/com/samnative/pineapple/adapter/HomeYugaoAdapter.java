package com.samnative.pineapple.adapter;

import android.support.annotation.LayoutRes;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.samnative.pineapple.entity.HomeYugaoModel;
import com.samnative.pineapple.main.R;
import com.samnative.pineapple.value.Config;
import com.xiao.nicevideoplayer.NiceVideoPlayer;
import com.xiao.nicevideoplayer.TxVideoPlayerController;

/**
 * 首页-热门预告
 */
public class HomeYugaoAdapter extends BaseQuickAdapter<HomeYugaoModel, BaseViewHolder> {

    public HomeYugaoAdapter(@LayoutRes int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, final HomeYugaoModel item) {

//        ImageView iv_img = helper.getView(R.id.iv_img);
//        Glide.with(mContext)
//                .load(Config._API_URL + item.getCover())//图片地址
//                .apply(GlideRequestOptions.OPTIONS_400_400)
//                .into(iv_img);

        TxVideoPlayerController controller = new TxVideoPlayerController(mContext);
        controller.onPlayModeChanged(NiceVideoPlayer.MODE_NORMAL);
        controller.setBacktoExit(false);
        controller.isCanFullScreen(true);
        controller.setCoverUrl(Config._API_URL+item.getCover());

        NiceVideoPlayer nice_video_player = helper.getView(R.id.nice_video_player);
        nice_video_player.setPlayerType(NiceVideoPlayer.TYPE_NATIVE);
        nice_video_player.setController(controller);
        nice_video_player.setUp(item.getMovieUrl(), null);

        TextView tv_title=helper.getView(R.id.tv_title);
        tv_title.setText(item.getMovieName());

//        ImageView iv_start=helper.getView(R.id.iv_img);
//        iv_start.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });

    }

}
