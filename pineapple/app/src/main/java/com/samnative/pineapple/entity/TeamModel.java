package com.samnative.pineapple.entity;

import java.io.Serializable;
import java.util.List;

public class TeamModel implements Serializable {


    /**
     * code : 200
     * msg : success
     * data : {"total":"2","list":[{"customerid":"8","headerpic":"","username":"18588445332","addtime":"2021-11-09 14:16:51","series":"1级","levelname":"普通会员"},{"customerid":"7","headerpic":"","username":"18588445333","addtime":"2021-11-09 14:08:22","series":"1级","levelname":"普通会员"}],"total_indirect_number":"0","total_direct_number":"2"}
     */

    private String code;
    private String msg;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * total : 2
         * list : [{"customerid":"8","headerpic":"","username":"18588445332","addtime":"2021-11-09 14:16:51","series":"1级","levelname":"普通会员"},{"customerid":"7","headerpic":"","username":"18588445333","addtime":"2021-11-09 14:08:22","series":"1级","levelname":"普通会员"}]
         * total_indirect_number : 0
         * total_direct_number : 2
         */

        private String total;
        private String total_indirect_number;
        private String total_direct_number;
        private String indirect_enterprise_number;
        private String direct_enterprise_number;
        private List<ListBean> list;

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }


        public String getIndirect_enterprise_number() {
            return indirect_enterprise_number;
        }

        public void setIndirect_enterprise_number(String indirect_enterprise_number) {
            this.indirect_enterprise_number = indirect_enterprise_number;
        }

        public String getDirect_enterprise_number() {
            return direct_enterprise_number;
        }

        public void setDirect_enterprise_number(String direct_enterprise_number) {
            this.direct_enterprise_number = direct_enterprise_number;
        }

        public String getTotal_indirect_number() {
            return total_indirect_number;
        }

        public void setTotal_indirect_number(String total_indirect_number) {
            this.total_indirect_number = total_indirect_number;
        }

        public String getTotal_direct_number() {
            return total_direct_number;
        }

        public void setTotal_direct_number(String total_direct_number) {
            this.total_direct_number = total_direct_number;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean implements Serializable {
            /**
             * customerid : 8
             * headerpic :
             * username : 18588445332
             * addtime : 2021-11-09 14:16:51
             * series : 1级
             * levelname : 普通会员
             */

            private String customerid;
            private String headerpic;
            private String username;
            private String addtime;
            private String series;
            private String nickname;
            private String levelname;

            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }

            public String getCustomerid() {
                return customerid;
            }

            public void setCustomerid(String customerid) {
                this.customerid = customerid;
            }

            public String getHeaderpic() {
                return headerpic;
            }

            public void setHeaderpic(String headerpic) {
                this.headerpic = headerpic;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getAddtime() {
                return addtime;
            }

            public void setAddtime(String addtime) {
                this.addtime = addtime;
            }

            public String getSeries() {
                return series;
            }

            public void setSeries(String series) {
                this.series = series;
            }

            public String getLevelname() {
                return levelname;
            }

            public void setLevelname(String levelname) {
                this.levelname = levelname;
            }
        }
    }
}
