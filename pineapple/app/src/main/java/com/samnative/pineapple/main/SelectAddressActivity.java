package com.samnative.pineapple.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.google.gson.reflect.TypeToken;
import com.samnative.pineapple.adapter.AddressManagerAdapter2;
import com.samnative.pineapple.entity.ReceiveAddressModel;
import com.samnative.pineapple.entity.UserModel;
import com.samnative.pineapple.http.HttpRequest;
import com.samnative.pineapple.http.JsonResult;
import com.samnative.pineapple.instance.AppPrefer;
import com.samnative.pineapple.interfaces.OnDataNotice;
import com.samnative.pineapple.utils.GsonUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 选择地址
 */
public class SelectAddressActivity extends BaseActivity implements OnClickListener, OnItemClickListener {

    private Context context;
    /**
     * 适配器
     */
    private AddressManagerAdapter2 amManager;
    /**
     * 地址列表
     */
    private ListView listView;
    /**
     * 数据
     */
    private List<ReceiveAddressModel> list;
    /**
     * 没有数据时显示的textview
     */
    private LinearLayout no_address;
    private String source;//选择select和管理select,用来区分是否显示操作栏

    @Override
    protected int getLayoutId() {
        return R.layout.activity_select_address;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        context = this;
        findViewById(R.id.tv_back).setOnClickListener(this);
        findViewById(R.id.tv_manager).setOnClickListener(this);
        no_address = (LinearLayout) findViewById(R.id.address_nodata);
        list = new ArrayList<ReceiveAddressModel>();
        source = "select";
        amManager = new AddressManagerAdapter2(context, list, source, new OnDataNotice() {
            @Override
            public void upData() {
                super.upData();
                dataInit();
            }
        });
        listView = (ListView) findViewById(R.id.address_manager_lv);
        listView.setAdapter(amManager);
        listView.setOnItemClickListener(this);
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.tv_back:
                finish();
                break;
            case R.id.tv_manager:
                startActivity(new Intent(context, AddressManagerActivity.class));
                break;
            default:
                break;
        }
    }

    /**
     * 获取收货地址信息
     */
    private void dataInit() {
        // TODO Auto-generated method stub

        Map<String, String> map = new HashMap<String, String>();
        map.put("_apiname", "user.logistics.logisticsList");
        map.put("mtoken", AppPrefer.getInstance(mContext).getString(AppPrefer.mtoken, ""));


        HttpRequest.post(this, "", map, new JsonResult() {
            @Override
            public void reqestSuccess(JSONObject result) {
                try {
                    JSONObject jsonObjectData = result.getJSONObject("data");
                    JSONArray jsonArray = jsonObjectData.getJSONArray("data");
                    if (jsonArray.length() > 0) {
                        Type type = new TypeToken<List<ReceiveAddressModel>>() {
                        }.getType();
                        list = GsonUtils.fromJsonList(jsonArray.toString(), type);
                        setData(list);
                        listView.setVisibility(View.VISIBLE);
                        no_address.setVisibility(View.GONE);
                        UserModel model = AppPrefer.getUserModel(context);
//                        model.getData().getUserinfo().setLogisticsDec("1");
                    } else {
                        listView.setVisibility(View.GONE);
                        no_address.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });


    }

    protected void setData(List<ReceiveAddressModel> list) {
        // TODO Auto-generated method stub
        amManager.setList(list);
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        dataInit();
    }


    /**
     * 点击操作选择收货地址
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // TODO Auto-generated method stub

        // 选择收货地址
        Intent intent = new Intent();
        intent.putExtra("address", list.get(position));
        setResult(100, intent);
        finish();
    }


}
