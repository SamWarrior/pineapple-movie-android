package com.samnative.pineapple.adapter;

import android.support.annotation.LayoutRes;
import android.support.v4.content.ContextCompat;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.samnative.pineapple.entity.Province;
import com.samnative.pineapple.main.R;


/**
 * 选择城市——左边
 */
public class XuanzeCityLeftAdapter extends BaseQuickAdapter<Province, BaseViewHolder> {


    private int currentIndex = -1;

    public int getCurrentIndex() {
        return currentIndex;
    }

    public void setCurrentIndex(int currentIndex) {
        this.currentIndex = currentIndex;
        notifyDataSetChanged();
    }

    public XuanzeCityLeftAdapter(@LayoutRes int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, Province item) {

        RelativeLayout rl_bg = helper.getView(R.id.rl_bg);
        TextView tv_name = helper.getView(R.id.tv_name);

        tv_name.setText(item.getName());
        if (helper.getAdapterPosition() == currentIndex) {
            rl_bg.setBackgroundColor(ContextCompat.getColor(mContext, R.color.main_white));
            tv_name.setTextColor(ContextCompat.getColor(mContext, R.color.main_color));
        } else {
            rl_bg.setBackgroundColor(ContextCompat.getColor(mContext, R.color.main_grey_bg));
            tv_name.setTextColor(ContextCompat.getColor(mContext, R.color.main_tab_text_n));
        }
    }
}