package com.samnative.pineapple.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.samnative.pineapple.main.R;


/**
 * 祝贺提示框
 */
public class ZhuheDialog extends AlertDialog {

    private Context context;
    private ImageView tv_cancel;
    private TextView tv_text1;
    private TextView tv_title;

    private String title, content;

    public ZhuheDialog(Context context, String title, String content) {
        super(context);
        this.context = context;
        this.title = title;
        this.content = content;
    }

    @Override
    public void show() {
        super.show();
        Window window = this.getWindow();
        View view = LayoutInflater.from(context).inflate(
                R.layout.dialog_zhuhe, null);
        tv_title = view.findViewById(R.id.tv_title);
        tv_title.setText(title);
        tv_text1 = (TextView) view.findViewById(R.id.tv_text1);
        tv_text1.setText(content);
        tv_cancel = (ImageView) view.findViewById(R.id.tv_cancel);
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });

        window.setContentView(view);
        window.setBackgroundDrawableResource(android.R.color.transparent);
    }


}
