package com.samnative.pineapple.entity;

/**
 * 热门预告
 */
public class HomeYugaoModel {


    /**
     * _id : 624994996edc8d3ee3dda962
     * movieId : y002
     * movieName : 被害人
     * movieUrl : https://vod.pipi.cn/fec9203cvodtransbj1251246104/901f65d0387702293218091481/v.f42905.mp4
     * cover : /uploads/e8d98fc4db12ade6e8f565d7764c0283
     * __v : 0
     */

    private String _id;
    private String movieId;
    private String movieName;
    private String movieUrl;
    private String cover;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getMovieId() {
        return movieId;
    }

    public void setMovieId(String movieId) {
        this.movieId = movieId;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public String getMovieUrl() {
        return movieUrl;
    }

    public void setMovieUrl(String movieUrl) {
        this.movieUrl = movieUrl;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }
}
