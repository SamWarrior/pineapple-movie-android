package com.samnative.pineapple.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.samnative.pineapple.entity.BankcardModel;
import com.samnative.pineapple.main.R;

/**
 * 银行卡列表适配器
 *
 * @author LiuKai
 */
public class ChooseBankcardAdapter extends BaseQuickAdapter<BankcardModel, BaseViewHolder> {

    private int index = -1;


    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
        notifyDataSetChanged();
    }

    public ChooseBankcardAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, BankcardModel item) {

        ImageView iv_bankcard_ic = helper.getView(R.id.iv_bankcard_ic);
        ImageView iv_check_bankcard = helper.getView(R.id.iv_check_bankcard);
        TextView tv_bankname = helper.getView(R.id.tv_bankname);
        TextView tv_cardtype = helper.getView(R.id.tv_cardtype);

        tv_bankname.setText(item.getBank_name());
        String cardType = item.getAccount_type().equals("1") ? "个人账户" : "对公账户";
        String account_number = item.getAccount_number();
        String cardNum = account_number.length() > 4 ? account_number
                .substring(account_number.length() - 4, account_number.length())
                : account_number;
        tv_cardtype.setText(cardType + "(尾号" + cardNum + ")");
//		holder.iv_bankcard_ic.setImageResource(getIcon(model.getBank_name()));
        if (index == helper.getAdapterPosition()) {
            iv_check_bankcard.setVisibility(View.VISIBLE);
        } else {
            iv_check_bankcard.setVisibility(View.GONE);
        }
    }


}
