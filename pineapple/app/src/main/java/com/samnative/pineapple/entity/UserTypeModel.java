package com.samnative.pineapple.entity;

import java.util.List;

/**
 * Created by Wy on 2019/5/27.
 */

public class UserTypeModel  {

    /**
     * code : 200
     * msg : success
     * data : {"list":[{"typeid":"1","name":"应用医院/医生","enable":"0"},{"typeid":"2","name":"学术界院士/教授","enable":"0"},{"typeid":"3","name":"科研机构/科学家","enable":"1"},{"typeid":"4","name":"细胞存储单位/生产商","enable":"0"},{"typeid":"5","name":"普通用户/患者","enable":"0"}],"isexam":"1"}
     */

    private String code;
    private String msg;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * list : [{"typeid":"1","name":"应用医院/医生","enable":"0"},{"typeid":"2","name":"学术界院士/教授","enable":"0"},{"typeid":"3","name":"科研机构/科学家","enable":"1"},{"typeid":"4","name":"细胞存储单位/生产商","enable":"0"},{"typeid":"5","name":"普通用户/患者","enable":"0"}]
         * isexam : 1
         */

        private String isexam;
        private List<ListBean> list;

        public String getIsexam() {
            return isexam;
        }

        public void setIsexam(String isexam) {
            this.isexam = isexam;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean {
            /**
             * typeid : 1
             * name : 应用医院/医生
             * enable : 0
             */

            private String typeid;
            private String name;
            private String enable;

            public String getTypeid() {
                return typeid;
            }

            public void setTypeid(String typeid) {
                this.typeid = typeid;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getEnable() {
                return enable;
            }

            public void setEnable(String enable) {
                this.enable = enable;
            }
        }
    }
}
