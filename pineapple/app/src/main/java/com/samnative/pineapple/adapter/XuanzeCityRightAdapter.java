package com.samnative.pineapple.adapter;

import android.support.annotation.LayoutRes;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.samnative.pineapple.entity.City;
import com.samnative.pineapple.main.R;


/**
 * 选择城市的右边
 */
public class XuanzeCityRightAdapter extends BaseQuickAdapter<City, BaseViewHolder> {


    public XuanzeCityRightAdapter(@LayoutRes int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, City item) {

        TextView tv_name = helper.getView(R.id.tv_name);
        tv_name.setText(item.getName());

    }
}