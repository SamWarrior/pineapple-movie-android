package com.samnative.pineapple.widget;

import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.samnative.pineapple.main.R;


/**
 * 智能排序,应用医院主页和预约主页
 */
public class ZhinengSortPop extends PopupWindow implements OnClickListener {

    private Context context;
    /**
     * 有效区域
     */
    private View view;
    /**
     * 监听接口对象
     */
    private OnSelect select;
    /**
     * 阴影层
     */
    private View viewShade;
    /**
     * 选项的按钮
     */
    private TextView tv_sort1, tv_sort2, tv_sort3, tv_sort4;

    /**
     * 监听选中
     *
     * @author WangPeng
     */
    public interface OnSelect {
        public void getChoose(String sortName, String sortId);
    }

    public ZhinengSortPop(Context context, OnSelect select) {
        this.context = context;
        this.select = select;
        LayoutInflater inflater = LayoutInflater.from(context);
        view = inflater.inflate(R.layout.pop_zhineng_sort, null);
        viewShade = view.findViewById(R.id.v_pop_shade);

        // 点击有效区域外则退出pop
        view.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int hight = view.findViewById(R.id.pop_layout).getBottom();
                int point = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (point > hight) {
                        dismiss();
                    }
                }
                return true;
            }
        });
        dataInit();
        setContentView(view);
        setWidth(LayoutParams.MATCH_PARENT);
        setHeight(LayoutParams.WRAP_CONTENT);
        setFocusable(true);
        // setAnimationStyle(R.style.AnimTop);
        setBackgroundDrawable(new ColorDrawable(0x00000000));
        setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }

    private void dataInit() {
        tv_sort1 = (TextView) view.findViewById(R.id.tv_sort1);
        tv_sort1.setOnClickListener(this);
        tv_sort2 = (TextView) view.findViewById(R.id.tv_sort2);
        tv_sort2.setOnClickListener(this);
        tv_sort3 = (TextView) view.findViewById(R.id.tv_sort3);
        tv_sort3.setOnClickListener(this);
        tv_sort4 = (TextView) view.findViewById(R.id.tv_sort4);
        tv_sort4.setOnClickListener(this);
    }

    @Override
    public void showAsDropDown(View anchor, int xoff, int yoff) {
        // TODO Auto-generated method stub

        if (Build.VERSION.SDK_INT == 24) {
            Rect rect = new Rect();
            anchor.getGlobalVisibleRect(rect);
            int h = anchor.getResources().getDisplayMetrics().heightPixels - rect.bottom;
            setHeight(h);
        }

        super.showAsDropDown(anchor, xoff, yoff);
        viewShade.setVisibility(View.GONE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
                anim.setDuration(300);
                viewShade.startAnimation(anim);
                viewShade.setVisibility(View.VISIBLE);
            }
        }, 300);
    }

    @Override
    public void dismiss() {
        // TODO Auto-generated method stub
        AlphaAnimation anim = new AlphaAnimation(1.0f, 0.0f);
        anim.setDuration(200);
        anim.setAnimationListener(new AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // TODO Auto-generated method stub
                viewShade.setVisibility(View.GONE);
                ZhinengSortPop.super.dismiss();
            }
        });
        viewShade.startAnimation(anim);
    }

    // ==========================================================点击监听

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.tv_sort1:
                if (select != null) {
                    select.getChoose("智能排序", "1");
                }
                setSelectColor((TextView) v);
                break;
            case R.id.tv_sort2:
                if (select != null) {
                    select.getChoose("已备案", "2");
                }
                setSelectColor((TextView) v);
                break;
            case R.id.tv_sort3:
                if (select != null) {
                    select.getChoose("离我最近", "3");
                }
                setSelectColor((TextView) v);
                break;
            case R.id.tv_sort4:
                if (select != null) {
                    select.getChoose("预约最高", "4");
                }
                setSelectColor((TextView) v);
                break;
            default:
                break;
        }
        dismiss();
    }

    /**
     * 设置当前选中按钮颜色
     *
     * @param tv
     */
    public void setSelectColor(TextView tv) {
        tv_sort1.setTextColor(context.getResources().getColor(
                R.color.main_tab_text_n));
        tv_sort1.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);
        tv_sort2.setTextColor(context.getResources().getColor(
                R.color.main_tab_text_n));
        tv_sort2.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);
        tv_sort3.setTextColor(context.getResources().getColor(
                R.color.main_tab_text_n));
        tv_sort3.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);
        tv_sort4.setTextColor(context.getResources().getColor(
                R.color.main_tab_text_n));
        tv_sort4.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);
        tv.setTextColor(context.getResources().getColor(R.color.main_color));
        tv.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.mipmap.ic_tab_check, 0);
    }
}
