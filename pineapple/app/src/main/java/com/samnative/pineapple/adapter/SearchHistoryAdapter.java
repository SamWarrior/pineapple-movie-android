package com.samnative.pineapple.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.samnative.pineapple.main.R;

import java.util.List;

/**
 * 搜索历史列表适配器
 * 
 * @author WangPeng
 * 
 *         2017-2-28
 * 
 */
public class SearchHistoryAdapter extends BaseAdapter {

	private Context context;
	private List<String> listData;

	public SearchHistoryAdapter(Context context, List<String> listData) {
		this.context = context;
		this.listData = listData;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listData.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return listData.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHodler vHodler = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.item_search_history, null);
			vHodler = new ViewHodler(convertView);
			convertView.setTag(vHodler);
		} else {
			vHodler = (ViewHodler) convertView.getTag();
		}
		vHodler.tv_item_title.setText(listData.get(position));
		return convertView;
	}

	private class ViewHodler {

		private TextView tv_item_title;

		public ViewHodler(View v) {
			tv_item_title = (TextView) v.findViewById(R.id.tv_item_title);
		}
	}

}
