package com.samnative.pineapple.widget;

import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.samnative.pineapple.main.R;

/**
 * 选择银行账户类型
 *
 */
public class ChooseBankAccoutTypePop extends PopupWindow {

    /**
     * 有效区域
     */
    private View view;
    /**
     * 选择监听对象
     */
    private ChooseSelect mChoose;
    /**
     * 阴影层
     */
    private View viewShade;
    /**
     * 0显示企业账户 1显示公司账户
     */
    private int isAccoutName = 0;

    public interface ChooseSelect {

        public void current(int index);
    }

    public void setIsAccoutName(int isAccoutName) {
        this.isAccoutName = isAccoutName;
    }

    public ChooseBankAccoutTypePop(Context context, int isAccoutName,
                                   ChooseSelect chooseSelect) {
        this.isAccoutName = isAccoutName;
        mChoose = chooseSelect;
        LayoutInflater inflater = LayoutInflater.from(context);
        view = inflater.inflate(R.layout.pop_choose_accouttype, null);
        viewShade = view.findViewById(R.id.v_pop_shade);
        view.findViewById(R.id.pop_close).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismiss();
                    }
                });

        // 点击有效区域外则退出pop
        view.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int hight = view.findViewById(R.id.pop_layout).getTop();
                int point = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (point < hight) {
                        dismiss();
                    }
                }
                return true;
            }
        });

        TextView tv1 = (TextView) view.findViewById(R.id.tv_text1);
        tv1.setText("个人账户");
        tv1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dismiss();
                if (mChoose != null) {
                    mChoose.current(1);
                }
            }
        });

        TextView tv2 = (TextView) view.findViewById(R.id.tv_text2);
        if (isAccoutName == 0) {
            tv2.setText("企业账户");
        } else {
            tv2.setText("公司账户");
        }
        tv2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dismiss();
                if (mChoose != null) {
                    mChoose.current(2);
                }
            }
        });

        setContentView(view);
        setWidth(LayoutParams.MATCH_PARENT);
        setHeight(LayoutParams.MATCH_PARENT);
        setFocusable(true);
        setAnimationStyle(R.style.AnimBottom);
        setBackgroundDrawable(new ColorDrawable(0x00000000));
        setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }

    @Override
    public void showAtLocation(View parent, int gravity, int x, int y) {
        // TODO Auto-generated method stub
        viewShade.setVisibility(View.GONE);
        super.showAtLocation(parent, gravity, x, y);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
                anim.setDuration(200);
                viewShade.startAnimation(anim);
                viewShade.setVisibility(View.VISIBLE);
            }
        }, 300);

    }

    @Override
    public void dismiss() {
        // TODO Auto-generated method stub
        AlphaAnimation anim = new AlphaAnimation(1.0f, 0.0f);
        anim.setDuration(200);
        anim.setAnimationListener(new AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // TODO Auto-generated method stub
                viewShade.setVisibility(View.GONE);
                ChooseBankAccoutTypePop.super.dismiss();
            }
        });
        viewShade.startAnimation(anim);
    }

}
