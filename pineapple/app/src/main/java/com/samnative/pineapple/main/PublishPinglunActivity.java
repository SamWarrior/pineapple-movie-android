package com.samnative.pineapple.main;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.gyf.immersionbar.ImmersionBar;
import com.samnative.pineapple.entity.UserModel;
import com.samnative.pineapple.http.HttpRequest;
import com.samnative.pineapple.http.JsonResult;
import com.samnative.pineapple.instance.AppPrefer;
import com.shashank.sony.fancytoastlib.FancyToast;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * 发表评论
 */
public class PublishPinglunActivity extends BaseActivity implements View.OnClickListener {


    private EditText et_content;
    private String movieid;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_publish_pinglun;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        ImmersionBar.with(this).statusBarDarkFont(true).init();
        movieid = getIntent().getStringExtra("movieid");
        find(R.id.tv_back).setOnClickListener(this);
        find(R.id.btn_submit).setOnClickListener(this);
        et_content = find(R.id.et_content);
    }


    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
    }


    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.tv_back:
                finish();
                break;
            case R.id.btn_submit:

                if (et_content.getText().toString().isEmpty()) {
                    FancyToast.showToast(mContext, "请填写评论内容", FancyToast.WARNING, false);
                    break;
                }

                requestData();

                break;
            default:
                break;
        }

    }

    public void requestData() {
        Map<String, String> map = new HashMap<>();
        map.put("movieId", movieid);
        map.put("content", et_content.getText().toString());
        HttpRequest.request(this, "/client/comments/sendcomment", "", map, "POST", new JsonResult() {
            @Override
            public void reqestSuccess(JSONObject result) {
                FancyToast.showToast(mContext, "提交成功", FancyToast.SUCCESS, false);
                finish();
            }

            @Override
            public void requestFailure(int code, String msg) {
                super.requestFailure(code, msg);
            }

            @Override
            public void requestFinish() {
                super.requestFinish();
            }
        });
    }


}
