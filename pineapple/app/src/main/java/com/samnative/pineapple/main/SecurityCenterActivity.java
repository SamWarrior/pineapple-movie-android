package com.samnative.pineapple.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.samnative.pineapple.http.HttpRequest;
import com.samnative.pineapple.http.JsonResult;
import com.shashank.sony.fancytoastlib.FancyToast;
import com.samnative.pineapple.entity.UserModel;
import com.samnative.pineapple.instance.AppPrefer;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * 安全中心
 * Created by Wy on 2018/4/26.
 */

public class SecurityCenterActivity extends BaseActivity implements View.OnClickListener {

    /**
     * 支付密码是否设置
     */
    private TextView tv_pay_set;

    /**
     * 登录密码是否设置
     */
    private TextView tv_login_set;

    /**
     * 用户信息
     */
    private UserModel userModel;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_ssecurity_center;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        find(R.id.rl_back).setOnClickListener(this);
        find(R.id.rl_modify).setOnClickListener(this);
        find(R.id.rl_modify2).setOnClickListener(this);
        tv_pay_set = find(R.id.tv_pay_set);
        tv_login_set = find(R.id.tv_login_set);

        try {
            userModel = AppPrefer.getUserModel(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (userModel != null) {

            if (userModel.getData().getPayDec().equals("1")) {
                tv_pay_set.setText(getString(R.string.set));
                tv_pay_set.setTextColor(getResources().getColor(R.color.main_tab_text_n3));
            } else {
                tv_pay_set.setText(getString(R.string.unset));
                tv_pay_set.setTextColor(getResources().getColor(R.color.main_tab_text_n3));
            }

            if (userModel.getData().getIsloginpwd().equals("1")) {
                tv_login_set.setText(getString(R.string.set));
                tv_login_set.setTextColor(getResources().getColor(R.color.main_tab_text_n3));
            } else {
                tv_login_set.setText(getString(R.string.unset));
                tv_login_set.setTextColor(getResources().getColor(R.color.main_tab_text_n3));
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_back:
                finish();
                break;
            case R.id.rl_modify://登录密码
                if (userModel == null) {
                    FancyToast.showToast(mContext, getString(R.string.unlogin), FancyToast.WARNING, false);
                    return;
                }
                if (userModel.getData().getIsloginpwd().equals("1")) {
                    startActivity(new Intent()
                            .putExtra("type", 1)
                            .setClass(mContext, PasswordLoginVerityActivity.class));
                } else {
                    startActivity(new Intent()
                            .setClass(mContext, PasswordLoginSetActivity.class));
                }
                break;
            case R.id.rl_modify2://支付密码
                if (userModel == null) {
                    FancyToast.showToast(mContext, getString(R.string.unlogin), FancyToast.WARNING, false);
                    return;
                }
                if (userModel.getData().getPayDec().equals("1")) {
                    Intent intent = new Intent().setClass(mContext, PasswordLoginVerityActivity.class);
                    intent.putExtra("type", 4);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent().setClass(mContext, PasswordPaySetActivity.class);
                    startActivity(intent);
                }
                break;
            default:
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshUserino();
    }

    /**
     * 刷新用户信息
     */
    public void refreshUserino() {
        if (AppPrefer.getInstance(mContext).getString(AppPrefer.mtoken, "").isEmpty()) {
            return;
        }
        Map<String, String> map = new HashMap<>();
        map.put("_apiname", "user.index.main");
        map.put("mtoken", AppPrefer.getInstance(mContext).getString(AppPrefer.mtoken, ""));
        HttpRequest.post(this, "更新信息中", map, new JsonResult() {
            @Override
            public void reqestSuccess(JSONObject result) {
                // TODO Auto-generated method stub
                try {
                    AppPrefer.getInstance(mContext).put(AppPrefer.userinfo, result.toString());
                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
            }
        });
    }
}
