package com.samnative.pineapple.widget;


import android.content.Context;

import com.bigkoo.pickerview.OptionsPickerView;
import com.samnative.pineapple.entity.City;
import com.samnative.pineapple.entity.County;
import com.samnative.pineapple.entity.Province;
import com.samnative.pineapple.utils.GsonUtils;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * 封装好的城市三级联动/两级联动选择器
 */
public class CityPicker {

    private Context context;
    private OptionsPickerView pvOptions;
    //省市区列表
    private ArrayList<Province> listProvince;
    private ArrayList<ArrayList<City>> listCity;
    private ArrayList<ArrayList<ArrayList<County>>> listCounty;


    //===============================

    public OnChangeCity onChangeCity;

    public void setOnChangeCity(OnChangeCity onChangeCity) {
        this.onChangeCity = onChangeCity;
    }

    public interface OnChangeCity {

        public void callCityInfo(String province, String city, String county, String areacode);

    }

    //================================

    public CityPicker(Context context) {
        this.context = context;
        if (listCounty == null) {
            String json = getFromAssets("area.json");
            if (json.length() > 1) {

                List<Province> listProvincetemp = GsonUtils.fromJsonList(json,
                        new TypeToken<List<Province>>() {
                        }.getType());
                listProvince = (ArrayList) listProvincetemp;
                listCity = new ArrayList<>();
                listCounty = new ArrayList<>();
                for (int i = 0; i < listProvince.size(); i++) {
                    ArrayList<City> city = listProvince.get(i).getArea();
                    listCity.add(city);
                    ArrayList<ArrayList<County>> list = new ArrayList<>();
                    for (int j = 0; j < city.size(); j++) {
                        ArrayList<County> county = city.get(j).getArea();
                        list.add(county);
                    }
                    listCounty.add(list);
                }

                setDate1();
            }
        }
    }

    public void setShowType(int type) {
        if (type == 1) {
            setDate1();
        } else {
            setDate2();
        }
    }

    /**
     * 第一种形式省市区三级
     */
    private void setDate1() {
        pvOptions = new OptionsPickerView<>(context);
        pvOptions.setPicker(listProvince, listCity, listCounty, true);
        pvOptions.setTitle("请选择");
        pvOptions.setCyclic(false, false, false);
        pvOptions.setSelectOptions(0, 0, 0);
        pvOptions.setOnoptionsSelectListener(new OptionsPickerView.OnOptionsSelectListener() {

            @Override
            public void onOptionsSelect(int options1, int option2,
                                        int options3) {
                // TODO Auto-generated method stub

                try {
                    String province = listProvince.get(options1)
                            .getName();
                    String city = listCity.get(options1).get(option2)
                            .getName();
                    String county = listCounty.get(options1)
                            .get(option2).get(options3).getName();
                    int areacode = listCounty.get(options1).get(option2).get(options3).getId();

                    if (onChangeCity != null) {
                        onChangeCity.callCityInfo(province, city, county, String.valueOf(areacode));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

    }

    /**
     * 第二种形式省市两级
     */
    private void setDate2() {
        pvOptions = new OptionsPickerView<>(context);
        pvOptions.setPicker(listProvince, listCity, true);
        pvOptions.setTitle("请选择");
        pvOptions.setCyclic(false, false, false);
        pvOptions.setSelectOptions(0, 0);
        pvOptions.setOnoptionsSelectListener(new OptionsPickerView.OnOptionsSelectListener() {

            @Override
            public void onOptionsSelect(int options1, int option2,
                                        int options3) {
                // TODO Auto-generated method stub

                try {
                    String province = listProvince.get(options1)
                            .getName();
                    String city = listCity.get(options1).get(option2)
                            .getName();
                    int areacode = listCity.get(options1).get(option2).getId();

                    if (onChangeCity != null) {
                        onChangeCity.callCityInfo(province, city, "", String.valueOf(areacode));
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

    }

    /**
     * 读取本地省市区三级列表
     *
     * @param fileName
     * @return
     */
    public String getFromAssets(String fileName) {
        try {
            InputStreamReader inputReader = new InputStreamReader(
                    context.getResources().getAssets().open(fileName));
            BufferedReader bufReader = new BufferedReader(inputReader);
            String line = "";
            String Result = "";
            while ((line = bufReader.readLine()) != null)
                Result += line;
            return Result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    /**
     * show弹窗
     */
    public void showCityPicker() {
        if (pvOptions != null) {
            pvOptions.show();
        }
    }

}
