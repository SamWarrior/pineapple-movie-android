package com.samnative.pineapple.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.samnative.pineapple.entity.UserTypeModel;
import com.samnative.pineapple.main.R;

import java.util.List;


/**
 * 地区列表适配器
 */
public class ChooseAreaAdapter extends BaseAdapter {

    private Context mContext;
    private List<UserTypeModel.DataBean.ListBean> list;

    public ChooseAreaAdapter(Context context, List<UserTypeModel.DataBean.ListBean> list) {
        // TODO Auto-generated constructor stub
        mContext = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return list != null ? list.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return list != null ? list.get(position) : 0;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final ViewHolder holder;
        UserTypeModel.DataBean.ListBean model = list.get(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(
                    R.layout.item_choose_coin, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tv_name.setText(model.getName());

        if (model.getEnable().equals("0")) {
            holder.tv_name.setTextColor(mContext.getResources().getColor(R.color.main_tab_text_n3));
        } else {
            holder.tv_name.setTextColor(mContext.getResources().getColor(R.color.main_tab_text_n));
        }
        return convertView;
    }


    public class ViewHolder {
        private TextView tv_name;

        public ViewHolder(View v) {
            tv_name = (TextView) v.findViewById(R.id.tv_name);
        }
    }


}
