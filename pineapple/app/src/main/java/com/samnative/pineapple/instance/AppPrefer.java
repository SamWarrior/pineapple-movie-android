package com.samnative.pineapple.instance;

import android.content.Context;
import android.text.TextUtils;

import com.samnative.pineapple.entity.UserModel;
import com.samnative.pineapple.utils.GsonUtils;

import net.grandcentrix.tray.AppPreferences;

/**
 * Created by Administrator on 2019/5/16.
 * <p>
 * 轻量级数据存取
 */

public class AppPrefer {

    public static AppPreferences appPreferences = null;

    public static AppPreferences getInstance(Context context) {
        if (appPreferences == null) {
            appPreferences = new AppPreferences(context);
        }
        return appPreferences;
    }

    /**
     * token
     */
    public static final String mtoken = "mtoken";
    /**
     * userinfo
     */
    public static final String userinfo = "userinfo";

    /**
     * 个人信息
     *
     * @param context
     * @return
     */
    public static UserModel getUserModel(Context context) {
        AppPreferences appPreferences = new AppPreferences(context);
        UserModel model = null;
        if (context != null && !TextUtils.isEmpty(appPreferences.getString(AppPrefer.userinfo, ""))) {
            model = GsonUtils.fromJson(appPreferences.getString(AppPrefer.userinfo, ""), UserModel.class);
        }
        return model;
    }


    /**
     * 首次打开APP生成UUID的key
     */
    public static final String _uuid = "_uuid";

    /**
     * 记录设备屏幕分辨率的key
     */
    public static final String _dpi = "_dpi";

    /**
     * 存储用户切换的城市名称的key
     */
    public static final String currentCity = "currentCity";

    /**
     * 存储用户切换的城市编号的key
     */
    public static final String currentCityCode = "currentCityCode";

    /**
     * 储存用户是否同意隐私协议的key
     */
    public static final String isPrivacyPolicy = "isPrivacyPolicy";

    /**
     *  储存APP是否是第一次启动
     */
    public static final String isFristStart = "isFristStart";
    /**
     * 融云token
     */
    public static String rtoken = "rtoken";
    /**
     * 融云userid
     */
    public static String ruserid = "ruserid";
}
