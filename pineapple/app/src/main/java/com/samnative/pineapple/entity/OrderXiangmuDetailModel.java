package com.samnative.pineapple.entity;

import java.util.List;

/**
 * 项目订单详情
 */
public class OrderXiangmuDetailModel {


    /**
     * id : 30
     * orderno : SER20211209155511561845
     * customerid : 7
     * serviceid : 23
     * service_sum : 1
     * msg_status : 0
     * pay_type : 1
     * totalamount : 1.00
     * totalservice : 0
     * pay_amount : 1.00
     * pay_service : 0
     * prouctprice : 1.00
     * lineprice : 1.00
     * evaluate : -1
     * pay_status : 1
     * orderstatus : 1
     * canceltype : 0
     * addtime : 2021-12-09 15:55:11
     * remark :
     * thumb : http://yjproject.oss-cn-shenzhen.aliyuncs.com/fzt/images/2021-11-16/1637056965ze418.png?x-oss-process=image/quality,q_80
     * title : 知识产权
     * pay_type_str : 余额支付
     * orderstatus_str : 服务中
     * canceltype_str :
     * orderact : [{"act":"1","acttype":"2","actname":"结束服务"}]
     */

    private String id;
    private String orderno;
    private String customerid;
    private String serviceid;
    private String service_sum;
    private String msg_status;
    private String pay_type;
    private String totalamount;
    private String totalservice;
    private String pay_amount;
    private String pay_service;
    private String prouctprice;
    private String lineprice;
    private String evaluate;
    private String pay_status;
    private String orderstatus;
    private String canceltype;
    private String addtime;
    private String remark;
    private String thumb;
    private String title;
    private String pay_type_str;
    private String orderstatus_str;
    private String canceltype_str;
    private List<OrderactBean> orderact;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getCustomerid() {
        return customerid;
    }

    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }

    public String getServiceid() {
        return serviceid;
    }

    public void setServiceid(String serviceid) {
        this.serviceid = serviceid;
    }

    public String getService_sum() {
        return service_sum;
    }

    public void setService_sum(String service_sum) {
        this.service_sum = service_sum;
    }

    public String getMsg_status() {
        return msg_status;
    }

    public void setMsg_status(String msg_status) {
        this.msg_status = msg_status;
    }

    public String getPay_type() {
        return pay_type;
    }

    public void setPay_type(String pay_type) {
        this.pay_type = pay_type;
    }

    public String getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(String totalamount) {
        this.totalamount = totalamount;
    }

    public String getTotalservice() {
        return totalservice;
    }

    public void setTotalservice(String totalservice) {
        this.totalservice = totalservice;
    }

    public String getPay_amount() {
        return pay_amount;
    }

    public void setPay_amount(String pay_amount) {
        this.pay_amount = pay_amount;
    }

    public String getPay_service() {
        return pay_service;
    }

    public void setPay_service(String pay_service) {
        this.pay_service = pay_service;
    }

    public String getProuctprice() {
        return prouctprice;
    }

    public void setProuctprice(String prouctprice) {
        this.prouctprice = prouctprice;
    }

    public String getLineprice() {
        return lineprice;
    }

    public void setLineprice(String lineprice) {
        this.lineprice = lineprice;
    }

    public String getEvaluate() {
        return evaluate;
    }

    public void setEvaluate(String evaluate) {
        this.evaluate = evaluate;
    }

    public String getPay_status() {
        return pay_status;
    }

    public void setPay_status(String pay_status) {
        this.pay_status = pay_status;
    }

    public String getOrderstatus() {
        return orderstatus;
    }

    public void setOrderstatus(String orderstatus) {
        this.orderstatus = orderstatus;
    }

    public String getCanceltype() {
        return canceltype;
    }

    public void setCanceltype(String canceltype) {
        this.canceltype = canceltype;
    }

    public String getAddtime() {
        return addtime;
    }

    public void setAddtime(String addtime) {
        this.addtime = addtime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPay_type_str() {
        return pay_type_str;
    }

    public void setPay_type_str(String pay_type_str) {
        this.pay_type_str = pay_type_str;
    }

    public String getOrderstatus_str() {
        return orderstatus_str;
    }

    public void setOrderstatus_str(String orderstatus_str) {
        this.orderstatus_str = orderstatus_str;
    }

    public String getCanceltype_str() {
        return canceltype_str;
    }

    public void setCanceltype_str(String canceltype_str) {
        this.canceltype_str = canceltype_str;
    }

    public List<OrderactBean> getOrderact() {
        return orderact;
    }

    public void setOrderact(List<OrderactBean> orderact) {
        this.orderact = orderact;
    }

    public static class OrderactBean {
        /**
         * act : 1
         * acttype : 2
         * actname : 结束服务
         */

        private String act;
        private String acttype;
        private String actname;

        public String getAct() {
            return act;
        }

        public void setAct(String act) {
            this.act = act;
        }

        public String getActtype() {
            return acttype;
        }

        public void setActtype(String acttype) {
            this.acttype = acttype;
        }

        public String getActname() {
            return actname;
        }

        public void setActname(String actname) {
            this.actname = actname;
        }
    }
}
