package com.samnative.pineapple.widget;

import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.samnative.pineapple.main.R;

/**
 * 选择店铺排序
 *
 * @author WangPeng
 *         <p>
 *         2017-3-6
 */
public class ProductTypeChoosePop extends PopupWindow implements
        OnClickListener {

    private Context context;
    /**
     * 有效区域
     */
    private View view;
    /**
     * 监听接口对象
     */
    private OnSelect select;
    /**
     * 阴影层
     */
    private View viewShade;

    private TextView tv_type_all, tv_type_cash, tv_type_jindou;

    /**
     * 监听选中
     *
     * @author WangPeng
     */
    public interface OnSelect {
        public void getChoose(String sortName, int sortId);
    }

    public ProductTypeChoosePop(Context context, OnSelect select) {
        this.context = context;
        this.select = select;
        LayoutInflater inflater = LayoutInflater.from(context);
        view = inflater.inflate(R.layout.pop_choose_product_type, null);
        viewShade = view.findViewById(R.id.v_pop_shade);

        // 点击有效区域外则退出pop
        view.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int hight = view.findViewById(R.id.pop_layout).getBottom();
                int point = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (point > hight) {
                        dismiss();
                    }
                }
                return true;
            }
        });
        dataInit();
        setContentView(view);
        setWidth(LayoutParams.MATCH_PARENT);
        setHeight(LayoutParams.WRAP_CONTENT);
        setFocusable(true);
        // setAnimationStyle(R.style.AnimTop);
        setBackgroundDrawable(new ColorDrawable(0x00000000));
        setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }

    private void dataInit() {
        tv_type_all = (TextView) view.findViewById(R.id.tv_type_all);
        tv_type_all.setOnClickListener(this);
        tv_type_cash = (TextView) view.findViewById(R.id.tv_type_cash);
        tv_type_cash.setOnClickListener(this);
        tv_type_jindou = (TextView) view.findViewById(R.id.tv_type_jindou);
        tv_type_jindou.setOnClickListener(this);
    }

    @Override
    public void showAsDropDown(View anchor, int xoff, int yoff) {
        // TODO Auto-generated method stub

        if (Build.VERSION.SDK_INT == 24) {
            Rect rect = new Rect();
            anchor.getGlobalVisibleRect(rect);
            int h = anchor.getResources().getDisplayMetrics().heightPixels - rect.bottom;
            setHeight(h);
        }

        super.showAsDropDown(anchor, xoff, yoff);
        viewShade.setVisibility(View.GONE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
                anim.setDuration(300);
                viewShade.startAnimation(anim);
                viewShade.setVisibility(View.VISIBLE);
            }
        }, 300);
    }

    @Override
    public void dismiss() {
        // TODO Auto-generated method stub
        AlphaAnimation anim = new AlphaAnimation(1.0f, 0.0f);
        anim.setDuration(200);
        anim.setAnimationListener(new AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // TODO Auto-generated method stub
                viewShade.setVisibility(View.GONE);
                ProductTypeChoosePop.super.dismiss();
            }
        });
        viewShade.startAnimation(anim);
    }

    // ==========================================================点击监听

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.tv_type_all:
                if (select != null) {
                    select.getChoose("全部", 0);
                }
                break;
            case R.id.tv_type_cash:
                if (select != null) {
                    select.getChoose("现金专区", 2);
                }
                break;
            case R.id.tv_type_jindou:
                if (select != null) {
                    select.getChoose("现金+积分专区", 3);
                }
                break;
            default:
                break;
        }
        dismiss();
    }
}
