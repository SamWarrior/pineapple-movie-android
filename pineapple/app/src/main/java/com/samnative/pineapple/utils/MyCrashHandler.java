package com.samnative.pineapple.utils;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;


/**
 * 程序异常崩溃日志捕捉
 * <p>
 * <p>
 * <p>
 * application中使用
 * public class MyApp extends Application {
 *
 * @Override public void onCreate() {
 * super.onCreate();
 * MyCrashHandler handler = new MyCrashHandler();
 * Thread.setDefaultUncaughtExceptionHandler(handler);
 * }
 * }
 */
public class MyCrashHandler implements Thread.UncaughtExceptionHandler {

    private Context context;

    public MyCrashHandler(Context context) {
        this.context = context;
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        Log.e("程序出现异常了", "Thread = " + t.getName() + "\nThrowable = " + e.getMessage());
        String stackTraceInfo = getStackTraceInfo(e);
        Log.e("stackTraceInfo", stackTraceInfo);
        saveThrowableMessage(stackTraceInfo);
    }

    /**
     * 获取错误的信息
     *
     * @param throwable
     * @return
     */
    private String getStackTraceInfo(final Throwable throwable) {
        PrintWriter pw = null;
        Writer writer = new StringWriter();
        try {
            pw = new PrintWriter(writer);
            throwable.printStackTrace(pw);
        } catch (Exception e) {
            return "";
        } finally {
            if (pw != null) {
                pw.close();
            }
        }
        return writer.toString();
    }

    /**
     * 日志保存路径
     */
    private String logFilePath = Environment.getExternalStorageDirectory() + File.separator + "yunji" +
            File.separator + File.separator + "crashLog";

    /**
     * 保存错误日志
     *
     * @param errorMessage
     */
    private void saveThrowableMessage(String errorMessage) {
        if (TextUtils.isEmpty(errorMessage)) {
            return;
        }
        File file = new File(logFilePath);
        if (!file.exists()) {
            boolean mkdirs = file.mkdirs();
            if (mkdirs) {
                writeStringToFile(errorMessage, file);
            }
        } else {
            writeStringToFile(errorMessage, file);
        }
    }

    /**
     * 日志.txt保存到本地储存卡
     *
     * @param errorMessage
     * @param file
     */
    private void writeStringToFile(final String errorMessage, final File file) {


        new Thread(new Runnable() {
            @Override
            public void run() {
                FileOutputStream outputStream = null;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("model", Build.BRAND + "_" + Build.MODEL);
                    jsonObject.put("systemver", "Android_" + Build.VERSION.RELEASE);
                    jsonObject.put("time", DateFormatUtils.long2Str(System.currentTimeMillis(), true));
                    jsonObject.put("crashlog", errorMessage);

                    ByteArrayInputStream inputStream = new ByteArrayInputStream(jsonObject.toString().getBytes());
                    outputStream = new FileOutputStream(new File(file, System.currentTimeMillis() + ".txt"));
                    int len = 0;
                    byte[] bytes = new byte[1024];
                    while ((len = inputStream.read(bytes)) != -1) {
                        outputStream.write(bytes, 0, len);
                    }
                    outputStream.flush();
                    Log.e("程序出异常了", "写入本地文件成功：" + file.getAbsolutePath());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                } finally {
                    if (outputStream != null) {
                        try {
                            outputStream.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }).start();
    }

}