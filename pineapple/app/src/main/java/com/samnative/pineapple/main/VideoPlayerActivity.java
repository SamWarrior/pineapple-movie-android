package com.samnative.pineapple.main;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.xiao.nicevideoplayer.NiceVideoPlayer;
import com.xiao.nicevideoplayer.NiceVideoPlayerManager;
import com.xiao.nicevideoplayer.TxVideoPlayerController;

/**
 * 视频播放专用
 */
public class VideoPlayerActivity extends BaseActivity implements View.OnClickListener {

    //    private TextView tv_title;
    private NiceVideoPlayer nice_video_player;
    private TxVideoPlayerController controller;

    private String name, videourl;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_video_player;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        name = getIntent().getStringExtra("name");
        videourl = getIntent().getStringExtra("videourl");
        find(R.id.rl_back).setOnClickListener(this);

        controller = new TxVideoPlayerController(mContext);
        controller.onPlayModeChanged(NiceVideoPlayer.MODE_FULL_SCREEN);
        controller.setTitle(name);
        controller.setBacktoExit(true);
        nice_video_player = find(R.id.nice_video_player);
        nice_video_player.setController(controller);
        nice_video_player.setPlayerType(NiceVideoPlayer.TYPE_NATIVE);
        if (!TextUtils.isEmpty(videourl)) {
            nice_video_player.setUp(videourl, null);
            nice_video_player.start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (nice_video_player != null && nice_video_player.isPlaying()) {
            nice_video_player.pause();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        // 在onStop时释放掉播放器
        NiceVideoPlayerManager.instance().releaseNiceVideoPlayer();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_back:
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        // 在全屏或者小窗口时按返回键要先退出全屏或小窗口，
        // 所以在Activity中onBackPress要交给NiceVideoPlayer先处理。
        if (NiceVideoPlayerManager.instance().onBackPressd()) return;
        super.onBackPressed();
    }

}
