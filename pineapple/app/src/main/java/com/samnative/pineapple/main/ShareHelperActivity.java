package com.samnative.pineapple.main;

import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.blankj.utilcode.util.LogUtils;
import com.shashank.sony.fancytoastlib.FancyToast;
import com.umeng.commonsdk.UMConfigure;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;
import com.samnative.pineapple.entity.ShareData;
import com.samnative.pineapple.instance.AppPrefer;
import com.samnative.pineapple.value.Config;

import java.io.File;

/**
 * 分享用的
 */
public class ShareHelperActivity extends AppCompatActivity implements OnClickListener {

    Context mContext;

    /***/
    private UMShareAPI mShareAPI;
    /***/
    private UMShareListener umShareListener;
    /**
     * 分享的数据集合
     */
    private ShareData sd = null;
    /**
     * 友盟分享的图片包装类
     */
    private UMImage image;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_helper);
        mContext = this;

        String token = AppPrefer.getInstance(mContext).getString(AppPrefer.mtoken, "");
        boolean isPrivacyPolicy = AppPrefer.getInstance(mContext).getBoolean(AppPrefer.isPrivacyPolicy, false);

        if (!token.equals("") && !UMConfigure.isInit && isPrivacyPolicy) {
            Log.e("2255", "用户已经注册/登录,且没有初始化友盟,此时可以真正初始化友盟SDK");
            //友盟分享初始化
            UMConfigure.init(this, Config.umengKey, "umeng", UMConfigure.DEVICE_TYPE_PHONE, "");
            PlatformConfig.setWeixin(Config.wechatId, Config.wechatKey);
            PlatformConfig.setWXFileProvider(getPackageName() + ".FileProvider");
            PlatformConfig.setQQZone(Config.qqId, Config.qqKey);
            PlatformConfig.setQQFileProvider(getPackageName() + ".FileProvider");
            PlatformConfig.setSinaWeibo(Config.sinaId, Config.sinaKey, "http://sns.whalecloud.com");
            PlatformConfig.setSinaFileProvider(getPackageName() + ".FileProvider");
        }

        sd = (ShareData) getIntent().getSerializableExtra("shareData");
        LogUtils.e("分享参数=" + sd.toString());
        if (sd != null) {
            if (sd.getImage() == null) {
                sd.setImage("");
            }
            if (sd.is_loca_img()) {
                File file = new File(sd.getImage());
                image = new UMImage(mContext, file);
            } else {
                image = new UMImage(mContext, sd.getImage());
            }
        }

        findViewById(R.id.tv_share_qq).setOnClickListener(this);
        findViewById(R.id.tv_share_wechat).setOnClickListener(this);
        findViewById(R.id.tv_share_weibo).setOnClickListener(this);
        findViewById(R.id.tv_share_copy).setOnClickListener(this);
        findViewById(R.id.share_main_layout).setOnClickListener(this);
        findViewById(R.id.btn_no).setOnClickListener(this);

        mShareAPI = UMShareAPI.get(mContext);
        UMConfigure.setLogEnabled(true);
        umShareListener = new UMShareListener() {
            @Override
            public void onStart(SHARE_MEDIA share_media) {

            }

            @Override
            public void onResult(SHARE_MEDIA platform) {
                FancyToast.showToast(mContext, getShareName(platform) + " 分享成功啦", FancyToast.SUCCESS, false);
                exitActivity();
            }

            @Override
            public void onError(SHARE_MEDIA platform, Throwable t) {
                FancyToast.showToast(mContext, getShareName(platform) + " 分享失败啦", FancyToast.ERROR, false);
                exitActivity();
            }

            @Override
            public void onCancel(SHARE_MEDIA platform) {
                FancyToast.showToast(mContext, getShareName(platform) + " 分享取消了", FancyToast.WARNING, false);
                exitActivity();
            }
        };
    }


    @Override
    public void onBackPressed() {
        exitActivity();
    }


    public void exitActivity() {
        finish();
        overridePendingTransition(R.anim.act_exit_bottom_alpha, R.anim.act_exit_top_alpha);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.share_main_layout:
            case R.id.btn_no:
                exitActivity();
                break;
            case R.id.tv_share_wechat: {
                if (!mShareAPI.isInstall(this, SHARE_MEDIA.WEIXIN)) {
                    tips(SHARE_MEDIA.WEIXIN);
                    break;
                }

                //分享web链接,目前只用这一种方式
                UMWeb web = new UMWeb(sd.getUrl());
                web.setTitle(sd.getTitle());//标题
                web.setThumb(image);  //缩略图
                web.setDescription(sd.getDescription());//描述

                new ShareAction(this)
                        .withMedia(web).setPlatform(SHARE_MEDIA.WEIXIN).setCallback(umShareListener).share();
            }
            break;
            case R.id.tv_share_qq: {

                //分享web链接,目前只用这一种方式
                UMWeb web = new UMWeb(sd.getUrl());
                web.setTitle(sd.getTitle());//标题
                web.setThumb(image);  //缩略图
                web.setDescription(sd.getDescription());//描述

                new ShareAction(this)
                        .withMedia(web).setPlatform(SHARE_MEDIA.QQ).setCallback(umShareListener).share();

            }
            break;
            case R.id.tv_share_weibo: {

                //分享web链接,目前只用这一种方式
                UMWeb web = new UMWeb(sd.getUrl());
                web.setTitle(sd.getTitle());//标题
                web.setThumb(image);  //缩略图
                web.setDescription(sd.getDescription());//描述

                new ShareAction(this)
                        .withMedia(web).setPlatform(SHARE_MEDIA.SINA).setCallback(umShareListener).share();

            }
            break;
//            case R.id.tv_share_sms:
//                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
//                sendIntent.putExtra(
//                        "sms_body",
//                        sd.getTitle() + "\n" + sd.getDescription() + "\n"
//                                + sd.getUrl());
//                sendIntent.setType("vnd.android-dir/mms-sms");
//                startActivity(sendIntent);
//                break;
            case R.id.tv_share_copy:
                ClipboardManager cm = (ClipboardManager) getSystemService(mContext.CLIPBOARD_SERVICE);
                // 将文本内容放到系统剪贴板里。
                cm.setText(sd.getUrl());
                FancyToast.showToast(mContext, "链接复制成功", FancyToast.SUCCESS, false);
                finish();
                break;
        }
    }

    /**
     * 未安装客户端提示(微信)
     *
     * @param platform
     */
    public void tips(SHARE_MEDIA platform) {
        FancyToast.showToast(mContext, "请安装" + getShareName(platform) + "客户端", FancyToast.WARNING, false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            /** attention to this below ,must add this **/
            UMShareAPI.get(mContext).onActivityResult(requestCode, resultCode, data);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    /**
     * 获取分享的平台名称
     *
     * @param platform
     * @return
     */
    public String getShareName(SHARE_MEDIA platform) {
        String str = "";
        if (platform == SHARE_MEDIA.QQ) {
            str = "QQ";
        } else if (platform == SHARE_MEDIA.QZONE) {
            str = "QQ空间";
        } else if (platform == SHARE_MEDIA.WEIXIN) {
            str = "微信";
        } else if (platform == SHARE_MEDIA.WEIXIN_CIRCLE) {
            str = "微信朋友圈";
        } else if (platform == SHARE_MEDIA.SINA) {
            str = "微博";
        }
        return str;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


}
