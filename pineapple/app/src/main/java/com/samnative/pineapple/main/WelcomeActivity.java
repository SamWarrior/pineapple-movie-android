package com.samnative.pineapple.main;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;

import com.samnative.pineapple.instance.AppPrefer;
import com.samnative.pineapple.utils.ViewHelper;
import com.samnative.pineapple.value.Config;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.DefaultRefreshFooterCreator;
import com.scwang.smartrefresh.layout.api.DefaultRefreshHeaderCreator;
import com.scwang.smartrefresh.layout.api.RefreshFooter;
import com.scwang.smartrefresh.layout.api.RefreshHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;

/**
 * 欢迎页
 */
public class WelcomeActivity extends BaseActivity {

    /**
     * 背景
     */
    private ImageView iv_bg;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_welcome;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);

        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
            finish();
            return;
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                SmartRefreshLayout.setDefaultRefreshHeaderCreator(new DefaultRefreshHeaderCreator() {
                    @NonNull
                    @Override
                    public RefreshHeader createRefreshHeader(Context context, RefreshLayout layout) {
                        MaterialHeader classicsHeader = new MaterialHeader(context);
                        return classicsHeader.setShowBezierWave(false);//指定为系统样式
                    }
                });
                //设置全局的Footer构建器
                SmartRefreshLayout.setDefaultRefreshFooterCreator(new DefaultRefreshFooterCreator() {
                    @NonNull
                    @Override
                    public RefreshFooter createRefreshFooter(Context context, RefreshLayout layout) {
                        //指定为经典Footer，默认是 BallPulseFooter
                        ClassicsFooter classicsFooter = new ClassicsFooter(context);
                        classicsFooter.setFinishDuration(0);
                        classicsFooter.setTextSizeTitle(12);
                        return classicsFooter.setSpinnerStyle(SpinnerStyle.Translate);
                    }
                });
            }
        }).start();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                startActivity(new Intent(mContext, MainActivity.class), false);
                finish();
            }
        }, 2500);
//        openSlideFinish(false);
        iv_bg = findViewById(R.id.iv_bg);
        swipePanel.setLeftEnabled(false);
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        String dpi = AppPrefer.getInstance(mContext).getString(AppPrefer._dpi, "");
        Point p = ViewHelper.getWindowRealize(this);
        if (dpi.isEmpty()) {
            //首次运行APP,去获取取一次屏幕分辨率,然后储存
            dpi = p.x + "*" + p.y;
            AppPrefer.getInstance(mContext).put(AppPrefer._dpi, dpi);
        }
        Config._dpi = dpi;

//        if ((p.y / p.x) >= 2f) {
            //该设备是全面屏，上大图
            iv_bg.setImageDrawable(ContextCompat.getDrawable(this, R.mipmap.bg_welcome_full));
//        } else {
//            iv_bg.setImageDrawable(ContextCompat.getDrawable(this, R.mipmap.bg_welcome));
//        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

}
