package com.samnative.pineapple.adapter;

import android.support.annotation.LayoutRes;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.samnative.pineapple.entity.HomeListModel;
import com.samnative.pineapple.entity.MovieDetalCommentModel;
import com.samnative.pineapple.main.R;
import com.samnative.pineapple.utils.GlideRequestOptions;
import com.samnative.pineapple.value.Config;

/**
 * 电影详情——评论
 */
public class MoviedetalCommnetAdapter extends BaseQuickAdapter<MovieDetalCommentModel, BaseViewHolder> {

    public MoviedetalCommnetAdapter(@LayoutRes int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, MovieDetalCommentModel item) {

        ImageView iv_image = helper.getView(R.id.iv_image);
        Glide.with(mContext)
                .load(Config._API_URL + item.getHeadicon())//图片地址
                .apply(GlideRequestOptions.OPTIONS_400_400)
                .into(iv_image);

        TextView tv_title=helper.getView(R.id.tv_title);
        TextView tv_content = helper.getView(R.id.tv_content);
        TextView tv_date = helper.getView(R.id.tv_date);

        tv_title.setText(item.getNickname());
        tv_content.setText(item.getContent());
        tv_date.setText(item.getStartTime());

    }

}
