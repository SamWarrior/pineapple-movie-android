package com.samnative.pineapple.interfaces;


import com.samnative.pineapple.entity.ExpressModel;

public interface OnChooseExpressListener {
	
	public void onChooseExpress(ExpressModel model);

}
