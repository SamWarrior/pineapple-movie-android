package com.samnative.pineapple.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.blankj.utilcode.util.LogUtils;
import com.samnative.pineapple.main.MainActivity;
import com.liulishuo.filedownloader.BaseDownloadTask;
import com.liulishuo.filedownloader.FileDownloadListener;
import com.liulishuo.filedownloader.FileDownloader;
import com.samnative.pineapple.entity.MessageEvent;
import com.samnative.pineapple.main.R;
import com.samnative.pineapple.value.Config;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.io.File;


/**
 * 应用主服务
 *
 * @author WangPeng
 */
public class MainService extends Service {

    private Context context;
    /**
     * 通知栏管理器
     */
    private NotificationManager mNotificationManager;
    /**
     * 通知栏构造器
     */
    private NotificationCompat.Builder mBuilder;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        context = this;
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mBuilder = new NotificationCompat.Builder(context, "1400745");

    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub
        int key = intent.getIntExtra(Config.SERVICE_KEY, -1);
        switch (key) {
            case Config.SERVICE_DOWNLOAD_UPDATA: // 下载更新包
            {
                final String downloadurl = intent.getStringExtra("downloadurl");
                String versionName = intent.getStringExtra("versionName");

                File file = new File(Environment.getExternalStorageDirectory(),
                        context.getPackageName() + ".apk");
                if (file.exists()) {
                    file.delete();
                }

                downLoadUpdate(downloadurl, versionName, file);
            }
            break;
            case Config.SERVICE_DOWNLOAD_FILE: // 下载其它文件
            {
                final String downloadurl = intent.getStringExtra("downloadurl");
                String filename = intent.getStringExtra("filename");

                File file = new File(Environment.getExternalStorageDirectory() + "/FZT/",
                        filename);
                if (file.exists()) {
                    file.delete();
                }

                downLoadOther(downloadurl, filename, file);
            }
            break;


            case Config.SERVICE_RECIVER_MESSAGE:// 监听新消息
                Log.e("onReceived: ", "启动了......");
                break;


        }

        return Service.START_REDELIVER_INTENT;
    }

    /**
     * 下载版本更新文件
     *
     * @param downLoadUrl 传入下载链接
     * @param targetFile  指定要保存的file
     * @param versionName 下载文件名称(发通知栏用)
     */
    public void downLoadUpdate(String downLoadUrl, String versionName, File targetFile) {
        FileDownloader.setup(this);
        FileDownloader.getImpl().create(downLoadUrl).setPath(targetFile.getAbsolutePath()).setForceReDownload(true)
                .setListener(new FileDownloadListener() {
                    //等待
                    @Override
                    protected void pending(BaseDownloadTask task, int soFarBytes, int totalBytes) {
                        LogUtils.e("等待下载");

                        mBuilder.setContentTitle("更新版本 " + versionName)// 设置通知栏标题
                                .setContentText("") // 设置通知栏显示内容
                                // .setContentIntent() // 设置通知栏点击意图
                                // .setNumber(number) //设置通知集合的数量
                                .setTicker("更新版本") // 通知首次出现在通知栏，带上升动画效果的
                                .setWhen(System.currentTimeMillis())// 通知产生的时间，会在通知信息里显示，一般是系统获取到的时间
                                .setPriority(Notification.PRIORITY_DEFAULT) // 设置该通知优先级
                                .setAutoCancel(false)// 设置这个标志当用户单击面板就可以让通知将自动取消
                                .setOngoing(true)// ture，设置他为一个正在进行的通知。他们通常是用来表示一个后台任务,用户积极参与(如播放音乐)或以某种方式正在等待,因此占用设备(如一个文件下载,同步操作,主动网络连接)
                                .setDefaults(Notification.DEFAULT_LIGHTS)// 向通知添加声音、闪灯和振动效果的最简单、最一致的方式是使用当前的用户默认设置，使用defaults属性，可以组合
                                // Notification.DEFAULT_ALL Notification.DEFAULT_SOUND 添加声音
                                .setSmallIcon(R.mipmap.ic_launcher)// 设置通知小ICON
                                .setProgress(100, 0, false);// max:进度条最大数值progress:当前进度indeterminate:表示进度是否不确定，true为不确定
                    }

                    //下载进度回调
                    @Override
                    protected void progress(BaseDownloadTask task, int soFarBytes, int totalBytes) {
                        int pro = (soFarBytes * 100 / totalBytes);
//                        LogUtils.e(pro);
                        mBuilder.setProgress(100, pro, false);
                        mBuilder.setContentText("已下载 " + pro + "%");
                        mNotificationManager.notify(UPDATA_PROGRESS, mBuilder.build());

                        MessageEvent event = new MessageEvent();
                        event.setDownLoadPro(pro);
                        EventBus.getDefault().post(event);

                    }

                    //完成下载
                    @Override
                    protected void completed(BaseDownloadTask task) {
                        LogUtils.e("下载完成了");
                        mNotificationManager.cancel(UPDATA_PROGRESS);
                        MessageEvent event = new MessageEvent();
                        event.setDownLoadPro(100);
                        event.setDownLoadEnd(true);
                        EventBus.getDefault().post(event);
                    }

                    //暂停
                    @Override
                    protected void paused(BaseDownloadTask task, int soFarBytes, int totalBytes) {
                        LogUtils.e("下载暂停了");
                    }

                    //下载出错
                    @Override
                    protected void error(BaseDownloadTask task, Throwable e) {
                        LogUtils.e("下载出错了");
                        mNotificationManager.cancel(UPDATA_PROGRESS);
                    }

                    //已存在相同下载
                    @Override
                    protected void warn(BaseDownloadTask task) {
                        LogUtils.e("已存在相同下载");
                        mNotificationManager.cancel(UPDATA_PROGRESS);
                    }
                }).start();
    }

    /**
     * 下载其它文件
     *
     * @param downLoadUrl 传入下载链接
     * @param targetFile  指定要保存的file
     * @param fileName    下载文件名称(发通知栏用)
     */
    public void downLoadOther(String downLoadUrl, String fileName, File targetFile) {
        FileDownloader.setup(this);
        FileDownloader.getImpl().create(downLoadUrl).setPath(targetFile.getAbsolutePath()).setForceReDownload(true)
                .setListener(new FileDownloadListener() {
                    //等待
                    @Override
                    protected void pending(BaseDownloadTask task, int soFarBytes, int totalBytes) {
                        LogUtils.e("等待下载");
                        mBuilder.setContentTitle(fileName)// 设置通知栏标题
                                .setContentText("") // 设置通知栏显示内容
                                // .setContentIntent() // 设置通知栏点击意图
                                // .setNumber(number) //设置通知集合的数量
                                .setTicker("下载文件") // 通知首次出现在通知栏，带上升动画效果的
                                .setWhen(System.currentTimeMillis())// 通知产生的时间，会在通知信息里显示，一般是系统获取到的时间
                                .setPriority(Notification.PRIORITY_DEFAULT) // 设置该通知优先级
                                .setAutoCancel(false)// 设置这个标志当用户单击面板就可以让通知将自动取消
                                .setOngoing(true)// ture，设置他为一个正在进行的通知。他们通常是用来表示一个后台任务,用户积极参与(如播放音乐)或以某种方式正在等待,因此占用设备(如一个文件下载,同步操作,主动网络连接)
                                .setDefaults(Notification.DEFAULT_LIGHTS)// 向通知添加声音、闪灯和振动效果的最简单、最一致的方式是使用当前的用户默认设置，使用defaults属性，可以组合
                                // Notification.DEFAULT_ALL Notification.DEFAULT_SOUND 添加声音
                                .setSmallIcon(R.mipmap.ic_launcher)// 设置通知小ICON
                                .setProgress(100, 0, false);// max:进度条最大数值progress:当前进度indeterminate:表示进度是否不确定，true为不确定
                    }

                    //下载进度回调
                    @Override
                    protected void progress(BaseDownloadTask task, int soFarBytes, int totalBytes) {
                        int pro = (soFarBytes * 100 / totalBytes);
//                        LogUtils.e(pro);
                        mBuilder.setProgress(100, pro, false);
                        mBuilder.setContentText("已下载 " + pro + "%");
                        mNotificationManager.notify(FILE_PROGRESS, mBuilder.build());
                    }

                    //完成下载
                    @Override
                    protected void completed(BaseDownloadTask task) {
                        LogUtils.e("下载完成了");
                        mBuilder.setProgress(100, 100, false);
                        mBuilder.setContentText("下载完成");
                        mNotificationManager.notify(FILE_PROGRESS, mBuilder.build());

                        MessageEvent event = new MessageEvent();
                        event.setOtherDownloadEnd(true);
                        EventBus.getDefault().post(event);
                    }

                    //暂停
                    @Override
                    protected void paused(BaseDownloadTask task, int soFarBytes, int totalBytes) {
                        LogUtils.e("下载暂停了");
                    }

                    //下载出错
                    @Override
                    protected void error(BaseDownloadTask task, Throwable e) {
                        LogUtils.e("下载出错了");
                        mNotificationManager.cancel(FILE_PROGRESS);
                    }

                    //已存在相同下载
                    @Override
                    protected void warn(BaseDownloadTask task) {
                        LogUtils.e("已存在相同下载");
                        mNotificationManager.cancel(FILE_PROGRESS);
                    }
                }).start();
    }

    /**
     * 下载更新时更新通知栏的KEY
     */
    private final int UPDATA_PROGRESS = 0x1001;

    /**
     * 下载其它文件时更新通知栏的KEY
     */
    private final int FILE_PROGRESS = 0x2001;
}
