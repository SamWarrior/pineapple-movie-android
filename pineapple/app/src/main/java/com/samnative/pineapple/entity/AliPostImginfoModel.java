package com.samnative.pineapple.entity;

/**
 * Ali图片上传参数model
 * 
 * 
 * <pre>
 * {"code":200,
 * "msg":"",
 * "server":"0",
 * "data":
 * {"accessid":"oEvCKKLOZ7tlBdf4",
 * "host": "http:\/\/niuniuhuiapp.oss-cn-shenzhen.aliyuncs.com\/",
 * "policy":"eyJleHBpcmF0aW9uIjoiMjAxNi0wNC0yM1QxMzoyNzozOVoiLCJjb25kaXRpb25zIjpbWyJjb250ZW50LWxlbmd0aC1yYW5nZSIsMCwxMDQ4NTc2MDAwXSxbInN0YXJ0cy13aXRoIiwiJGtleSIsIlVwbG9hZFwvcHJvZHVjdFwvMjAxNi0wNC0yMyJdXX0=",
 * "signature":"MUVfWHvEsP\/wU9ViXJkr4eR09T0=",
 * "expire":1461389259,
 * "dir":"Upload\/product\/2016-04-23\/1461389229el788",
 * "success_action_status":"200",
 * "ImgServerName":"http:\/\/images.niuniuhuiapp.com\/"}
 * }
 * </pre>
 * 
 * @author WangPeng
 * 
 */
public class AliPostImginfoModel {

	private String accessid = "";
	private String host = "";
	private String policy = "";
	private String signature = "";
	private String dir = "";
	private String success_action_status = "";
	private String ImgServerName = "";
	private int expire;

	public String getAccessid() {
		return accessid;
	}

	public void setAccessid(String accessid) {
		this.accessid = accessid;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPolicy() {
		return policy;
	}

	public void setPolicy(String policy) {
		this.policy = policy;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getDir() {
		return dir;
	}

	public void setDir(String dir) {
		this.dir = dir;
	}

	public String getSuccess_action_status() {
		return success_action_status;
	}

	public void setSuccess_action_status(String success_action_status) {
		this.success_action_status = success_action_status;
	}

	public String getImgServerName() {
		return ImgServerName;
	}

	public void setImgServerName(String imgServerName) {
		ImgServerName = imgServerName;
	}

	public int getExpire() {
		return expire;
	}

	public void setExpire(int expire) {
		this.expire = expire;
	}

}
