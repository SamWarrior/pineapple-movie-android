package com.samnative.pineapple.adapter;

import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.samnative.pineapple.entity.HomeListModel;
import com.samnative.pineapple.main.R;
import com.samnative.pineapple.utils.GlideRequestOptions;
import com.samnative.pineapple.utils.ViewHelper;
import com.samnative.pineapple.value.Config;
import com.willy.ratingbar.ScaleRatingBar;

import java.util.List;


/**
 * 首页列表适配器
 */
public class HomeListAdapter extends BaseMultiItemQuickAdapter<HomeListModel, BaseViewHolder> {


    public HomeListAdapter(@Nullable List<HomeListModel> data) {
        super(data);
        //1为左右单图 2为上下单图 3为上下多图 4广告位
        addItemType(1, R.layout.item_main_home);
        addItemType(2, R.layout.item_main_home2);
//        addItemType(3, R.layout.item_main_home3);
        addItemType(4, R.layout.item_main_home4);
    }

    @Override
    protected void convert(BaseViewHolder helper, final HomeListModel item) {
        switch (helper.getItemViewType()) {
            case 1:
                initItemType1(helper, item);
                return;
            case 2:
                initItemType2(helper, item);
                return;
//            case 3:
//                initItemType3(helper, item);
//                return;
            case 4:
                initItemType4(helper, item);
                return;
        }
    }

    /**
     * 初始化样式1 图片在左,文字在右
     *
     * @param helper
     * @param item
     */
    public void initItemType1(BaseViewHolder helper, final HomeListModel item) {
        ImageView image = helper.getView(R.id.image);

        Glide.with(mContext)
                .load(Config._API_URL + item.getCover()) //图片地址
                .apply(GlideRequestOptions.OPTIONS_400_400)
                .into(image);

        TextView tv_text1 = helper.getView(R.id.tv_text1);
        tv_text1.setText(item.getMovieName());
        TextView tv_text2 = helper.getView(R.id.tv_text2);
        tv_text2.setText(item.getInfo());
        TextView tv_text3 = helper.getView(R.id.tv_text3);
        tv_text3.setText("时长："+item.getTotalTime());
        TextView tv_text4 = helper.getView(R.id.tv_text4);
        tv_text4.setText("上映时间："+item.getReleaseDate());
        TextView tv_text5 = helper.getView(R.id.tv_text5);
        tv_text5.setText(item.getCounts()+"");

        ScaleRatingBar rating=helper.getView(R.id.rating);
        try {
            rating.setRating((float) (item.getScore()/2));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 初始化样式2 文字在上,图在下
     *
     * @param helper
     * @param item
     */
    public void initItemType2(BaseViewHolder helper, final HomeListModel item) {
        ImageView image = helper.getView(R.id.image);
        ViewHelper.setHeight(mContext, image, 0.60f);

//        Glide.with(mContext)
////                .load(item.getLogo().get(0)) //图片地址
////                .apply(GlideRequestOptions.OPTIONS_400_400)
////                .into(image);
////
////        TextView tv_title = helper.getView(R.id.tv_title);
////        tv_title.setText(item.getTitle());
////        TextView tv_push = helper.getView(R.id.tv_push);
////        tv_push.setText(item.getPublisher());
////
////        TextView tv_date = helper.getView(R.id.tv_date);
////        tv_date.setText(item.getAddtime());
    }

//    /**
//     * 初始化样式3 图在上(三张),文字在下
//     *
//     * @param helper
//     * @param item
//     */
//    public void initItemType3(BaseViewHolder helper, final HomeListModel item) {
//        ImageView image = helper.getView(R.id.image);
//        image.setVisibility(View.GONE);
//        ImageView image2 = helper.getView(R.id.image2);
//        image2.setVisibility(View.GONE);
//        ImageView image3 = helper.getView(R.id.image3);
//        image3.setVisibility(View.GONE);
//        if (item.getLogo().size() > 0) {
//            image.setVisibility(View.VISIBLE);
//
//            Glide.with(mContext)
//                    .load(item.getLogo().get(0)) //图片地址
//                    .apply(GlideRequestOptions.OPTIONS_400_400)
//                    .into(image);
//        }
//        if (item.getLogo().size() > 1) {
//            image2.setVisibility(View.VISIBLE);
//
//            Glide.with(mContext)
//                    .load(item.getLogo().get(1)) //图片地址
//                    .apply(GlideRequestOptions.OPTIONS_400_400)
//                    .into(image2);
//
//        }
//        if (item.getLogo().size() > 2) {
//            image3.setVisibility(View.VISIBLE);
//
//            Glide.with(mContext)
//                    .load(item.getLogo().get(2)) //图片地址
//                    .apply(GlideRequestOptions.OPTIONS_400_400)
//                    .into(image3);
//        }
//        TextView tv_title = helper.getView(R.id.tv_title);
//        tv_title.setText(item.getTitle());
//        TextView tv_content = helper.getView(R.id.tv_content);
//        tv_content.setText(item.getAddtime());
//
//        TextView tv_text8 = helper.getView(R.id.tv_text8);
//        tv_text8.setText("浏览" + item.getView());
//    }

    /**
     * 初始化样式4 单张图片
     *
     * @param helper
     * @param item
     */
    public void initItemType4(BaseViewHolder helper, final HomeListModel item) {
        ImageView image = helper.getView(R.id.image);

//        Glide.with(mContext)
//                .load(item.getLogo().get(0)) //图片地址
//                .apply(GlideRequestOptions.OPTIONS_400_400)
//                .into(image);
    }

}