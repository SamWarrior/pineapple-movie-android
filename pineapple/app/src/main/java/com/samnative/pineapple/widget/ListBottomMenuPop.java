package com.samnative.pineapple.widget;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.samnative.pineapple.adapter.ListMenuPopAdapter;
import com.samnative.pineapple.main.R;

import java.util.List;


/**
 * 通用型，底部弹出菜单列表
 */
public class ListBottomMenuPop extends PopupWindow {

    private Context context;
    /**
     * 有效区域
     */
    private View view;
    /**
     * 监听接口对象
     */
    private OnSelect select;
    /**
     * 阴影层
     */
    private View viewShade;
    /**
     * 菜单
     */
    private RecyclerView rv_menu;
    /**
     * 适配器
     */
    private ListMenuPopAdapter adapter;

    private TextView tv_title;


    /**
     * 监听选中
     *
     * @author WangPeng
     */
    public interface OnSelect {
        public void getChoose(int position);
    }

    public void setListMenu(List<String> listMenu) {
        this.adapter.getData().clear();
        this.adapter.addData(listMenu);
    }

    public ListBottomMenuPop(Context context, String title, OnSelect select) {
        this.context = context;
        this.select = select;
        LayoutInflater inflater = LayoutInflater.from(context);
        view = inflater.inflate(R.layout.pop_list_bottom_menu, null);
        viewShade = view.findViewById(R.id.v_pop_shade);

        // 点击有效区域外则退出pop
        view.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int hight = view.findViewById(R.id.rv_menu).getBottom();
                int point = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (point > hight) {
                        dismiss();
                    }
                }
                return true;
            }
        });

        adapter = new ListMenuPopAdapter(R.layout.item_list_nemu_pop);
        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapterr, View view, int position) {
                adapter.setCurrentChecked(position);
                select.getChoose(position);
                dismiss();
            }
        });
        rv_menu = view.findViewById(R.id.rv_menu);
        rv_menu.setLayoutManager(new LinearLayoutManager(context));
        rv_menu.setAdapter(adapter);

        tv_title = view.findViewById(R.id.tv_title);
        tv_title.setText(title);

        setContentView(view);
        setWidth(LayoutParams.MATCH_PARENT);
        setHeight(LayoutParams.WRAP_CONTENT);
        setFocusable(true);
        setBackgroundDrawable(new ColorDrawable(0x00000000));
        setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }


    @Override
    public void showAsDropDown(View anchor, int xoff, int yoff) {
        super.showAsDropDown(anchor, xoff, yoff);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                PropertyValuesHolder pvhX = PropertyValuesHolder.ofFloat("alpha", 0f,
                        1f);
                ObjectAnimator.ofPropertyValuesHolder(viewShade, pvhX).setDuration(150).start();
            }
        }, 100);
    }

    @Override
    public void showAtLocation(View parent, int gravity, int x, int y) {
        // TODO Auto-generated method stub
        super.showAtLocation(parent, gravity, x, y);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                PropertyValuesHolder pvhX = PropertyValuesHolder.ofFloat("alpha", 0f,
                        1f);
                ObjectAnimator.ofPropertyValuesHolder(viewShade, pvhX).setDuration(150).start();
            }
        }, 100);

    }

    @Override
    public void dismiss() {
        // TODO Auto-generated method stub
        PropertyValuesHolder pvhX = PropertyValuesHolder.ofFloat("alpha", 1f,
                0f);
        ObjectAnimator.ofPropertyValuesHolder(viewShade, pvhX).setDuration(150).start();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                ListBottomMenuPop.super.dismiss();
            }
        }, 130);
    }

}
