package com.samnative.pineapple.main.wxapi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.android.pay.IPayCallBack;
import com.android.pay.WXPayHelper;
import com.blankj.utilcode.util.ToastUtils;
import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.samnative.pineapple.value.Config;

public class WXPayEntryActivity extends Activity implements IWXAPIEventHandler {

    private IWXAPI m_WXApi;
    private Context m_Context;

    @Override
    protected void onCreate(Bundle arg0) {
        // TODO Auto-generated method stub
        super.onCreate(arg0);
        m_Context = this;
        m_WXApi = WXAPIFactory.createWXAPI(this, Config.wechatId);
        m_WXApi.handleIntent(getIntent(), this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        m_WXApi.handleIntent(intent, this);
    }

    @Override
    public void onReq(BaseReq arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onResp(BaseResp p_BaseResp) {
        // TODO Auto-generated method stub
        if (p_BaseResp.getType() == ConstantsAPI.COMMAND_PAY_BY_WX) {
            int m_PayResult = 0;
            if (p_BaseResp.errCode == -2) {
                ToastUtils.showShort("您取消了支付");
                m_PayResult = -2;
            }
            if (p_BaseResp.errCode == -1) {

                String m_ErroInfor = p_BaseResp.errStr == null ? ""
                        : p_BaseResp.errStr;

                ToastUtils.showShort("支付失败" + m_ErroInfor);
                m_PayResult = -1;
            }

            IPayCallBack m_IPayCallBack = WXPayHelper.getIPayCallBack();
            if (m_IPayCallBack != null) {
                m_IPayCallBack.payCallBack(m_PayResult);
            }
            finish();
        }
    }

}
