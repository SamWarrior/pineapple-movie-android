package com.samnative.pineapple.main;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.reflect.TypeToken;
import com.gyf.immersionbar.ImmersionBar;
import com.samnative.pineapple.entity.HomeListModel;
import com.samnative.pineapple.utils.GsonUtils;
import com.shashank.sony.fancytoastlib.FancyToast;
import com.samnative.pineapple.entity.UserModel;
import com.samnative.pineapple.http.HttpRequest;
import com.samnative.pineapple.http.JsonResult;
import com.samnative.pineapple.instance.AppPrefer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 意思反馈
 */
public class FankuiActivity extends BaseActivity implements View.OnClickListener {


    private EditText et_content, et_title;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_fankui;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        ImmersionBar.with(this).statusBarDarkFont(true).init();
        find(R.id.tv_back).setOnClickListener(this);
        find(R.id.btn_submit).setOnClickListener(this);

        et_content = find(R.id.et_content);
        et_title = find(R.id.et_title);

    }


    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
    }


    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.tv_back:
                finish();
                break;
            case R.id.btn_submit:
                if (et_title.getText().toString().isEmpty()) {
                    FancyToast.showToast(mContext, "请填写反馈标题", FancyToast.WARNING, false);
                    break;
                }
                if (et_content.getText().toString().isEmpty()) {
                    FancyToast.showToast(mContext, "请填写反馈内容", FancyToast.WARNING, false);
                    break;
                }

                requestData();

                break;
            default:
                break;
        }

    }

    public void requestData() {
        Map<String, String> map = new HashMap<>();
        map.put("title", et_title.getText().toString());
        map.put("content", et_content.getText().toString());
        HttpRequest.request((Activity) mContext, "/client/feedback/sendfeed", "", map, "POST", new JsonResult() {
            @Override
            public void reqestSuccess(JSONObject result) {
                FancyToast.showToast(mContext, "提交成功", FancyToast.SUCCESS, false);
                finish();
            }
        });
    }


}
