package com.samnative.pineapple.adapter;

import android.support.annotation.LayoutRes;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.samnative.pineapple.entity.AboutModel;
import com.samnative.pineapple.main.R;

/**
 * 关于我们
 */
public class LianxiWomenAdapter extends BaseQuickAdapter<AboutModel, BaseViewHolder> {

    public LianxiWomenAdapter(@LayoutRes int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, final AboutModel item) {


        TextView tv_txt = helper.getView(R.id.tv_txt);
        TextView tv_txt_ = helper.getView(R.id.tv_txt_);
        tv_txt_.setText(item.getTitle());
        tv_txt.setText(item.getText());

    }

}
