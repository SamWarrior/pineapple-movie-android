package com.samnative.pineapple.adapter;

import android.support.annotation.LayoutRes;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.samnative.pineapple.entity.HomeHotmovieModel;
import com.samnative.pineapple.main.R;
import com.samnative.pineapple.utils.GlideRequestOptions;
import com.samnative.pineapple.value.Config;
import com.willy.ratingbar.ScaleRatingBar;

/**
 * 首页-热门电影
 */
public class HomeHotmovieAdapter extends BaseQuickAdapter<HomeHotmovieModel, BaseViewHolder> {

    public HomeHotmovieAdapter(@LayoutRes int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, final HomeHotmovieModel item) {

        ImageView image = helper.getView(R.id.image);
        TextView tv_title = helper.getView(R.id.tv_title);
        ScaleRatingBar rating=helper.getView(R.id.rating);
        tv_title.setText(item.getMovieName());
        Glide.with(mContext)
                .load(Config._API_URL+item.getCover())//图片地址
                .apply(GlideRequestOptions.OPTIONS_400_400)
                .into(image);

        try {
            rating.setRating((float) (item.getScore()/2));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
