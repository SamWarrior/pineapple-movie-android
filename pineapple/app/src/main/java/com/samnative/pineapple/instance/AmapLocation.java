package com.samnative.pineapple.instance;

import android.content.Context;
import android.util.Log;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.samnative.pineapple.value.Config;

/**
 * 定位工具类
 */
public class AmapLocation {

    /**
     * AMapLocationClient类对象
     */
    public AMapLocationClient mLocationClient = null;
    /**
     * AMapLocationClientOption对象
     */
    public AMapLocationClientOption mLocationOption = null;

    public static AmapLocation instance = null;

    public static AmapLocation getInstance(Context mContext) {
        if (instance == null) {
            instance = new AmapLocation(mContext);
        }
        return instance;
    }


    public AmapLocation(Context mContext) {

        // 初始化定位
        mLocationClient = new AMapLocationClient(mContext);
        // 初始化AMapLocationClientOption对象
        mLocationOption = new AMapLocationClientOption();
        // 设置定位模式AMapLocationMode.Battery_Saving低功耗模式,Hight_Accuracy高精度模式
        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Battery_Saving);
        // 设置定位间隔,单位毫秒,默认为2000ms，最低1000ms。
        mLocationOption.setInterval(15000);
        // 设置是否返回地址信息（默认返回地址信息）
        mLocationOption.setNeedAddress(true);
        // 设置是否强制刷新WIFI，默认为true，强制刷新。
        mLocationOption.setWifiActiveScan(false);
        // 设置是否允许模拟位置,默认为false，不允许模拟位置
        mLocationOption.setMockEnable(true);
        // 给定位客户端对象设置定位参数
        mLocationClient.setLocationOption(mLocationOption);
        // 设置定位回调监听
        mLocationClient.setLocationListener(new AMapLocationListener() {
            @Override
            public void onLocationChanged(AMapLocation amapLocation) {
                if (amapLocation == null) {
                    return;
                }
                if (amapLocation.getErrorCode() == 0) {
                    // // 可在其中解析amapLocation获取相应内容。
                    // amapLocation.getLocationType();//
                    // 获取当前定位结果来源，如网络定位结果，详见定位类型表
                    // amapLocation.getLatitude();// 获取纬度
                    // amapLocation.getLongitude();// 获取经度
                    // amapLocation.getAccuracy();// 获取精度信息
                    // amapLocation.getAddress();//
                    // 地址，如果option中设置isNeedAddress为false，则没有此结果，网络定位结果中会有地址信息，GPS定位不返回地址信息。
                    // amapLocation.getCountry();// 国家信息
                    // amapLocation.getProvince();// 省信息
                    // amapLocation.getCity();// 城市信息
                    // amapLocation.getDistrict();// 城区信息
                    // amapLocation.getStreet();// 街道信息
                    // amapLocation.getStreetNum();// 街道门牌号信息
                    // amapLocation.getCityCode();// 城市编码
                    // amapLocation.getAdCode();// 地区编码
                    // amapLocation.getAoiName();// 获取当前定位点的AOI信息
                    // // 获取定位时间
                    // SimpleDateFormat df = new
                    // SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    // Date date = new Date(amapLocation.getTime());
                    // df.format(date);
                    Log.e("2255",
                            amapLocation.getLatitude() + "\n"
                                    + amapLocation.getLongitude() + "\n"
                                    + amapLocation.getCity() + "\n"
                                    + amapLocation.getDistrict() + "\n"
                                    + amapLocation.getStreet() + "\n"
                                    + amapLocation.getCityCode() + "\n"
                                    + amapLocation.getAdCode());
                    boolean is = Config.currentCity.isEmpty();
                    Config.latitude = amapLocation.getLatitude() + "";
                    Config.longitude = amapLocation.getLongitude() + "";
                    Config.currentCity = amapLocation.getCity();
                } else {
                    // 定位失败时，可通过ErrCode（错误码）信息来确定失败的原因，errInfo是错误信息，详见错误码表。
                    Log.e("2255",
                            "location Error, ErrCode:"
                                    + amapLocation.getErrorCode()
                                    + ", errInfo:"
                                    + amapLocation.getErrorInfo());
                }

            }
        });
    }

    public void startLocation() {
        // 启动定位
        if (mLocationClient != null) {
            mLocationClient.startLocation();
        }
    }

    public void stopLocation() {
        // 停止定位后，本地定位服务并不会被销毁
        if (mLocationClient != null) {
            mLocationClient.stopLocation();
        }
    }

}