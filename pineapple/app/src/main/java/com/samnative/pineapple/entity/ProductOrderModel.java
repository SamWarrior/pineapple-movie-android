package com.samnative.pineapple.entity;

import java.util.List;

/**
 * 订单列表
 *
 */
public class ProductOrderModel {

    private String orderno;// 订单编号
    private Double actualfreight = 0.0;// 订单运费
    private String productcount;// 商品总数
    private Double productamount = 0.0;// 现金总价
    private Double bullamount = 0.0;// 牛币总额
    private Double totalamount;// 订单运费+现金总价
    private String addtime;// 添加时间
    private String orderstatus;// 订单状态
    private String businessid;// 店铺ID
    private String businessname;// 店铺名称
    private String evaluate;// 是否评价
    private String islongdate;//
    private String return_status;
    private String delivertime;
    private String paytime;
    private String finish_time;
    private String isabroad;
    private String isnbtc;
    private String nbtcuid;
    private String nbtctitle;
    private String deductemall;
    private String isdeductemall;

    public String getDeductemall() {
        return deductemall;
    }

    public void setDeductemall(String deductemall) {
        this.deductemall = deductemall;
    }

    public String getIsdeductemall() {
        return isdeductemall;
    }

    public void setIsdeductemall(String isdeductemall) {
        this.isdeductemall = isdeductemall;
    }

    private Receipt_address receipt_address;
    private Orderstatusstr orderstatusstr;

    private List<OrderAct> orderact;
    private List<ProItem> orderitem;
    private String business_tel;

    public String getBusiness_tel() {
        return business_tel;
    }

    public void setBusiness_tel(String business_tel) {
        this.business_tel = business_tel;
    }

    public String getNbtctitle() {
        return nbtctitle;
    }

    public void setNbtctitle(String nbtctitle) {
        this.nbtctitle = nbtctitle;
    }

    public String getIsnbtc() {
        return isnbtc;
    }

    public void setIsnbtc(String isnbtc) {
        this.isnbtc = isnbtc;
    }

    public String getNbtcuid() {
        return nbtcuid;
    }

    public void setNbtcuid(String nbtcuid) {
        this.nbtcuid = nbtcuid;
    }

    public String getIsabroad() {
        return isabroad;
    }

    public void setIsabroad(String isabroad) {
        this.isabroad = isabroad;
    }

    public String getPaytime() {
        return paytime;
    }

    public void setPaytime(String paytime) {
        this.paytime = paytime;
    }

    public Receipt_address getReceipt_address() {
        return receipt_address;
    }

    public void setReceipt_address(Receipt_address receipt_address) {
        this.receipt_address = receipt_address;
    }

    public Orderstatusstr getOrderstatusstr() {
        return orderstatusstr;
    }

    public void setOrderstatusstr(Orderstatusstr orderstatusstr) {
        this.orderstatusstr = orderstatusstr;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public Double getActualfreight() {
        return actualfreight;
    }

    public void setActualfreight(Double actualfreight) {
        this.actualfreight = actualfreight;
    }

    public String getProductcount() {
        return productcount;
    }

    public void setProductcount(String productcount) {
        this.productcount = productcount;
    }

    public Double getProductamount() {
        return productamount;
    }

    public void setProductamount(Double productamount) {
        this.productamount = productamount;
    }

    public Double getBullamount() {
        return bullamount;
    }

    public void setBullamount(Double bullamount) {
        this.bullamount = bullamount;
    }

    public Double getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(Double totalamount) {
        this.totalamount = totalamount;
    }

    public String getAddtime() {
        return addtime;
    }

    public void setAddtime(String addtime) {
        this.addtime = addtime;
    }

    public String getOrderstatus() {
        return orderstatus;
    }

    public void setOrderstatus(String orderstatus) {
        this.orderstatus = orderstatus;
    }

    public String getBusinessid() {
        return businessid;
    }

    public void setBusinessid(String businessid) {
        this.businessid = businessid;
    }

    public String getBusinessname() {
        return businessname;
    }

    public void setBusinessname(String businessname) {
        this.businessname = businessname;
    }

    public String getEvaluate() {
        return evaluate;
    }

    public void setEvaluate(String evaluate) {
        this.evaluate = evaluate;
    }

    public String getIslongdate() {
        return islongdate;
    }

    public void setIslongdate(String islongdate) {
        this.islongdate = islongdate;
    }

    public String getReturn_status() {
        return return_status;
    }

    public void setReturn_status(String return_status) {
        this.return_status = return_status;
    }

    public String getDelivertime() {
        return delivertime;
    }

    public void setDelivertime(String delivertime) {
        this.delivertime = delivertime;
    }

    public List<OrderAct> getOrderact() {
        return orderact;
    }

    public void setOrderact(List<OrderAct> orderact) {
        this.orderact = orderact;
    }

    public List<ProItem> getOrderitem() {
        return orderitem;
    }

    public void setOrderitem(List<ProItem> orderitem) {
        this.orderitem = orderitem;
    }

    /***
     * 订单列表下的按钮操作
     */
    public class OrderAct {

        private String act;// 按钮类型1操作钮，2为显示钮
        private String acttype;// 按钮ID
        private String actname;// 按钮值

        public String getAct() {
            return act;
        }

        public void setAct(String act) {
            this.act = act;
        }

        public String getActtype() {
            return acttype;
        }

        public void setActtype(String acttype) {
            this.acttype = acttype;
        }

        public String getActname() {
            return actname;
        }

        public void setActname(String actname) {
            this.actname = actname;
        }

    }

    /**
     * 订单列表下的店铺下的商品model
     */
    public class ProItem {
        private String productid;// 商品ID
        private String productname;// 商品名称
        private String productnum;// 商品数量
        private String thumb;// 商品图片
        private Double prouctprice;// 商品现金价格
        private Double bullamount;// 商品牛币
        private String skudetail;// 商品规格
        private String skuid;//商品规格ID
        private Double productamount;//退款用交易金额
        private Double productbull;//退款用交易积分
        private String productunit;

        private String business_name;//店铺名称
        private String returnType;//退款类型
        private String type;//退款类型(1为退款 2为退货)
        private String returnreason;//退款原因
        private Double returnamount;//退款金额
        private Double returnbull;//退款牛币
        private String remark;//退款说明
        private String returnno;// 退款编号
        private String starttime;//申请时间
        private String endtime;//退款时间
        private List<OrderAct> orderact;

        public String getProductunit() {
            return productunit;
        }

        public void setProductunit(String productunit) {
            this.productunit = productunit;
        }

        public String getProductid() {
            return productid;
        }

        public void setProductid(String productid) {
            this.productid = productid;
        }

        public String getProductname() {
            return productname;
        }

        public void setProductname(String productname) {
            this.productname = productname;
        }

        public String getProductnum() {
            return productnum;
        }

        public void setProductnum(String productnum) {
            this.productnum = productnum;
        }

        public String getThumb() {
            return thumb;
        }

        public void setThumb(String thumb) {
            this.thumb = thumb;
        }

        public Double getProuctprice() {
            return prouctprice;
        }

        public void setProuctprice(Double prouctprice) {
            this.prouctprice = prouctprice;
        }

        public Double getBullamount() {
            return bullamount;
        }

        public void setBullamount(Double bullamount) {
            this.bullamount = bullamount;
        }

        public String getSkudetail() {
            return skudetail;
        }

        public void setSkudetail(String skudetail) {
            this.skudetail = skudetail;
        }

        public String getSkuid() {
            return skuid;
        }

        public void setSkuid(String skuid) {
            this.skuid = skuid;
        }

        public Double getProductamount() {
            return productamount;
        }

        public void setProductamount(Double productamount) {
            this.productamount = productamount;
        }

        public Double getProductbull() {
            return productbull;
        }

        public void setProductbull(Double productbull) {
            this.productbull = productbull;
        }

        public String getBusiness_name() {
            return business_name;
        }

        public void setBusiness_name(String business_name) {
            this.business_name = business_name;
        }

        public String getReturnType() {
            return returnType;
        }

        public void setReturnType(String returnType) {
            this.returnType = returnType;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getReturnreason() {
            return returnreason;
        }

        public void setReturnreason(String returnreason) {
            this.returnreason = returnreason;
        }

        public Double getReturnamount() {
            return returnamount;
        }

        public void setReturnamount(Double returnamount) {
            this.returnamount = returnamount;
        }

        public Double getReturnbull() {
            return returnbull;
        }

        public void setReturnbull(Double returnbull) {
            this.returnbull = returnbull;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getReturnno() {
            return returnno;
        }

        public void setReturnno(String returnno) {
            this.returnno = returnno;
        }

        public String getStarttime() {
            return starttime;
        }

        public void setStarttime(String starttime) {
            this.starttime = starttime;
        }

        public String getEndtime() {
            return endtime;
        }

        public void setEndtime(String endtime) {
            this.endtime = endtime;
        }

        public List<OrderAct> getOrderact() {
            return orderact;
        }

        public void setOrderact(List<OrderAct> orderact) {
            this.orderact = orderact;
        }

    }

    /**
     * 收货信息
     *
     * @author WangPeng
     *         <p>
     *         2017-3-10
     */
    public class Receipt_address {

        private String logisticsid;
        private String mobile;
        private String realname;
        private String city_id;
        private String city;
        private String address;
        private String express_name;
        private String express_no;
        private String idnumber;


        public String getIdnumber() {
            return idnumber;
        }

        public void setIdnumber(String idnumber) {
            this.idnumber = idnumber;
        }

        public String getLogisticsid() {
            return logisticsid;
        }

        public void setLogisticsid(String logisticsid) {
            this.logisticsid = logisticsid;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getRealname() {
            return realname;
        }

        public void setRealname(String realname) {
            this.realname = realname;
        }

        public String getCity_id() {
            return city_id;
        }

        public void setCity_id(String city_id) {
            this.city_id = city_id;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getExpress_name() {
            return express_name;
        }

        public void setExpress_name(String express_name) {
            this.express_name = express_name;
        }

        public String getExpress_no() {
            return express_no;
        }

        public void setExpress_no(String express_no) {
            this.express_no = express_no;
        }

    }

    /**
     * 订单状态信息
     *
     * @author WangPeng
     *         <p>
     *         2017-3-10
     */
    public class Orderstatusstr {
        private String statusstr;
        private String statusinfo;

        public String getStatusstr() {
            return statusstr;
        }

        public void setStatusstr(String statusstr) {
            this.statusstr = statusstr;
        }

        public String getStatusinfo() {
            return statusinfo;
        }

        public void setStatusinfo(String statusinfo) {
            this.statusinfo = statusinfo;
        }
    }


    public String getFinish_time() {
        return finish_time;
    }

    public void setFinish_time(String finish_time) {
        this.finish_time = finish_time;
    }

}
