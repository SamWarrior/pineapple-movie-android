package com.samnative.pineapple.widget;

import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.PopupWindow;

import com.samnative.pineapple.main.PasswordLoginVerityActivity;
import com.samnative.pineapple.main.R;
import com.samnative.pineapple.utils.ViewHelper;


/**
 * 输入密码控件
 *
 * @author WangPeng
 */
public class InputPasswordPop extends PopupWindow implements OnClickListener,
        TextWatcher {

    private Context context;
    /**
     * 有效区域
     */
    private View view;
    /**
     * 阴影层
     */
    private View viewShade;
    /**
     * 输入密码
     */
    private EditText et_password;
    /**
     * 输入监听，输入到6位自动触发监听
     */
    private OnResult onResult;

    /**
     * 输入支付密码控件
     *
     * @param context 上下文
     */
    public InputPasswordPop(Context context, OnResult onResult) {
        this.context = context;
        this.onResult = onResult;
        LayoutInflater inflater = LayoutInflater.from(context);
        view = inflater.inflate(R.layout.pop_input_password, null);
        view.findViewById(R.id.iv_close).setOnClickListener(this);
        view.findViewById(R.id.tv_forget_psw).setOnClickListener(this);
        et_password = (EditText) view.findViewById(R.id.et_password);
        et_password.addTextChangedListener(this);
        viewShade = view.findViewById(R.id.v_pop_shade);
        setContentView(view);
        setWidth(LayoutParams.MATCH_PARENT);
        setHeight(LayoutParams.MATCH_PARENT);
        setFocusable(true);
        setAnimationStyle(R.style.AnimBottom);
        setBackgroundDrawable(null);
        setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }

    @Override
    public void showAtLocation(View parent, int gravity, int x, int y) {
        // TODO Auto-generated method stub
        viewShade.setVisibility(View.GONE);
        super.showAtLocation(parent, gravity, x, y);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
                anim.setDuration(200);
                viewShade.startAnimation(anim);
                viewShade.setVisibility(View.VISIBLE);

                InputMethodManager imm = (InputMethodManager) context
                        .getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(et_password, InputMethodManager.SHOW_FORCED);
            }
        }, 300);

    }

    /**
     * 点击监听
     */
    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.iv_close:
                ViewHelper.hideKeyboard(context, et_password);
                dismiss();
                break;
            case R.id.tv_forget_psw:
                Intent intent = new Intent().setClass(context, PasswordLoginVerityActivity.class);
                intent.putExtra("type", 4);
                context.startActivity(intent);
                dismiss();
                break;
        }
    }


    /**
     * 输入完成监听
     */
    public interface OnResult {

        public void password(String password);

    }

    // ===============================================================输入监听START

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count,
                                  int after) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        // TODO Auto-generated method stub

    }

    @Override
    public void afterTextChanged(Editable s) {
        // TODO Auto-generated method stub

        if (s.toString().length() == 6) {
            ViewHelper.hideKeyboard(context, et_password);
            onResult.password(s.toString());
            dismiss();
        }

    }

    // ===============================================================输入监听END
}
