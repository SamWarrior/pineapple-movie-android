package com.samnative.pineapple.listener;

import android.support.v7.widget.RecyclerView;

/**
 * Created by desmond on 10/4/15.
 * listView滑动自定义监听
 */
public interface ScrollTabListener {

    void adjustScroll(int scrollHeight, int headerHeight);

    void onListViewScroll(RecyclerView.LayoutManager manager, int firstVisibleItem, int visibleItemCount,
                          int totalItemCount, int pagePosition);

}
