package com.samnative.pineapple.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import java.util.List;

public class MyFragmentAdapter extends FragmentPagerAdapter {

	private List<Fragment> listData;
	private List<String> listTitle;
	private Fragment mCurrentFragment;
	public MyFragmentAdapter(FragmentManager fm, List<Fragment> listData) {
		super(fm);
		this.listData = listData;
	}

	public MyFragmentAdapter(FragmentManager fm, List<Fragment> listData,
                             List<String> listTitle) {
		super(fm);
		this.listData = listData;
		this.listTitle = listTitle;
	}

	@Override
	public Fragment getItem(int arg0) {
		return listData.get(arg0);
	}

	@Override
	public int getCount() {
		return listData.size();
	}

	@Override
	public CharSequence getPageTitle(int position) {
		if (listTitle != null) {
			return listTitle.get(position);
		} else {
			return "";
		}
	}

	@Override
	public void setPrimaryItem(ViewGroup container, int position, Object object) {
		mCurrentFragment = (Fragment) object;
		super.setPrimaryItem(container, position, object);
	}

	public Fragment getCurrentFragment() {
		return mCurrentFragment;
	}
	@Override
	public int getItemPosition(Object object) {
		return POSITION_NONE;
	}

}
