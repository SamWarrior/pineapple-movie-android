package com.samnative.pineapple.entity;

import java.io.Serializable;

/**
 * 收货地址信息
 * 
 * @author WangPeng
 * 
 *         2017-3-7
 * 
 */
public class ReceiveAddressModel implements Serializable {

	/**
	 * 收货地址列表
	 */
	private static final long serialVersionUID = 1L;

	private String address_id;//地址ID
	private String customerid;//会员ID
	private String mobile;
	private String city_id;//城市ID
	private String isdefault;//是否默认
	private String realname;//收货姓名
	private String city;//城市、区县
	private String address;//详细地址
	public String getAddress_id() {
		return address_id;
	}
	public void setAddress_id(String address_id) {
		this.address_id = address_id;
	}
	public String getCustomerid() {
		return customerid;
	}
	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getCity_id() {
		return city_id;
	}
	public void setCity_id(String city_id) {
		this.city_id = city_id;
	}
	public String getIsdefault() {
		return isdefault;
	}
	public void setIsdefault(String isdefault) {
		this.isdefault = isdefault;
	}
	public String getRealname() {
		return realname;
	}
	public void setRealname(String realname) {
		this.realname = realname;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

}