package com.samnative.pineapple.main.wxapi;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.android.pay.IPayCallBack;
import com.android.pay.WXPayHelper;
import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.modelbiz.WXLaunchMiniProgram;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.umeng.socialize.weixin.view.WXCallbackActivity;
import com.samnative.pineapple.value.Config;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by ntop on 15/9/4.
 */
public class WXEntryActivity extends WXCallbackActivity implements IWXAPIEventHandler {

    private IWXAPI api;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        api = WXAPIFactory.createWXAPI(this, Config.wechatId); //自己app的appKey
//        api = WXAPIFactory.createWXAPI(this, ConstantText.WX_APPKEY, false);
        try {
            Intent intent = getIntent();
            api.handleIntent(intent, this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.e("2255", "WXEntryActivity Create");
    }

    @Override

    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        api.handleIntent(intent, this);

        Log.e("2255", "WXEntryActivity newintent");
    }


    @Override
    public void onReq(BaseReq baseReq) {

    }

    @Override
    public void onResp(BaseResp baseResp) {

        Log.e("2255", "WXEntryActivity Resq");

        int m_PayResult = 0;

        if (baseResp.getType() == ConstantsAPI.COMMAND_LAUNCH_WX_MINIPROGRAM) {
            WXLaunchMiniProgram.Resp launchMiniProResp = (WXLaunchMiniProgram.Resp) baseResp;
            String extraData = launchMiniProResp.extMsg; // 对应下面小程序中的app-parameter字段的value

            //交易成功 SUCCESS
            //交易失败 FAIL
            //待支付 INIT
            //支付中 USERPAYING
            //已撤销 CANCELED
            //已过期 EXPIRED
            Log.e("2255", extraData + "");
            try {
                JSONObject json = new JSONObject(extraData);
                String payState = json.getString("payState");
                if (payState.equals("SUCCESS")) {
                    m_PayResult = 0;
                } else {
                    m_PayResult = -1;
                }

            } catch (JSONException e) {
                e.printStackTrace();
                m_PayResult = -1;
            }

            IPayCallBack m_IPayCallBack = WXPayHelper.getIPayCallBack();
            if (m_IPayCallBack != null) {
                m_IPayCallBack.payCallBack(m_PayResult);
            }

            finish();
        }else {
            super.onResp(baseResp);//一定要加super，实现我们的方法，否则不能回调
        }

    }
}
