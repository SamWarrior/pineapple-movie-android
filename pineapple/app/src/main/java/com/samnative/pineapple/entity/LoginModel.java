package com.samnative.pineapple.entity;

/**
 * Created by Wy on 2019/5/27.
 */

public class LoginModel {


    /**
     * code : 200
     * msg : success
     * data : {"username":"18507177046","type":"0","mtoken":"419bfaf6e5072063ce7adf1f18be3727","role":"3","mobile":"18507177046","rechargeStr":"","tradeoperurl":"/trade/index/tradeinstruct","mallurl":"/index/index/indexstatic"}
     */

    private String code;
    private String msg;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * username : 18507177046
         * type : 0
         * mtoken : 419bfaf6e5072063ce7adf1f18be3727
         * role : 3
         * mobile : 18507177046
         * rechargeStr :
         * tradeoperurl : /trade/index/tradeinstruct
         * mallurl : /index/index/indexstatic
         */

        private String username;
        private String type;
        private String mtoken;
        private String role;
        private String mobile;
        private String rechargeStr;
        private String tradeoperurl;
        private String mallurl;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getMtoken() {
            return mtoken;
        }

        public void setMtoken(String mtoken) {
            this.mtoken = mtoken;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getRechargeStr() {
            return rechargeStr;
        }

        public void setRechargeStr(String rechargeStr) {
            this.rechargeStr = rechargeStr;
        }

        public String getTradeoperurl() {
            return tradeoperurl;
        }

        public void setTradeoperurl(String tradeoperurl) {
            this.tradeoperurl = tradeoperurl;
        }

        public String getMallurl() {
            return mallurl;
        }

        public void setMallurl(String mallurl) {
            this.mallurl = mallurl;
        }
    }
}
