package com.samnative.pineapple.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.reflect.TypeToken;
import com.samnative.pineapple.adapter.HomeSearchAdapter;
import com.samnative.pineapple.entity.HomeListModel;
import com.samnative.pineapple.entity.KuaisuZixunModel;
import com.samnative.pineapple.http.HttpRequest;
import com.samnative.pineapple.http.JsonResult;
import com.samnative.pineapple.instance.AppPrefer;
import com.samnative.pineapple.main.MovieDetailActivity;
import com.samnative.pineapple.main.R;
import com.samnative.pineapple.utils.GsonUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 搜索页面
 */
public class HomeSearchFragment extends BaseFragment implements OnClickListener {

    private EditText et_search;
    private SmartRefreshLayout srl_fresh;
    private RecyclerView rv_content;
    private HomeSearchAdapter searchAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_home_search;
    }

    @Override
    protected void initView(View view, Bundle bundle) {

        et_search = find(R.id.et_search);
        find(R.id.tv_confirm).setOnClickListener(this);

        srl_fresh = find(R.id.srl_fresh);
        srl_fresh.setEnableLoadMore(false);
        srl_fresh.setEnableAutoLoadMore(false);
        srl_fresh.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                requestData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                requestData();
            }
        });

        searchAdapter=new HomeSearchAdapter(R.layout.item_home_searchmovie);
        searchAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Intent intent = new Intent(mContext, MovieDetailActivity.class);
                intent.putExtra("movieid", searchAdapter.getData().get(position).getMovieId());
                startActivity(intent);
            }
        });
        rv_content = find(R.id.rv_content);
        final GridLayoutManager layoutManager=new GridLayoutManager(mContext,3);
        rv_content.setLayoutManager(layoutManager);
        rv_content.setAdapter(searchAdapter);

        requestData();
    }


    public void requestData() {
        Map<String, String> map = new HashMap<>();
        map.put("mtoken", AppPrefer.getInstance(mContext).getString(AppPrefer.mtoken, ""));
        if (!et_search.getText().toString().isEmpty()) {
            map.put("keywords", et_search.getText().toString());
        }
        HttpRequest.request(getActivity(), "/client/movies/search", "", map, "GET", new JsonResult() {
            @Override
            public void reqestSuccess(JSONObject result) {
                try {
                    JSONArray jsonArray = result.getJSONArray("list");
                    List<HomeListModel> list = GsonUtils.fromJsonList(jsonArray.toString(), new TypeToken<List<HomeListModel>>() {
                    }.getType());
                    searchAdapter.getData().clear();
                    searchAdapter.addData(list);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void requestFinish() {
                super.requestFinish();
                srl_fresh.finishRefresh();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_confirm:

                requestData();

                break;
            default:
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
