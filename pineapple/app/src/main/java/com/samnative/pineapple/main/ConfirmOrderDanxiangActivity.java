package com.samnative.pineapple.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.samnative.pineapple.http.HttpRequest;
import com.samnative.pineapple.http.JsonResult;
import com.samnative.pineapple.instance.AppPrefer;
import com.samnative.pineapple.widget.LoadingDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ezy.ui.view.RoundButton;

/**
 * 确认订单=》单项订单
 */
public class ConfirmOrderDanxiangActivity extends BaseActivity implements View.OnClickListener {

    private TextView tv_text1, tv_text2, tv_text3, tv_text4;
    private TextView tv_shengyu, tv_date, tv_yingfu, tv_amount;
    private CheckBox cb1;
    private RoundButton btn_submit;
    private EditText et_content;

    private String id, orderNo;
    private String prouctprice = "", serviceprice = "1";

    @Override
    protected int getLayoutId() {
        return R.layout.activity_comfirm_order_danxiang;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        id = getIntent().getStringExtra("id");
        find(R.id.back_btn).setOnClickListener(this);

        tv_text1 = find(R.id.tv_text1);
        tv_text2 = find(R.id.tv_text2);
        tv_text3 = find(R.id.tv_text3);
        tv_text4 = find(R.id.tv_text4);
        tv_shengyu = find(R.id.tv_shengyu);
        tv_date = find(R.id.tv_date);
        tv_yingfu = find(R.id.tv_yingfu);
        tv_amount = find(R.id.tv_amount);
        et_content = find(R.id.et_content);

        cb1 = find(R.id.cb1);
        cb1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //勾选了
                    tv_yingfu.setText("已扣除" + serviceprice + "次，应付：");
                    tv_amount.setText("￥0.00");
                } else {
                    //未勾选
                    tv_yingfu.setText("应付：");
                    tv_amount.setText("￥" + prouctprice);
                }
            }
        });

        btn_submit = find(R.id.btn_submit);
        btn_submit.setOnClickListener(this);
        requestData();
    }


    @Override
    protected void initData() {
        super.initData();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                finish();
                break;
            case R.id.btn_submit:
                if (id == null) {
                    return;
                }
                addOrder();
                break;
        }
    }


    /**
     * 下订前的信息显示
     * <p>
     */
    private void requestData() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("mtoken", AppPrefer.getInstance(mContext).getString(AppPrefer.mtoken, ""));
        map.put("_apiname", "legal.index.serviceOrderInfo");
        map.put("id", id);
        HttpRequest.post((Activity) mContext, null, map, new JsonResult() {
            @Override
            public void reqestSuccess(JSONObject result) {
                try {
                    JSONObject jsonData = result.getJSONObject("data");
                    String title = jsonData.optString("title");
                    String lineprice = jsonData.optString("lineprice");
                    String categoryid = jsonData.optString("categoryid");
                    String category = jsonData.optString("category");
                    String is_role = jsonData.optString("is_role");
                    String endtime = jsonData.optString("endtime");
                    String serveramount = jsonData.optString("serveramount");
                    prouctprice = jsonData.optString("prouctprice");
                    serviceprice = jsonData.optString("serviceprice");

                    tv_text1.setText(category);
                    tv_text2.setText(title);
                    tv_text3.setText("￥" + prouctprice);
                    tv_shengyu.setText("剩余"+serveramount+"次");
                    tv_date.setText(endtime);
                    tv_amount.setText("￥" + prouctprice);
                    if (serveramount.equals("0")) {
                        cb1.setEnabled(false);
                    } else {
                        cb1.setEnabled(true);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void requestFinish() {
                super.requestFinish();
            }
        });
    }

    /**
     * 下订
     * <p>
     * {"code":"200","msg":"success","data":{"orderno":"PCK20211108144930223334"}
     */
    private void addOrder() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("_apiname", "legal.index.addServiceOrder");
        map.put("mtoken", AppPrefer.getInstance(mContext).getString(AppPrefer.mtoken, ""));
        map.put("id", id);
        map.put("remark", et_content.getText().toString());
        map.put("pay_type", cb1.isChecked() ? "2" : "1");
        HttpRequest.post((Activity) mContext, null, map, new JsonResult() {
            @Override
            public void reqestSuccess(JSONObject result) {
                try {
                    JSONObject jsonData = result.getJSONObject("data");
                    orderNo = jsonData.getString("orderno");

                    Intent intent = new Intent(mContext, ChoosePayActivity.class);
                    intent.putExtra("orderNo", orderNo);
                    intent.putExtra("orderType", 1);
                    startActivityForResult(intent, 220);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void requestFinish() {
                super.requestFinish();
            }
        });
    }

    /**
     * 支付结果处理
     */
    public void enterPayResult() {
        LoadingDialog.getInstance(mContext).setMessage("获取支付结果").show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                LoadingDialog.dismissDialog();
//                Intent intent = new Intent().setClass(context,
//                        OrderDetailActivity.class);
//                intent.putExtra("ORDER_NUM", orderNo);
//                startActivity(intent);
//                finish();
            }
        }, 1000);
    }


    @Override
    protected void onActivityResult(int arg0, int arg1, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(arg0, arg1, data);
        if (data == null) {
            return;
        }
        if (arg1 == 220) {
//            enterPayResult();
            finish();
        }
    }

}
