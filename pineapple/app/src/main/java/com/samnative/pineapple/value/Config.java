package com.samnative.pineapple.value;

/**
 * 参数配置类
 */

public class Config {

    //========================================================================后台接口

    /**
     * api 密钥
     */
    public static final String _API_KEY = "6717cb11670ef12f500cbe169c5276d5";
    /**
     * api 请求设备类型
     */
    public static final String _API_DEV = "AND";
    /**
     * api版本号
     */
    public static final String _API_VERSION = "1.1.0";


    /**
     * 测试环境配置
     */
    //API接口
    public static final String _API_URL = "http://101.33.206.34:3030";
    //后端web
    public static final String WEB_URL = "http://";


//    /**
//     * 生产环境配置
//     */
//    //API接口
//    public static final String _API_URL = "http://101.33.206.34:3030";
//    //后端web
//    public static final String WEB_URL = "http://";




    //========================================================================其他第三方库配置

    /**
     * 友盟的key
     */
    public static final String umengKey = "6193531ae0f9bb492b5e19c2";
    /**
     * QQ分享的ID
     */
    public static final String qqId = "1109552126";
    /**
     * QQ分享的KEY
     */
    public static final String qqKey = "QdNOROL44ZZslCI1";
    /**
     * 微信分享的ID
     */
    public static final String wechatId = "wx00b759a84c9e8fa9";
    /**
     * 微信分享的KEY
     */
    public static final String wechatKey = "d9292d090667f5ce669f43cac59c8c82";
    /**
     * 微博分享ID
     */
    public static final String sinaId = "1140667099";
    /**
     * 微博分享KEY
     */
    public static final String sinaKey = "4533cfd16eb8a5eb59fd0f603ccfc8ee";
    /**
     * 佳信客服KEY
     */
    public static final String jiaxinKey = "nxqxcmyydmczbq#szfztkj910#10001";



    //========================================================================本地service

    /**
     * 本地服务StartKey
     */
    public static final String SERVICE_KEY = "SERVICE_KEY";
    /**
     * 本地服务下载新版本key
     */
    public static final int SERVICE_DOWNLOAD_UPDATA = 100;
    /**
     * 本地服务下载其它文件key
     */
    public static final int SERVICE_DOWNLOAD_FILE = 101;
    /**
     * 监听新消息
     */
    public static final int SERVICE_RECIVER_MESSAGE = 102;


    //=========================================================================可配置的全局参数


    /**
     * UUID
     */
    public static String _uuid = "";
    /**
     * 设备的屏幕分辨率
     */
    public static String _dpi = "";
    /**
     * 当前网络类型
     */
    public static String _network = "";
    /**
     * 当前所在位置的经度
     */
    public static String longitude = "";
    /**
     * 当前所在位置的纬度
     */
    public static String latitude = "";
    /**
     * 当前所在的城市
     */
    public static String currentCity = "";

    /**
     * 当前定位所在的城市id
     */
    public static String currentCityid = "";

    // 1仅退款，2退货退款
    public static String REFUND_ID = "1";


    // =========================================banner,专题等跳转类型
    /**
     * 跳转到商品详情
     */
    public static final int INTO_TYPE_PRODETAL = 1;
    /**
     * 跳转到WEB(专题,广告,资讯等)
     */
    public static final int INTO_TYPE_WEB = 2;
    /**
     * 跳转到商家详情
     */
    public static final int INTO_TYPE_MALLHOME = 4;
    /**
     * 跳转到订单详情
     */
    public static final int INTO_TYPE_ORDER_DETAIL = 5;
    /**
     * 跳转到牛品分类
     */
    public static final int INTO_TYPE_PRO_CATEGORY = 6;
    /**
     * 跳转到登录注册
     */
    public static final int INTO_TYPE_LOGIN = 11;

    //==============================================常量

    /**
     * 图片缓存路径
     */
    public static final String SaveImgPath = "/pineapple/imgcache/";
    /**
     * 首页模块缓存文件夹
     */
    public static final String HOME_CACHE_PATH = "/home_cache/";
    /**
     * 商城模块缓存文件夹
     */
    public static final String MALL_CACHE_PATH = "/mall_cache/";
    /**
     * 临时图片数据保存文件夹
     */
    public static final String SAVE_PATH_IN_SDCARD = "/pineapple/temp/";
    /**
     * 拍照照片保存名称
     */
    public static final String IMAGE_CAPTURE_NAME = "img.jpeg";


}
