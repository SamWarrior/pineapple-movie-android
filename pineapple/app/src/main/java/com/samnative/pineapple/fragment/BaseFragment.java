package com.samnative.pineapple.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.samnative.pineapple.main.R;

public abstract class BaseFragment extends Fragment {

    public Context mContext;
    private View mContentView;

    protected abstract int getLayoutId();

    protected abstract void initView(View view, Bundle bundle);

    protected <T extends View> T find(int id) {
        return (T) this.mContentView.findViewById(id);
    }

    public void onAttach(Context context) {
        super.onAttach(context);
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mContext = getActivity();
    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }

    public void onStop() {
        super.onStop();
    }

    public void onDestroyView() {
        super.onDestroyView();
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mContentView == null) {
            this.mContentView = inflater.inflate(getLayoutId(), container, false);
            initView(this.mContentView, savedInstanceState);
        }
        loadData();
        return this.mContentView;
    }

    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void onStart() {
        super.onStart();
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    protected void loadData() {
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onDetach() {
        super.onDetach();
    }

    /**
     * 打开新Activity
     *
     * @param intent intent
     */
    public void startActivity(Intent intent) {
        startActivity(intent, true);
    }

    /**
     * 打开新Activity
     *
     * @param intent  intent
     * @param animIn  新Activity进入的动画
     * @param animOut 当前Activity退出的动画
     */
    public void startActivity(Intent intent, int animIn, int animOut) {
        super.startActivity(intent);
        getActivity().overridePendingTransition(animIn, animOut);
    }

    /**
     * 打开新的Activity
     *
     * @param intent intent
     * @param isAnim 是否开启过渡动画
     */
    public void startActivity(Intent intent, boolean isAnim) {
        if (isAnim) {
            startActivity(intent, R.anim.act_right_in, R.anim.act_left_out);
        } else {
            super.startActivity(intent);
        }
    }

    //

    /**
     * 打开新Activity
     *
     * @param intent intent
     */
    public void startActivityForResult(Intent intent, int requestCode) {
        startActivityForResult(intent, requestCode, true);
    }

    /**
     * 打开新Activity
     *
     * @param intent  intent
     * @param animIn  新Activity进入的动画
     * @param animOut 当前Activity退出的动画
     */
    public void startActivityForResult(Intent intent, int requestCode, int animIn, int animOut) {
        super.startActivityForResult(intent, requestCode);
        getActivity().overridePendingTransition(animIn, animOut);
    }

    /**
     * 打开新的Activity
     *
     * @param intent intent
     * @param isAnim 是否开启过渡动画
     */
    public void startActivityForResult(Intent intent, int requestCode, boolean isAnim) {
        if (isAnim) {
            startActivityForResult(intent, requestCode, R.anim.act_right_in, R.anim.act_left_out);
        } else {
            super.startActivityForResult(intent, requestCode);
        }
    }
}
