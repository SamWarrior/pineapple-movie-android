package com.samnative.pineapple.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.reflect.TypeToken;
import com.gyf.immersionbar.ImmersionBar;
import com.samnative.pineapple.adapter.HomeSearchAdapter;
import com.samnative.pineapple.entity.HomeListModel;
import com.samnative.pineapple.http.HttpRequest;
import com.samnative.pineapple.http.JsonResult;
import com.samnative.pineapple.instance.AppPrefer;
import com.samnative.pineapple.utils.GsonUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import com.shashank.sony.fancytoastlib.FancyToast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 我的收藏
 */
public class MineCollectActivity extends BaseActivity implements View.OnClickListener {


    private SmartRefreshLayout srl_fresh;
    private RecyclerView rv_content;
    private HomeSearchAdapter searchAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_mine_collect;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        ImmersionBar.with(this).statusBarDarkFont(true).init();

        find(R.id.tv_back).setOnClickListener(this);
        srl_fresh = find(R.id.srl_fresh);
        srl_fresh.setEnableLoadMore(false);
        srl_fresh.setEnableAutoLoadMore(false);
        srl_fresh.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                requestData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                requestData();
            }
        });

        searchAdapter=new HomeSearchAdapter(R.layout.item_home_searchmovie);
        searchAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Intent intent = new Intent(mContext, MovieDetailActivity.class);
                intent.putExtra("movieid", searchAdapter.getData().get(position).getMovieId());
                startActivity(intent);
            }
        });
        rv_content = find(R.id.rv_content);
        final GridLayoutManager layoutManager=new GridLayoutManager(mContext,3);
        rv_content.setLayoutManager(layoutManager);
        rv_content.setAdapter(searchAdapter);


    }


    @Override
    protected void onResume() {
        super.onResume();
        requestData();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_back:
                finish();
                break;
            default:
                break;
        }

    }

    public void requestData() {
        Map<String, String> map = new HashMap<>();
        HttpRequest.request((Activity) mContext, "/client/collections/colleclist", "", map, "GET", new JsonResult() {
            @Override
            public void reqestSuccess(JSONObject result) {
                try {
                    JSONArray jsonArray = result.getJSONArray("list");
                    List<HomeListModel> list = GsonUtils.fromJsonList(jsonArray.toString(), new TypeToken<List<HomeListModel>>() {
                    }.getType());
                    searchAdapter.getData().clear();
                    searchAdapter.addData(list);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void requestFinish() {
                super.requestFinish();
                srl_fresh.finishRefresh();
            }
        });
    }

}
