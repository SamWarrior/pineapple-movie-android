package com.samnative.pineapple.entity;

import java.io.Serializable;
import java.util.List;

/**
 * 首页的服务优选
 */
public class HomeYouxuanModel implements Serializable {

    /**
     * id : 1
     * name : 企业
     * list : [{"id":"1","title":"知识产权","thumb":"","categoryid":"1"}]
     */

    private String id;
    private String name;
    private List<ListBean> list;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean implements Serializable {
        /**
         * id : 1
         * title : 知识产权
         * thumb :
         * categoryid : 1
         */

        private String id;
        private String title;
        private String thumb;
        private String categoryid;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getThumb() {
            return thumb;
        }

        public void setThumb(String thumb) {
            this.thumb = thumb;
        }

        public String getCategoryid() {
            return categoryid;
        }

        public void setCategoryid(String categoryid) {
            this.categoryid = categoryid;
        }
    }
}
