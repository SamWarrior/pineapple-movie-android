package com.samnative.pineapple.entity;

import com.chad.library.adapter.base.entity.MultiItemEntity;

import java.util.List;

/**
 * 首页列表mocel,适用产品目录,研究项目,科普走廊等
 */
public class HomeListModel implements MultiItemEntity {

    private int showtype=1;
    /**
     * score : 8.6
     * counts : 100
     * _id : 60e5b43683efac13d02b89df
     * movieId : m002
     * movieName : 中国医生
     * originaName : 中国医生
     * info : 中国大陆 / 剧情 / 灾难
     * releaseDate : 2021-07-09(中国大陆)
     * totalTime : 120
     * movieUrl : http://vfx.mtime.cn/Video/2021/07/02/mp4/210702155200691112.mp4
     * plotDesc : 电影根据新冠肺炎疫情防控斗争的真实事件改编，以武汉市金银潭医院为核心故事背景，同时兼顾武汉同济医院、武汉市肺科医院、武汉协和医院、武汉大学人民医院（湖北省人民医院）、火神山医院、方舱医院等兄弟单位，以武汉医护人员、全国各省市援鄂医疗队为人物原型，全景式记录波澜壮阔、艰苦卓绝的抗疫斗争。
     * cover : http://movieapi.chunyi.vip/uploads/6683b55751c59b77a4fd65d83b593dbc
     * plotsWords : [{"_id":"60e5c41e83efac13d02b89eb","autor":"刘伟强","actorRole":"导演","imgUrl":"http://localhost:3030/uploads/d6343ba198311cfd4ca11b513350d6f9"},{"_id":"60e5c46f83efac13d02b89ee","imgUrl":"http://localhost:3030/uploads/edb11d8b29012552284d5c09993c8e87","autor":"张涵予","actorRole":"主演"},{"_id":"60e5c46f83efac13d02b89ed","imgUrl":"http://localhost:3030/uploads/c0ed1b9072171436e7bc529322db8001","actorRole":"主演","autor":"袁泉"},{"_id":"60e5c46f83efac13d02b89ec","imgUrl":"http://localhost:3030/uploads/1986d652a2f9253ccf13f7c3fd71b0e3","actorRole":"主演","autor":"朱亚文"}]
     * plotsImg : [{"_id":"60e5c4b983efac13d02b89f7","imgUrl":"http://localhost:3030/uploads/1cc31c04a63b3e553d2b408994ff6518"},{"_id":"60e5c4b983efac13d02b89f6","imgUrl":"http://localhost:3030/uploads/8c6d60fffad882c170a729751ab6da37"},{"_id":"60e5c4b983efac13d02b89f5","imgUrl":"http://localhost:3030/uploads/29adc03a26952e3c1fff46eb92e5f225"},{"_id":"60e5c4b983efac13d02b89f4","imgUrl":"http://localhost:3030/uploads/8788c8d52f69674f15e939a74cbb5d00"}]
     * __v : 0
     */

    private double score;
    private int counts;
    private String _id;
    private String movieId;
    private String movieName;
    private String originaName;
    private String info;
    private String releaseDate;
    private String totalTime;
    private String movieUrl;
    private String plotDesc;
    private String cover;
    private List<PlotsWordsBean> plotsWords;
    private List<PlotsImgBean> plotsImg;
    private String iscollec;

    @Override
    public int getItemType() {
        if (showtype == 1 || showtype == 2 || showtype == 3) {
            return showtype;
        } else {
            return 4;
        }
    }

    public String getIscollec() {
        return iscollec;
    }

    public void setIscollec(String iscollec) {
        this.iscollec = iscollec;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public int getCounts() {
        return counts;
    }

    public void setCounts(int counts) {
        this.counts = counts;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getMovieId() {
        return movieId;
    }

    public void setMovieId(String movieId) {
        this.movieId = movieId;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public String getOriginaName() {
        return originaName;
    }

    public void setOriginaName(String originaName) {
        this.originaName = originaName;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(String totalTime) {
        this.totalTime = totalTime;
    }

    public String getMovieUrl() {
        return movieUrl;
    }

    public void setMovieUrl(String movieUrl) {
        this.movieUrl = movieUrl;
    }

    public String getPlotDesc() {
        return plotDesc;
    }

    public void setPlotDesc(String plotDesc) {
        this.plotDesc = plotDesc;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public List<PlotsWordsBean> getPlotsWords() {
        return plotsWords;
    }

    public void setPlotsWords(List<PlotsWordsBean> plotsWords) {
        this.plotsWords = plotsWords;
    }

    public List<PlotsImgBean> getPlotsImg() {
        return plotsImg;
    }

    public void setPlotsImg(List<PlotsImgBean> plotsImg) {
        this.plotsImg = plotsImg;
    }


    public static class PlotsWordsBean {
        /**
         * _id : 60e5c41e83efac13d02b89eb
         * autor : 刘伟强
         * actorRole : 导演
         * imgUrl : http://localhost:3030/uploads/d6343ba198311cfd4ca11b513350d6f9
         */

        private String _id;
        private String autor;
        private String actorRole;
        private String imgUrl;

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public String getAutor() {
            return autor;
        }

        public void setAutor(String autor) {
            this.autor = autor;
        }

        public String getActorRole() {
            return actorRole;
        }

        public void setActorRole(String actorRole) {
            this.actorRole = actorRole;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }
    }

    public static class PlotsImgBean {
        /**
         * _id : 60e5c4b983efac13d02b89f7
         * imgUrl : http://localhost:3030/uploads/1cc31c04a63b3e553d2b408994ff6518
         */

        private String _id;
        private String imgUrl;

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }
    }
}
