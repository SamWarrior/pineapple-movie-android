package com.samnative.pineapple.widget;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.samnative.pineapple.main.R;
import com.samnative.pineapple.main.Webview2Activity;
import com.samnative.pineapple.value.Config;


/**
 * 温馨提示
 */
public class WenxinDialog extends Dialog {

    /**
     * 上下文
     */
    private Context context;
    /**
     * 视图
     */
    private View view;
    /**
     * 监听接口
     */
    private Click click;
    /**
     * 控制按钮
     */
    private TextView tv_no, tv_ok;
    /**
     * 内容
     */
    private TextView tv_msg;

    public static WenxinDialog instance;

    public static void dismissDialog() {
        if (instance != null && instance.isShowing()) {
            try {
                instance.dismiss();
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
        }
    }

    public static WenxinDialog getInstance(Context context, Click click) {

        if (instance != null && instance.isShowing()) {
            instance.dismiss();
            instance = null;
        }
        instance = new WenxinDialog(context, click);
        return instance;
    }

    public WenxinDialog(Context context, final Click click) {
        super(context, R.style.DialogTheme);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        this.setCanceledOnTouchOutside(false);
        this.setCancelable(false);
        this.context = context;
        this.click = click;
        view = getLayoutInflater().inflate(R.layout.dialog_wenxin, null);
        tv_ok = (TextView) view.findViewById(R.id.tv_ok);
        tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                click.ok();
                dismiss();

            }
        });
        tv_no = (TextView) view.findViewById(R.id.tv_no);
        tv_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                click.cancel();
                dismiss();
            }
        });
        tv_msg = view.findViewById(R.id.tv_msg);
        setXieyiStyle(tv_msg);
        this.setContentView(view);
    }


    @Override
    public void show() {
        if (instance != null && context != null) {
            super.show();
        } else {
            instance = null;

        }
    }

    /**
     * 设置协议样式
     */
    public void setXieyiStyle(TextView textview) {

        String str = textview.getText().toString();

        SpannableStringBuilder ssb = new SpannableStringBuilder();
        ssb.append(str);

        final int start = str.indexOf("《");//第一个出现的位置
        ssb.setSpan(new ClickableSpan() {

            @Override
            public void onClick(View widget) {
//                Toast.makeText(mContext, "《用户协议》",
//                        Toast.LENGTH_SHORT).show();
                Intent intent1 = new Intent(context, Webview2Activity.class);
                intent1.putExtra("url", Config.WEB_URL + "/Introduction/Index/registDeal");
                context.startActivity(intent1);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(context.getResources().getColor(R.color.main_color));       //设置文件颜色
                // 去掉下划线
                ds.setUnderlineText(false);
            }

        }, start, start + 8, 0);

        final int end = str.lastIndexOf("《");//最后一个出现的位置
        ssb.setSpan(new ClickableSpan() {

            @Override
            public void onClick(View widget) {
//                Toast.makeText(mContext, "《隐私政策》",
//                        Toast.LENGTH_SHORT).show();
                Intent intent1 = new Intent(context, Webview2Activity.class);
                intent1.putExtra("url", Config.WEB_URL + "/Introduction/index/privacy");
                context.startActivity(intent1);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(context.getResources().getColor(R.color.main_color));       //设置文件颜色
                // 去掉下划线
                ds.setUnderlineText(false);
            }

        }, end, end + 6, 0);

        textview.setMovementMethod(LinkMovementMethod.getInstance());
        textview.setText(ssb, TextView.BufferType.SPANNABLE);
    }

    public interface Click {
        public void ok();

        public void cancel();
    }

    public void setClick(Click click) {
        this.click = click;
    }
}
