package com.samnative.pineapple.entity;

/**
 * 检测更新model
 */
public class UpdataModel {
    /**
     * 0 无更新 1非强制更新 2强制更新
     */
    private String update;
    private String url;
    private String info;
    private String title;
    private String remark;

    private String btnEnterText;
    private String btnCancelText;

    public String getBtnEnterText() {
        return btnEnterText;
    }

    public void setBtnEnterText(String btnEnterText) {
        this.btnEnterText = btnEnterText;
    }

    public String getBtnCancelText() {
        return btnCancelText;
    }

    public void setBtnCancelText(String btnCancelText) {
        this.btnCancelText = btnCancelText;
    }

    private String new_version;

    public String getUpdate() {
        return update;
    }

    public void setUpdate(String update) {
        this.update = update;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getNew_version() {
        return new_version;
    }

    public void setNew_version(String new_version) {
        this.new_version = new_version;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
