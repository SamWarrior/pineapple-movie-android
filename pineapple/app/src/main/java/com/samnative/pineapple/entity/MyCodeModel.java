package com.samnative.pineapple.entity;

/**
 * 我的二维码
 * Created by Wy on 2018/5/12.
 */

public class MyCodeModel {

    /**
     * data : {"url":"http: //192.168.1.16: 10997/?_apiname=user.index.myCode&&userid=21&checkcode=38d09cde3eea905b6455d32d88849462&type=3&role=1&recorole=1","myCode":"w123456","content":"我的推荐码","address":"http: //www.niuniuhuiapp.net: 30970/user/register/register?parentid=21&checkcode=38d09cde3eea905b6455d32d88849462","bottomContent":"扫一扫推荐码"}
     */

    private String code;
    private String msg;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * url : http: //192.168.1.16: 10997/?_apiname=user.index.myCode&&userid=21&checkcode=38d09cde3eea905b6455d32d88849462&type=3&role=1&recorole=1
         * myCode : w123456
         * content : 我的推荐码
         * address : http: //www.niuniuhuiapp.net: 30970/user/register/register?parentid=21&checkcode=38d09cde3eea905b6455d32d88849462
         * bottomContent : 扫一扫推荐码
         */

        private String url;
        private String myCode;
        private String content;
        private String address;
        private String bottomContent;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getMyCode() {
            return myCode;
        }

        public void setMyCode(String myCode) {
            this.myCode = myCode;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getBottomContent() {
            return bottomContent;
        }

        public void setBottomContent(String bottomContent) {
            this.bottomContent = bottomContent;
        }
    }
}
