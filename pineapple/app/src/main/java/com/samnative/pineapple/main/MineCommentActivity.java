package com.samnative.pineapple.main;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.reflect.TypeToken;
import com.samnative.pineapple.adapter.MineCommentAdapter;
import com.samnative.pineapple.adapter.MyTeamAdapter;
import com.samnative.pineapple.entity.HomeListModel;
import com.samnative.pineapple.entity.MineCommentModel;
import com.samnative.pineapple.entity.TeamModel;
import com.samnative.pineapple.http.HttpRequest;
import com.samnative.pineapple.http.JsonResult;
import com.samnative.pineapple.instance.AppPrefer;
import com.samnative.pineapple.utils.GsonUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 我的评论
 */
public class MineCommentActivity extends BaseActivity implements BaseQuickAdapter.OnItemClickListener {

    private MineCommentAdapter mineCommentAdapter;
    private RecyclerView rlv_content;
    private TextView tv_null_data;
    private SmartRefreshLayout srl_fresh;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_mine_comment;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        find(R.id.rl_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        srl_fresh = find(R.id.srl_fresh);
        srl_fresh.setEnableLoadMore(false);
        tv_null_data = find(R.id.tv_null_data);
        rlv_content = find(R.id.rlv_content);
        mineCommentAdapter = new MineCommentAdapter(R.layout.item_mine_comment);
        mineCommentAdapter.setOnItemClickListener(this);
        rlv_content.setLayoutManager(new LinearLayoutManager(mContext));
        rlv_content.setAdapter(mineCommentAdapter);

        srl_fresh.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                requestData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                requestData();
            }
        });

        requestData();

    }

    public void requestData() {
        Map<String, String> map = new HashMap<>();
        HttpRequest.request((Activity) mContext, "/client/comments/mycomments", "", map, "GET", new JsonResult() {
            @Override
            public void reqestSuccess(JSONObject result) {
                try {
                    JSONArray jsonArray = result.getJSONArray("list");
                    List<MineCommentModel> list = GsonUtils.fromJsonList(jsonArray.toString(), new TypeToken<List<MineCommentModel>>() {
                    }.getType());
                    mineCommentAdapter.getData().clear();
                    mineCommentAdapter.addData(list);

                    if (mineCommentAdapter.getData().size()>0){
                        tv_null_data.setVisibility(View.GONE);
                    }else {
                        tv_null_data.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void requestFinish() {
                super.requestFinish();
                srl_fresh.finishRefresh();
            }
        });
    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
    }
}
