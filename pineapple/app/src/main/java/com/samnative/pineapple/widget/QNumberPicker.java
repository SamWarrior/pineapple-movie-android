package com.samnative.pineapple.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.NumberPicker;


import com.samnative.pineapple.main.R;

import java.lang.reflect.Field;

public class QNumberPicker extends NumberPicker implements View.OnFocusChangeListener {

    private int numperPickerTextColor;

    public QNumberPicker(Context context) {
        super(context);
    }

//	public QNumberPicker(Context context,int numperPickerTextColor) {
//		super(context);
//
////		this.numperPickerTextColor = numperPickerTextColor;
//	}

    public QNumberPicker(Context context, AttributeSet attrs) {
        super(context, attrs);
//		this.numperPickerTextColor = 0x332c2b;
    }

    public QNumberPicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
//		this.numperPickerTextColor = 0x332c2b;
    }

    @Override
    public void addView(View child) {
        super.addView(child);
        updateView(child);
    }

    @Override
    public void addView(View child, int index,
                        android.view.ViewGroup.LayoutParams params) {
        super.addView(child, index, params);
        updateView(child);
    }

    @Override
    public void addView(View child, android.view.ViewGroup.LayoutParams params) {
        super.addView(child, params);
        updateView(child);
    }


    @SuppressLint("ResourceAsColor")
    public void updateView(View view) {
        if (view instanceof EditText) {

            // 这里修改字体的属性
//			((EditText) view).setTextColor(ContextCompat.getColor(getContext(), R.color.color_black));
            ((EditText) view).setTextColor(R.color.tab_text_color);
            // ((EditText) view).setTextSize();
        }
    }


    /**
     * 设置NumberPicker分割线颜色和高度
     *
     * @param numberPicker：NumberPicker
     * @param color：分割线颜色               16位
     * @param heigth                    分割线高度  px
     */
    public static void setNumberPickerDividerColor(NumberPicker numberPicker, int color, int heigth) {
        Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        //分割线颜色
        for (Field SelectionDividerField : pickerFields) {
            if (SelectionDividerField.getName().equals("mSelectionDivider")) {
                SelectionDividerField.setAccessible(true);
                try {
                    SelectionDividerField.set(numberPicker, new ColorDrawable(color));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }

        // 分割线高度
        for (Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDividerHeight")) {
                pf.setAccessible(true);
                try {

                    pf.set(numberPicker, heigth);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {

    }
}