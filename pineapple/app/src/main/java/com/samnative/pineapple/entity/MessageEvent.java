package com.samnative.pineapple.entity;

/**
 * 消息总线的消息实体
 * Created by Administrator on 2019/5/15.
 */
public class MessageEvent {

    /**
     * APP更新下载进度
     */
    private int downLoadPro = -1;

    public int getDownLoadPro() {
        return downLoadPro;
    }

    public void setDownLoadPro(int downLoadPro) {
        this.downLoadPro = downLoadPro;
    }


    /**
     * APP是否下载完成
     */
    private boolean downLoadEnd;

    public boolean isDownLoadEnd() {
        return downLoadEnd;
    }

    public void setDownLoadEnd(boolean downLoadEnd) {
        this.downLoadEnd = downLoadEnd;
    }


    /**
     * 识别退出登录操作指令
     */
    private int event = -1;

    public int getEvent() {
        return event;
    }

    public void setEvent(int event) {
        this.event = event;
    }


    /**
     * 下载其它文件是否完成
     */
    private boolean isOtherDownloadEnd;

    public boolean isOtherDownloadEnd() {
        return isOtherDownloadEnd;
    }

    public void setOtherDownloadEnd(boolean otherDownloadEnd) {
        isOtherDownloadEnd = otherDownloadEnd;
    }
}
