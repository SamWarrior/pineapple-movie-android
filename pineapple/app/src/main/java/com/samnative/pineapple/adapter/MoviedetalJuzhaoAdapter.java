package com.samnative.pineapple.adapter;

import android.support.annotation.LayoutRes;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.samnative.pineapple.entity.HomeListModel;
import com.samnative.pineapple.main.R;
import com.samnative.pineapple.utils.GlideRequestOptions;
import com.samnative.pineapple.value.Config;

/**
 * 电影详情——剧照
 */
public class MoviedetalJuzhaoAdapter extends BaseQuickAdapter<HomeListModel.PlotsImgBean, BaseViewHolder> {

    public MoviedetalJuzhaoAdapter(@LayoutRes int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, final HomeListModel.PlotsImgBean item) {

        ImageView iv_image = helper.getView(R.id.iv_image);
        Glide.with(mContext)
                .load(Config._API_URL + item.getImgUrl())//图片地址
                .apply(GlideRequestOptions.OPTIONS_400_400)
                .into(iv_image);

    }

}
