package com.samnative.pineapple.main;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;


import com.google.gson.reflect.TypeToken;
import com.samnative.pineapple.adapter.LianxiWomenAdapter;
import com.samnative.pineapple.entity.AboutModel;
import com.samnative.pineapple.http.HttpRequest;
import com.samnative.pineapple.http.JsonResult;
import com.samnative.pineapple.utils.GsonUtils;
import com.samnative.pineapple.utils.PackageUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 关于我们
 */

public class AboutActivity extends BaseActivity implements View.OnClickListener {

    private TextView tv_version;

    private RecyclerView rlv_content;
    private LianxiWomenAdapter aboutAdapter;

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_back:
                finish();
                break;
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_about;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);

        find(R.id.tv_back).setOnClickListener(this);

        tv_version = find(R.id.tv_version);
        tv_version.setText(getString(R.string.app_name) + "\nv" + PackageUtils.getAppVersionName(mContext));

        aboutAdapter = new LianxiWomenAdapter(R.layout.item_about_layout);
        rlv_content = find(R.id.rlv_content);
        rlv_content.setLayoutManager(new LinearLayoutManager(mContext));
        rlv_content.setAdapter(aboutAdapter);

        requestData();
    }


    /**
     * 获取用户信息
     */
    public void requestData() {
        Map<String, String> map = new HashMap<>();
        HttpRequest.request(this, "/client/about/aboutinfo", "", map, "GET", new JsonResult() {
            @Override
            public void reqestSuccess(JSONObject result) {
                try {
                    JSONArray jsonArr = result.getJSONArray("data");
                    List<AboutModel> list = GsonUtils.fromJsonList(jsonArr.toString(), new TypeToken<List<AboutModel>>() {
                    }.getType());
                    aboutAdapter.getData().clear();
                    aboutAdapter.addData(list);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

}
