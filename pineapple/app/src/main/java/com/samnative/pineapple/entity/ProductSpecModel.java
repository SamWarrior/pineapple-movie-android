package com.samnative.pineapple.entity;

import java.util.List;

/**
 * 商品规格model
 * 
 * @author WangPeng
 * 
 */
public class ProductSpecModel {

	private String id = "";
	private String spec_name = "";
	private String f_images = "";
	private List<Value> value;

	public String getF_images() {
		return f_images;
	}

	public void setF_images(String f_images) {
		this.f_images = f_images;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSpec_name() {
		return spec_name;
	}

	public void setSpec_name(String spec_name) {
		this.spec_name = spec_name;
	}

	@Override
	public String toString() {
		return "ProductSpecModel [id=" + id + ", spec_name=" + spec_name
				+ ", value=" + value + "]";
	}

	public List<Value> getValue() {
		return value;
	}

	public void setValue(List<Value> value) {
		this.value = value;
	}

	public class Value {
		private String id = "";
		private String parent_id = "";
		private String spec_value = "";

		public String getId() {
			return id;
		}

		public String getSpec_value() {
			return spec_value;
		}

		public void setSpec_value(String spec_value) {
			this.spec_value = spec_value;
		}

		public String getParent_id() {
			return parent_id;
		}

		public void setParent_id(String parent_id) {
			this.parent_id = parent_id;
		}

		public void setId(String id) {
			this.id = id;
		}

	}
}
