package com.samnative.pineapple.widget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.PopupWindow;

import com.samnative.pineapple.adapter.ChooseAreaAdapter;
import com.samnative.pineapple.entity.UserTypeModel;
import com.samnative.pineapple.main.R;

import java.util.List;


/**
 * 下拉选择
 */
public class UserTypePop extends PopupWindow implements OnClickListener {

    private Context context;
    private List<UserTypeModel.DataBean.ListBean> list;
    private AdapterView.OnItemClickListener onItemClickListener;
    private View view;
    private ListView lv_coin;
    private ChooseAreaAdapter adapter;
    private LinearLayout rl_content;

    public UserTypePop(Context context, List<UserTypeModel.DataBean.ListBean> list,
                       AdapterView.OnItemClickListener onItemClickListener) {
        super();
        this.context = context;
        this.list = list;
        this.onItemClickListener = onItemClickListener;
        adapter = new ChooseAreaAdapter(context, list);
        init();
    }

    private void init() {
        // TODO Auto-generated method stub
        view = LayoutInflater.from(context).inflate(R.layout.choose_area_pop, null);
        lv_coin = (ListView) view.findViewById(R.id.lv_coin);

        rl_content = (LinearLayout) view.findViewById(R.id.rl_content);
        lv_coin.setAdapter(adapter);
        lv_coin.setOnItemClickListener(onItemClickListener);
        setContentView(view);
        setWidth(LayoutParams.WRAP_CONTENT);
        setHeight(LayoutParams.WRAP_CONTENT);
        setFocusable(true);
//        setAnimationStyle(R.style.AnimTop);
    }

    public LinearLayout getRl_content() {
        return rl_content;
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

        switch (v.getId()) {
            default:
                break;
        }

    }

}
