package com.samnative.pineapple.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.samnative.pineapple.main.LoginActivity;
import com.samnative.pineapple.main.Webview2Activity;
import com.samnative.pineapple.value.Config;

public class EnterByTypeUtils {


    /**
     * 按类型跳转
     * 跳转类型
     *
     * @param id
     * @param title
     */
    public static void enterType(final Context context, String typestr,
                                 String id, String title) {
        try {
            Intent intent = new Intent();
            Class<?> c = null;
            int type = Integer.parseInt(typestr);
            switch (type) {
                case Config.INTO_TYPE_WEB:
                    intent.putExtra("url", id);
                    c = Webview2Activity.class;
                    break;
                case Config.INTO_TYPE_MALLHOME:
                    break;
                case Config.INTO_TYPE_LOGIN:
                    c = LoginActivity.class;
                    break;
            }

            if (c != null) {
                intent.setClass(context, c);
                ((Activity) context).startActivity(intent);
            }

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }
}
