package com.samnative.pineapple.entity;

import java.io.Serializable;

/**
 * 银行卡信息
 */
public class BankcardModel implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    private String bank_name;
    private String account_type;
    private String account_number;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getAccount_type() {
        return account_type;
    }

    public void setAccount_type(String account_type) {
        this.account_type = account_type;
    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

}