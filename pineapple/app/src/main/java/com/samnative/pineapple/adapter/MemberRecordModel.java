package com.samnative.pineapple.adapter;

import java.io.Serializable;

/**
 * 我的会员=>记录明细
 */
public class MemberRecordModel implements Serializable {


    /**
     * id : 12
     * orderno : PCK20211110143036927618
     * flowtype : 2419
     * remark : 20000
     * fromuserid : 0
     * direction : 1
     * amount : 62
     * flowtime : 2021-11-10 14: 34: 22
     * flowkey : 加
     * flowname : 购买套餐获得
     * datetime : 2021-11
     * mobile :
     */

    private String id;
    private String orderno;
    private String flowtype;
    private String remark;
    private String fromuserid;
    private String direction;
    private String amount;
    private String flowtime;
    private String flowkey;
    private String flowname;
    private String datetime;
    private String mobile;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getFlowtype() {
        return flowtype;
    }

    public void setFlowtype(String flowtype) {
        this.flowtype = flowtype;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getFromuserid() {
        return fromuserid;
    }

    public void setFromuserid(String fromuserid) {
        this.fromuserid = fromuserid;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getFlowtime() {
        return flowtime;
    }

    public void setFlowtime(String flowtime) {
        this.flowtime = flowtime;
    }

    public String getFlowkey() {
        return flowkey;
    }

    public void setFlowkey(String flowkey) {
        this.flowkey = flowkey;
    }

    public String getFlowname() {
        return flowname;
    }

    public void setFlowname(String flowname) {
        this.flowname = flowname;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
