package com.samnative.pineapple.entity;

/**
 * 商品SKU model
 *
 * @author WangPeng
 */
public class ProductSkuModel {

    private String id = "";
    private String aotusku = "";
    private String productid = "";
    private String prouctprice = "";
    private String marketprice = "";
    private String f_productspec = "";
    private String bullamount;
    private int productstorage;
    private String productimage;

    public int getProductstorage() {
        return productstorage;
    }

    public void setProductstorage(int productstorage) {
        this.productstorage = productstorage;
    }

    public String getProductimage() {
        return productimage;
    }

    public void setProductimage(String productimage) {
        this.productimage = productimage;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAotusku() {
        return aotusku;
    }

    public void setAotusku(String aotusku) {
        this.aotusku = aotusku;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getProuctprice() {
        return prouctprice;
    }

    public void setProuctprice(String prouctprice) {
        this.prouctprice = prouctprice;
    }

    public String getMarketprice() {
        return marketprice;
    }

    public void setMarketprice(String marketprice) {
        this.marketprice = marketprice;
    }

    public String getF_productspec() {
        return f_productspec;
    }

    public void setF_productspec(String f_productspec) {
        this.f_productspec = f_productspec;
    }

    public String getBullamount() {
        return bullamount;
    }

    public void setBullamount(String bullamount) {
        this.bullamount = bullamount;
    }

}
