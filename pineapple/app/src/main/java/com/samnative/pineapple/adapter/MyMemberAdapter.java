package com.samnative.pineapple.adapter;

import android.support.v4.content.ContextCompat;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.samnative.pineapple.main.R;

/**
 * 我的会员
 */
public class MyMemberAdapter extends BaseQuickAdapter<MemberRecordModel, BaseViewHolder> {

    public MyMemberAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, MemberRecordModel item) {
        TextView tv_name = helper.getView(R.id.tv_name);
        TextView tv_date = helper.getView(R.id.tv_date);
        TextView tv_count = helper.getView(R.id.tv_count);
        TextView tv_amount = helper.getView(R.id.tv_amount);

        tv_name.setText(item.getFlowname());
        tv_date.setText(item.getFlowtime());

        //这里的remark是金额，amount是次数，特此注释
        tv_amount.setText("￥" + item.getRemark());

        String count = item.getAmount();
        if (item.getDirection().equals("1")) {//+
            tv_count.setTextColor(ContextCompat.getColor(mContext, R.color.main_color));
            if (count.startsWith("+")) {
                tv_count.setText(count);
            } else {
                tv_count.setText("+" + count + "次");
            }
        } else {//-
            tv_count.setTextColor(ContextCompat.getColor(mContext, R.color.main_tab_text_n));
            if (count.startsWith("-")) {
                tv_count.setText(count);
            } else {
                tv_count.setText("-" + count + "次");
            }
        }

    }
}