package com.samnative.pineapple.utils;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.samnative.pineapple.main.R;

/**
 * Created by Wy on 2019/8/12.
 */

public class GlideRequestOptions {

    public static RequestOptions OPTIONS_600_300 = new RequestOptions()
            .placeholder(R.mipmap.ic_default_600_300)//图片加载出来前，显示的图片
//                            .fallback( R.drawable.img_blank) //url为空的时候,显示的图片
            .error(R.mipmap.ic_default_600_300);//图片加载失败后，显示的图片


    public static RequestOptions OPTIONS_400_400 = new RequestOptions()
            .placeholder(R.mipmap.ic_default_400_400)//图片加载出来前，显示的图片
            .error(R.mipmap.ic_default_400_400);//图片加载失败后，显示的图片


    /**
     * 默认头像
     */
    public static RequestOptions DEFAULT_HEAD = new RequestOptions()
            .placeholder(R.mipmap.default_head_portrait)//图片加载出来前，显示的图片
//                            .fallback( R.drawable.img_blank) //url为空的时候,显示的图片
            .error(R.mipmap.default_head_portrait);//图片加载失败后，显示的图片
    /**
     * 默认头像2
     */
    public static RequestOptions PATIENT_HEAD = new RequestOptions()
            .placeholder(R.mipmap.ic_patient_head)//图片加载出来前，显示的图片
//                            .fallback( R.drawable.img_blank) //url为空的时候,显示的图片
            .error(R.mipmap.ic_patient_head);//图片加载失败后，显示的图片


    /**
     * 不使用缓存的
     */
    public static RequestOptions NODISKCACHE = new RequestOptions()
            .skipMemoryCache(true) // 不使用内存缓存
            .diskCacheStrategy(DiskCacheStrategy.NONE);

    /**
     * 默认添加图片
     */
    public static RequestOptions ADD_PICTURES = new RequestOptions()
            .placeholder(R.mipmap.add_pictures)//图片加载出来前，显示的图片
//                            .fallback( R.drawable.img_blank) //url为空的时候,显示的图片
            .error(R.mipmap.add_pictures);//图片加载失败后，显示的图片

}
