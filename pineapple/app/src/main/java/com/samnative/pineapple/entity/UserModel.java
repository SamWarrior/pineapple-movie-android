package com.samnative.pineapple.entity;

/**
 * Created by Wy on 2019/5/27.
 */

public class UserModel {


    /**
     * code : 200
     * msg : success
     * data : {"userinfo":{"_id":"1651561","nickname":"大菠","username":"13800138000","headerpic":"uploads/05210ba68351ded0b9a4ed89246388dc"}}
     */

    private int code;
    private String msg;
    private UserBean user;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public UserBean getData() {
        return user;
    }

    public void setData(UserBean data) {
        this.user = data;
    }

    public static class UserBean {


        private String nickname;
        private String username;
        private String headicon;
        private String address;


        private String isnameauth = "0";
        private String payDec = "0";
        private String isloginpwd = "1";

        public String getHeadicon() {
            return headicon;
        }

        public void setHeadicon(String headicon) {
            this.headicon = headicon;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getIsloginpwd() {
            return isloginpwd;
        }

        public void setIsloginpwd(String isloginpwd) {
            this.isloginpwd = isloginpwd;
        }

        public String getPayDec() {
            return payDec;
        }

        public void setPayDec(String payDec) {
            this.payDec = payDec;
        }

        public String getIsnameauth() {
            return isnameauth;
        }

        public void setIsnameauth(String isnameauth) {
            this.isnameauth = isnameauth;
        }


        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

    }
}
