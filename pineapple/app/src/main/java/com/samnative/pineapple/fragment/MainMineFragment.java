package com.samnative.pineapple.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kongzue.dialog.interfaces.OnDialogButtonClickListener;
import com.kongzue.dialog.util.BaseDialog;
import com.kongzue.dialog.v3.MessageDialog;
import com.samnative.pineapple.entity.UserModel;
import com.samnative.pineapple.http.HttpRequest;
import com.samnative.pineapple.http.JsonResult;
import com.samnative.pineapple.instance.AppPrefer;
import com.samnative.pineapple.main.AboutActivity;
import com.samnative.pineapple.main.AddressManagerActivity;
import com.samnative.pineapple.main.FankuiActivity;
import com.samnative.pineapple.main.HelpCenterActivity;
import com.samnative.pineapple.main.LoginActivity;
import com.samnative.pineapple.main.MineCollectActivity;
import com.samnative.pineapple.main.MineCommentActivity;
import com.samnative.pineapple.main.MineNoticeActivity;
import com.samnative.pineapple.main.MineSettingActivity;
import com.samnative.pineapple.main.MyBalanceActivity;
import com.samnative.pineapple.main.MyCodeActivity;
import com.samnative.pineapple.main.MyInfoActivity;
import com.samnative.pineapple.main.MyMemberActivity;
import com.samnative.pineapple.main.MyTeamActivity;
import com.samnative.pineapple.main.R;
import com.samnative.pineapple.main.Webview2Activity;
import com.samnative.pineapple.utils.GlideRequestOptions;
import com.samnative.pineapple.value.Config;
import com.kongzue.stacklabelview.StackLabel;
import com.makeramen.roundedimageview.RoundedImageView;
import com.shashank.sony.fancytoastlib.FancyToast;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ezy.ui.view.RoundButton;

/**
 * 我的
 */
public class MainMineFragment extends BaseFragment implements OnClickListener {

    private TextView nickname_tv;
    private TextView tv_userid;
    private RoundedImageView image;
    private StackLabel stackLabelView;

    private RoundButton tv_exit_login;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_mine;
    }

    @Override
    protected void initView(View view, Bundle bundle) {
        tv_userid = find(R.id.tv_userid);
        nickname_tv = find(R.id.nickname_tv);
        image = find(R.id.image);
        find(R.id.ll_login).setOnClickListener(this);
        find(R.id.rl_head).setOnClickListener(this);
        find(R.id.tv_set).setOnClickListener(this);
        find(R.id.tv_lianxi).setOnClickListener(this);
        find(R.id.tv_shoucang).setOnClickListener(this);
        find(R.id.tv_pinglun).setOnClickListener(this);
        find(R.id.rl_my_info).setOnClickListener(this);
        find(R.id.rl_help).setOnClickListener(this);
        find(R.id.rl_yszc).setOnClickListener(this);
        find(R.id.tv_xiaoxi).setOnClickListener(this);
        tv_exit_login = find(R.id.tv_exit_login);
        tv_exit_login.setOnClickListener(this);

        stackLabelView = find(R.id.stackLabelView);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_set:
                startActivity(new Intent(mContext, MineSettingActivity.class));
                break;
            case R.id.rl_my_info:
                startActivity(new Intent(getActivity(), MyInfoActivity.class));
                break;
            case R.id.rl_help:
                startActivity(new Intent(getActivity(), FankuiActivity.class));
                break;
            case R.id.rl_yszc: {
                Intent intent1 = new Intent(mContext, Webview2Activity.class);
                intent1.putExtra("url", Config.WEB_URL + "/Introduction/index/privacy");
                startActivity(intent1);
            }
            break;
            case R.id.tv_lianxi: {
                startActivity(new Intent(getActivity(), AboutActivity.class));
            }
            break;
            case R.id.ll_login:
                if (AppPrefer.getInstance(mContext).getString(AppPrefer.mtoken, "").equals("")) {
                    Intent intent = new Intent(mContext, LoginActivity.class);
                    startActivityForResult(intent, 200);
                }
                break;
            case R.id.tv_shoucang:
                if (AppPrefer.getInstance(mContext).getString(AppPrefer.mtoken, "").equals("")) {
                    Intent intent = new Intent(mContext, LoginActivity.class);
                    startActivityForResult(intent, 200);
                }
                startActivity(new Intent(getActivity(), MineCollectActivity.class));
                break;
            case R.id.tv_pinglun:
                if (AppPrefer.getInstance(mContext).getString(AppPrefer.mtoken, "").equals("")) {
                    Intent intent = new Intent(mContext, LoginActivity.class);
                    startActivityForResult(intent, 200);
                }
                startActivity(new Intent(getActivity(), MineCommentActivity.class));
                break;
            case R.id.tv_xiaoxi:
                if (AppPrefer.getInstance(mContext).getString(AppPrefer.mtoken, "").equals("")) {
                    Intent intent = new Intent(mContext, LoginActivity.class);
                    startActivityForResult(intent, 200);
                }
                startActivity(new Intent(getActivity(), MineNoticeActivity.class));
                break;
            case R.id.tv_exit_login:
                MessageDialog.show((AppCompatActivity) mContext, null, getString(R.string.is_loginout), getString(R.string.confirm), getString(R.string.cancel)).setOnOkButtonClickListener(
                        new OnDialogButtonClickListener() {
                            @Override
                            public boolean onClick(BaseDialog baseDialog, View v) {
                                //清除个人信息
                                AppPrefer.getInstance(mContext).remove(AppPrefer.userinfo);
                                AppPrefer.getInstance(mContext).remove(AppPrefer.mtoken);
                                AppPrefer.getInstance(mContext).clear();

                                onResume();
                                return false;
                            }
                        });
                break;
            default:
                break;
        }
    }


    /**
     * 获取用户信息
     */
    public void showUserInfo() {
        if (AppPrefer.getInstance(mContext).getString(AppPrefer.mtoken, "").isEmpty()) {
            return;
        }
        Map<String, String> map = new HashMap<>();
        HttpRequest.request(getActivity(), "/client/user/userinfo", "", map, "POST", new JsonResult() {
            @Override
            public void reqestSuccess(JSONObject result) {
                // TODO Auto-generated method stub
                try {
                    AppPrefer.getInstance(mContext).put(AppPrefer.userinfo, result.toString());
                    setData();
                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
            }

            @Override
            public void requestFailure(int code, String msg) {
                super.requestFailure(code, msg);
                if (code == 104) {
                    AppPrefer.getInstance(mContext).clear();
                    startActivity(new Intent()
                            .putExtra("event", 1)
                            .setClass(mContext, LoginActivity.class));
                }
            }
        });
    }

    /**
     * 设置数据
     */
    private void setData() {
        UserModel userModel = AppPrefer.getUserModel(mContext);
        if (userModel == null) {
            return;
        }
        nickname_tv.setText(userModel.getData().getUsername());
        tv_userid.setText(userModel.getData().getNickname());
        Glide.with(mContext)
                .load(Config._API_URL + userModel.getData().getHeadicon()) //图片地址
                .apply(GlideRequestOptions.PATIENT_HEAD)
                .into(image);


    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!AppPrefer.getInstance(mContext).getString(AppPrefer.mtoken, "").isEmpty()) {
            find(R.id.ll_nickname).setVisibility(View.VISIBLE);
            tv_exit_login.setVisibility(View.VISIBLE);
        } else {
            find(R.id.ll_nickname).setVisibility(View.GONE);
            tv_exit_login.setVisibility(View.GONE);
            nickname_tv.setText("点击登录");
        }
        showUserInfo();
    }

}
