package com.samnative.pineapple.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kongzue.dialog.interfaces.OnDialogButtonClickListener;
import com.kongzue.dialog.util.BaseDialog;
import com.kongzue.dialog.v3.MessageDialog;
import com.samnative.pineapple.utils.PackageUtils;
import com.shashank.sony.fancytoastlib.FancyToast;

import java.io.File;
import java.math.BigDecimal;


public class MineSettingActivity extends BaseActivity implements View.OnClickListener {

    //认证状态
    private TextView tvVerity;

    private RelativeLayout rl_auth_name;

    /**
     * 缓存大小
     */
    private TextView tv_cache;
    /**
     * 当前版本
     */
    private TextView tv_version;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_mine_setting;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        find(R.id.rl_back).setOnClickListener(this);
        find(R.id.rl_acc_set).setOnClickListener(this);
        find(R.id.rl_cache).setOnClickListener(this);
        find(R.id.tv_exit_login).setOnClickListener(this);
        find(R.id.rl_fankui).setOnClickListener(this);
        find(R.id.rl_shengming).setOnClickListener(this);
        find(R.id.rl_lxwm).setOnClickListener(this);
        find(R.id.tv_zhuxiao).setOnClickListener(this);
        find(R.id.rl_xiugaimima).setOnClickListener(this);

        rl_auth_name = find(R.id.rl_auth_name);
        rl_auth_name.setOnClickListener(this);
        tvVerity = find(R.id.tv_verity);
        tv_cache = find(R.id.tv_cache);
        tv_version = find(R.id.tv_version);
        tv_cache.setText(getCacheSize());
        tv_version.setText(PackageUtils.getAppVersionName(mContext));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_back:
                finish();
                break;
            case R.id.rl_acc_set: {
                Intent intent = new Intent(mContext, MyInfoActivity.class);
                startActivity(intent);
            }
            break;
//            case R.id.rl_security_center: {
//                Intent intent = new Intent(mContext, SecurityCenterActivity.class);
//                startActivity(intent);
//            }
//            break;
//            case R.id.rl_auth_name: {
//                Intent intent = new Intent(mContext, AuthNameActivity.class)
//                        .putExtra("userModel", userModel);
//                startActivity(intent);
//            }
//            break;
            case R.id.rl_cache:
                MessageDialog.show(MineSettingActivity.this, null, getString(R.string.is_clearcache), getString(R.string.confirm), getString(R.string.cancel)).setOnOkButtonClickListener(
                        new OnDialogButtonClickListener() {
                            @Override
                            public boolean onClick(BaseDialog baseDialog, View v) {
                                deleteAllCache(mContext.getExternalCacheDir());
                                FancyToast.showToast(mContext, getString(R.string.cache_cleared), FancyToast.SUCCESS, false);
                                tv_cache.setText("0KB");
                                return false;
                            }
                        });
                break;

            case R.id.rl_shengming: {
//                UserModel userModel = AppPrefer.getUserModel(mContext);
//                if (userModel == null) {
//                    break;
//                }
//                Intent intent = new Intent(mContext, Webview2Activity.class);
//                intent.putExtra("url", userModel.getData().getTipsUrl());
//                startActivity(intent);

            }
            break;
            case R.id.rl_lxwm: {
//                UserModel userModel = AppPrefer.getUserModel(mContext);
//                if (userModel == null) {
//                    break;
//                }
//                Intent intent = new Intent(mContext, Webview2Activity.class);
//                intent.putExtra("url", userModel.getData().getContactusUrl());
//                startActivity(intent);
                Intent intent = new Intent(mContext, AboutActivity.class);
                startActivity(intent);
            }
            break;
            case R.id.rl_xiugaimima: {
                Intent intent = new Intent(mContext, ChangePasswordActivity.class);
                startActivity(intent);
            }
            break;
            default:
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * 获取缓存大小
     */
    public String getCacheSize() {
        File file = mContext.getExternalCacheDir();
        long size = getDicAllSize(file);
        return bytes2kb(size);
    }

    /**
     * 递归计算目录下文件总字节
     *
     * @param file
     * @return
     */
    public long getDicAllSize(File file) {
        long size = 0;
        File[] files = file.listFiles();
        for (int i = 0; i < files.length; i++) {
            if (files[i].isDirectory()) {
                getDicAllSize(files[i]);
            } else {
                size += files[i].length();
            }
        }
        return size;
    }

    /**
     * 递归删除所有缓存
     *
     * @param file
     * @return
     */
    public void deleteAllCache(File file) {
        File[] files = file.listFiles();
        for (int i = 0; i < files.length; i++) {
            if (files[i].isDirectory()) {
                getDicAllSize(files[i]);
            } else {
                files[i].delete();
            }
        }
    }

    /**
     * byte(字节)根据长度转成kb(千字节)和mb(兆字节)
     *
     * @param bytes
     * @return
     */
    public static String bytes2kb(long bytes) {
        BigDecimal filesize = new BigDecimal(bytes);
        BigDecimal megabyte = new BigDecimal(1024 * 1024);
        float returnValue = filesize.divide(megabyte, 2, BigDecimal.ROUND_UP)
                .floatValue();
        if (returnValue > 1)
            return (returnValue + "MB");
        BigDecimal kilobyte = new BigDecimal(1024);
        returnValue = filesize.divide(kilobyte, 2, BigDecimal.ROUND_UP)
                .floatValue();
        return (returnValue + "KB");
    }


}
