package com.samnative.pineapple.main;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

import com.samnative.pineapple.http.HttpRequest;
import com.samnative.pineapple.http.JsonResult;
import com.samnative.pineapple.instance.AppPrefer;
import com.samnative.pineapple.widget.ChooseBankAccoutTypePop;
import com.shashank.sony.fancytoastlib.FancyToast;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * 添加银行卡
 */
public class AddBankcardActivity extends BaseActivity implements OnClickListener, TextWatcher {

    /**
     * 输入信息
     */
    private EditText et_card_name, et_bank_num, et_bank_detailname;
    /**
     * 账户信息
     */
    private TextView tv_type_name;
    /**
     * 银行名称
     */
    private EditText tv_bank_name;
    /**
     * 账户类型
     */
    private String account_type = "1";

    @Override
    protected int getLayoutId() {
        return R.layout.activity_add_bankcard;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        find(R.id.tv_back).setOnClickListener(this);
        find(R.id.tv_enter).setOnClickListener(this);
        et_card_name = (EditText) find(R.id.et_card_name);
        et_bank_num = (EditText) find(R.id.et_bank_num);
        et_bank_num.addTextChangedListener(this);
        et_bank_detailname = (EditText) find(R.id.et_bank_detailname);
        tv_type_name = (TextView) find(R.id.tv_type_name);
        tv_type_name.setOnClickListener(this);
        tv_bank_name = (EditText) find(R.id.tv_bank_name);

//        String name = AppPrefer.getUserModel(mContext).getData().getUserinfo().getRealname();
//        if (name != null) {
//            et_card_name.setText(name);
//        }
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.tv_back:
                finish();
                break;
            case R.id.tv_type_name:
//                ChooseBankAccoutTypePop cbatp = new ChooseBankAccoutTypePop(
//                        mContext, 1, new ChooseBankAccoutTypePop.ChooseSelect() {
//                    @Override
//                    public void current(int index) {
//                        // TODO Auto-generated method stub
//                        tv_type_name.setText(index == 1 ? "个人" : "公司账户");
//                        et_card_name.setHint(index == 1 ? "请输入姓名"
//                                : "请输入公司名称");
//                        account_type = index + "";
//
//                        if (index == 1) {
//                            String name = AppPrefer.getUserModel(mContext).getData().getUserinfo().getRealname();
//                            if (name != null) {
//                                et_card_name.setText(name);
//                            }
//                        } else {
//                            et_card_name.setText("");
//                            tv_bank_name.setText("");
//                            et_bank_num.setText("");
//                        }
//
//                    }
//                });
//                cbatp.setIsAccoutName(1);
//                cbatp.showAtLocation(v, Gravity.CENTER, 0, 0);
                break;
//            case tv_bank_name:
//                ChooseBankPop cbp = new ChooseBankPop(context,
//                        new ChooseBankPop.ChooseSelect() {
//                            @Override
//                            public void current(String name) {
//                                // TODO Auto-generated method stub
//                                tv_bank_name.setText(name);
//                            }
//                        });
//                cbp.showAtLocation(v, Gravity.CENTER, 0, 0);
//                break;
            case R.id.tv_enter:

                if (et_card_name.getText().toString().isEmpty()) {
                    FancyToast.showToast(mContext, "请输入开户名称", FancyToast.WARNING, false);
                    break;
                }
                if (et_bank_num.getText().toString().isEmpty()) {
                    FancyToast.showToast(mContext, "请输入银行卡号", FancyToast.WARNING, false);
                    break;
                }
                if (et_bank_detailname.getText().toString().isEmpty()) {
                    FancyToast.showToast(mContext, "请输入开户支行名称", FancyToast.WARNING, false);
                    break;
                }

                requestData();

                break;
            default:
                break;
        }
    }

    /**
     * 添加银行卡
     */
    public void requestData() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("_apiname", "user.user.addbank");
        map.put("mtoken", AppPrefer.getInstance(mContext).getString(AppPrefer.mtoken, ""));
        map.put("account_type", account_type);
        map.put("account_name", et_card_name.getText().toString());
        map.put("account_number", et_bank_num.getText().toString());
        map.put("bank_type_name", tv_bank_name.getText().toString());
        map.put("branch", et_bank_detailname.getText().toString());
        HttpRequest.post((Activity) mContext, "", map, new JsonResult() {
            @Override
            public void reqestSuccess(JSONObject result) {
                FancyToast.showToast(mContext, "添加成功", FancyToast.SUCCESS, true);
                finish();
            }

        });
    }


    /**
     * 根据银行卡号识别银行名称(至少6位)
     *
     * @param carNo
     */
    public void requestBankInfo(String carNo) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("_apiname", "user.user.checkBankNumber");
        map.put("mtoken", AppPrefer.getInstance(mContext).getString(AppPrefer.mtoken, ""));
        map.put("account_number", carNo);
        HttpRequest.post((Activity) mContext, "", map, new JsonResult() {
            @Override
            public void reqestSuccess(JSONObject result) {
                tv_bank_name.setText(result.optString("data"));
            }

        });
    }

    //=============================================================================监听卡号输入START

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

        if (s.toString().length() == 6 && account_type.equals("1")) {
            requestBankInfo(s.toString());
        }

    }

    //=============================================================================监听卡号输入END

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
