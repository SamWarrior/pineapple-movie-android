package com.samnative.pineapple.entity;

import java.util.List;

/**
 * 商品model
 *
 * @author WangPeng
 */
public class ProductDetailModel {

    private String productid;
    private String productname;
    private String prouctprice;
    private String supplyprice;
    private String bullamount;
    private String enable;
    private String transport;
    private String description;
    private String evaluatecount;
    private String salecount;
    private String scores;
    private String area;
    private String marketprice;
    private String detailInfo;
    private String detailParams;
    private String give_profitamount;
    private String give_bullamount;
    private String iscollect;
    private String nbtctitle;
    private String brokprice;
    private String brokpricestr;
    private int isservice;
    private String tcostr;
    private String shopinstructionurl;

    private ProductCommentModel commnentData;
    private Business business;
    private ProductQianggouModel qianggou;

    private List<ProductDetailImgModel> bannerimg;
    private List<ProductSpecModel> spec;
    private List<ProductSkuModel> sku;

    public String getShopinstructionurl() {
        return shopinstructionurl;
    }

    public void setShopinstructionurl(String shopinstructionurl) {
        this.shopinstructionurl = shopinstructionurl;
    }

    public String getTcostr() {
        return tcostr;
    }

    public void setTcostr(String tcostr) {
        this.tcostr = tcostr;
    }

    public String getBrokpricestr() {
        return brokpricestr;
    }

    public void setBrokpricestr(String brokpricestr) {
        this.brokpricestr = brokpricestr;
    }

    public String getBrokprice() {
        return brokprice;
    }

    public void setBrokprice(String brokprice) {
        this.brokprice = brokprice;
    }

    public String getNbtctitle() {
        return nbtctitle;
    }

    public void setNbtctitle(String nbtctitle) {
        this.nbtctitle = nbtctitle;
    }

    public int getIsservice() {
        return isservice;
    }

    public void setIsservice(int isservice) {
        this.isservice = isservice;
    }

    public String getIscollect() {
        return iscollect;
    }

    public void setIscollect(String iscollect) {
        this.iscollect = iscollect;
    }

    public String getGive_profitamount() {
        return give_profitamount;
    }

    public void setGive_profitamount(String give_profitamount) {
        this.give_profitamount = give_profitamount;
    }

    public String getGive_bullamount() {
        return give_bullamount;
    }

    public void setGive_bullamount(String give_bullamount) {
        this.give_bullamount = give_bullamount;
    }

    public String getMarketprice() {
        return marketprice;
    }

    public void setMarketprice(String marketprice) {
        this.marketprice = marketprice;
    }

    public String getDetailInfo() {
        return detailInfo;
    }

    public void setDetailInfo(String detailInfo) {
        this.detailInfo = detailInfo;
    }

    public String getDetailParams() {
        return detailParams;
    }

    public void setDetailParams(String detailParams) {
        this.detailParams = detailParams;
    }

    public String getEnable() {
        return enable;
    }

    public void setEnable(String enable) {
        this.enable = enable;
    }

    public String getSupplyprice() {
        return supplyprice;
    }

    public void setSupplyprice(String supplyprice) {
        this.supplyprice = supplyprice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEvaluatecount() {
        return evaluatecount;
    }

    public void setEvaluatecount(String evaluatecount) {
        this.evaluatecount = evaluatecount;
    }

    public String getSalecount() {
        return salecount;
    }

    public void setSalecount(String salecount) {
        this.salecount = salecount;
    }

    public String getScores() {
        return scores;
    }

    public void setScores(String scores) {
        this.scores = scores;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getTransport() {
        return transport;
    }

    public void setTransport(String transport) {
        this.transport = transport;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getProuctprice() {
        return prouctprice;
    }

    public void setProuctprice(String prouctprice) {
        this.prouctprice = prouctprice;
    }

    public String getBullamount() {
        return bullamount;
    }

    public void setBullamount(String bullamount) {
        this.bullamount = bullamount;
    }

    public List<ProductDetailImgModel> getBannerimg() {
        return bannerimg;
    }

    public void setBannerimg(List<ProductDetailImgModel> bannerimg) {
        this.bannerimg = bannerimg;
    }

    public List<ProductSpecModel> getSpec() {
        return spec;
    }

    public void setSpec(List<ProductSpecModel> spec) {
        this.spec = spec;
    }

    public List<ProductSkuModel> getSku() {
        return sku;
    }

    public void setSku(List<ProductSkuModel> sku) {
        this.sku = sku;
    }

    public ProductCommentModel getCommnentData() {
        return commnentData;
    }

    public void setCommnentData(ProductCommentModel commnentData) {
        this.commnentData = commnentData;
    }

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    public ProductQianggouModel getQianggou() {
        return qianggou;
    }

    public void setQianggou(ProductQianggouModel qianggou) {
        this.qianggou = qianggou;
    }
}
