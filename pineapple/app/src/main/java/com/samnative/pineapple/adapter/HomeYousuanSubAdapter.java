package com.samnative.pineapple.adapter;

import android.support.annotation.LayoutRes;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.samnative.pineapple.entity.HomeYouxuanModel;
import com.samnative.pineapple.main.R;
import com.samnative.pineapple.utils.GlideRequestOptions;

/**
 * 首页-优选自列表
 */
public class HomeYousuanSubAdapter extends BaseQuickAdapter<HomeYouxuanModel.ListBean, BaseViewHolder> {

    public HomeYousuanSubAdapter(@LayoutRes int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, final HomeYouxuanModel.ListBean item) {

        ImageView iv_item_salewas_pic = helper.getView(R.id.iv_item_salewas_pic);
        TextView tv_item_salewas_name = helper.getView(R.id.tv_item_salewas_name);
        tv_item_salewas_name.setText(item.getTitle());


        Glide.with(mContext)
                .load(item.getThumb()) //图片地址
                .apply(GlideRequestOptions.OPTIONS_400_400)
                .into(iv_item_salewas_pic);
    }

}
