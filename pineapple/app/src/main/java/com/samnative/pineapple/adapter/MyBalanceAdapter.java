package com.samnative.pineapple.adapter;

import android.support.v4.content.ContextCompat;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.samnative.pineapple.entity.BalanceRecordModel;
import com.samnative.pineapple.main.R;

public class MyBalanceAdapter extends BaseQuickAdapter<BalanceRecordModel, BaseViewHolder> {

    public MyBalanceAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, BalanceRecordModel item) {
        TextView tv_name = helper.getView(R.id.tv_name);
        TextView tv_date = helper.getView(R.id.tv_date);
        TextView tv_amount = helper.getView(R.id.tv_amount);

        tv_name.setText(item.getFlowname());
        tv_date.setText(item.getFlowtime());

        String amount = item.getAmount();
        if (item.getDirection().equals("1")) {//+
            tv_amount.setTextColor(ContextCompat.getColor(mContext, R.color.main_color));
            if (amount.startsWith("+")) {
                tv_amount.setText(amount);
            } else {
                tv_amount.setText("+" + amount);
            }
        } else {//-
            tv_amount.setTextColor(ContextCompat.getColor(mContext, R.color.main_tab_text_n));
            if (amount.startsWith("-")) {
                tv_amount.setText(amount);
            } else {
                tv_amount.setText("-" + amount);
            }
        }

    }
}