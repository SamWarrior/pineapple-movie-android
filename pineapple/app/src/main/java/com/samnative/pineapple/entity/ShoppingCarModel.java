package com.samnative.pineapple.entity;

import java.util.List;

/**
 * 购物车Model
 */
public class ShoppingCarModel {

    private String id;
    private String customerid;
    private String businessid;
    private String addtime;
    private String businessname;
    private boolean isChecked;
    private boolean isEdit;
    private List<GoodsData> goodsData;

    public boolean isEdit() {
        return isEdit;
    }

    public void setEdit(boolean isEdit) {
        this.isEdit = isEdit;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomerid() {
        return customerid;
    }

    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }

    public String getBusinessid() {
        return businessid;
    }

    public void setBusinessid(String businessid) {
        this.businessid = businessid;
    }

    public String getAddtime() {
        return addtime;
    }

    public void setAddtime(String addtime) {
        this.addtime = addtime;
    }

    public String getBusinessname() {
        return businessname;
    }

    public void setBusinessname(String businessname) {
        this.businessname = businessname;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }

    public List<GoodsData> getGoodsData() {
        return goodsData;
    }

    public void setGoodsData(List<GoodsData> goodsData) {
        this.goodsData = goodsData;
    }

    public class GoodsData {

        private String cartid;
        private String customerid;
        private String businessid;
        private String productid;
        private String productnum;
        private String skuid;
        private String sku_code;
        private String addtime;
        private String productname;
        private String enable;
        private double prouctprice;
        private double bullamount;
        private String thumb;
        private String productimage;
        private int productstorage;
        private String spec;
        private boolean isChecked;

        public int getProductstorage() {
            return productstorage;
        }

        public void setProductstorage(int productstorage) {
            this.productstorage = productstorage;
        }

        public String getEnable() {
            return enable;
        }

        public void setEnable(String enable) {
            this.enable = enable;
        }

        public String getSpec() {
            return spec;
        }

        public void setSpec(String spec) {
            this.spec = spec;
        }

        public String getCustomerid() {
            return customerid;
        }

        public void setCustomerid(String customerid) {
            this.customerid = customerid;
        }

        public String getBusinessid() {
            return businessid;
        }

        public void setBusinessid(String businessid) {
            this.businessid = businessid;
        }

        public String getProductid() {
            return productid;
        }

        public void setProductid(String productid) {
            this.productid = productid;
        }

        public String getProductnum() {
            return productnum;
        }

        public void setProductnum(String productnum) {
            this.productnum = productnum;
        }

        public String getSkuid() {
            return skuid;
        }

        public void setSkuid(String skuid) {
            this.skuid = skuid;
        }

        public String getSku_code() {
            return sku_code;
        }

        public void setSku_code(String sku_code) {
            this.sku_code = sku_code;
        }

        public String getAddtime() {
            return addtime;
        }

        public void setAddtime(String addtime) {
            this.addtime = addtime;
        }

        public String getProductname() {
            return productname;
        }

        public void setProductname(String productname) {
            this.productname = productname;
        }

        public double getProuctprice() {
            return prouctprice;
        }

        public void setProuctprice(double prouctprice) {
            this.prouctprice = prouctprice;
        }

        public double getBullamount() {
            return bullamount;
        }

        public void setBullamount(double bullamount) {
            this.bullamount = bullamount;
        }

        public String getCartid() {
            return cartid;
        }

        public void setCartid(String cartid) {
            this.cartid = cartid;
        }

        public String getThumb() {
            return thumb;
        }

        public void setThumb(String thumb) {
            this.thumb = thumb;
        }

        public String getProductimage() {
            return productimage;
        }

        public void setProductimage(String productimage) {
            this.productimage = productimage;
        }

        public boolean isChecked() {
            return isChecked;
        }

        public void setChecked(boolean isChecked) {
            this.isChecked = isChecked;
        }

    }

}
