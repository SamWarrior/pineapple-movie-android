package com.samnative.pineapple.adapter;

import android.support.annotation.LayoutRes;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.samnative.pineapple.entity.WentiListModel;
import com.samnative.pineapple.main.R;


/**
 * 问题列表
 */
public class WentiListAdapter extends BaseQuickAdapter<WentiListModel, BaseViewHolder> {

    public WentiListAdapter(@LayoutRes int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, WentiListModel item) {

        TextView tv_title = helper.getView(R.id.tv_title);
        tv_title.setText(item.getTitle());
    }
}