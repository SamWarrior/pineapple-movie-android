package com.samnative.pineapple.main;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.shashank.sony.fancytoastlib.FancyToast;
import com.samnative.pineapple.http.HttpRequest;
import com.samnative.pineapple.http.JsonResult;
import com.samnative.pineapple.instance.AppPrefer;
import com.samnative.pineapple.utils.MD5Util;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * 设置修改支付密码
 */

public class PasswordPaySetActivity extends BaseActivity implements View.OnClickListener {

    /**
     * 密码
     */
    private EditText etPw;
    /**
     * 再次输入密码
     */
    private EditText etPwAgain;


    private String mobile;
//    private String encrypt;

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_back:
                finish();
                break;
            case R.id.tv_sure:
                if (etPw.getText().toString().isEmpty() || etPw.getText().toString().length() < 6) {
                    FancyToast.showToast(mContext, "请输入6位数字支付密码", FancyToast.WARNING, false);
                    return;
                }
                if (etPwAgain.getText().toString().isEmpty() || etPwAgain.getText().toString().length() < 6) {
                    FancyToast.showToast(mContext, "请输入6位数字支付密码", FancyToast.WARNING, false);
                    return;
                }
                if (!(etPw.getText().toString().trim()).equals(etPwAgain.getText().toString().trim())) {
                    FancyToast.showToast(mContext, getString(R.string.match_fail), FancyToast.WARNING, false);
                    return;
                }
                doBind();
                break;
            default:
        }
    }

    /**
     * 修改支付密码
     */
    private void doBind() {

        String ispaypwd = AppPrefer.getUserModel(mContext).getData().getPayDec();
        String _apiname = "";
        if (ispaypwd.equals("0")) {
            _apiname = "user.user.setPay";
        } else {
            _apiname = "user.user.updatePayPwd";
        }


        Map<String, String> map = new HashMap<>();
        map.put("mobile", mobile);
//        map.put("encrypt", encrypt);
        map.put("mtoken", AppPrefer.getInstance(mContext).getString(AppPrefer.mtoken, ""));
        map.put("paypwd", MD5Util.getMD5Str(etPwAgain.getText().toString()));
        map.put("_apiname", _apiname);
        HttpRequest.post(this, "", map, new JsonResult() {
            @Override
            public void reqestSuccess(JSONObject result) {
                // TODO Auto-generated method stub
                FancyToast.showToast(mContext, getString(R.string.success_set), FancyToast.SUCCESS, false);

                if (PasswordLoginVerityActivity.instance != null) {
                    PasswordLoginVerityActivity.instance.finish();
                }

                finish();
            }
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_paypwd_set;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        find(R.id.rl_back).setOnClickListener(this);
        find(R.id.tv_sure).setOnClickListener(this);

        etPw = find(R.id.et_pw);
        etPwAgain = find(R.id.et_pw_again);

//        encrypt = getIntent().getStringExtra("encrypt");
        mobile = getIntent().getStringExtra("mobile");
    }
}
