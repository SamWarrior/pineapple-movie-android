package com.samnative.pineapple.adapter;

import android.support.annotation.LayoutRes;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.samnative.pineapple.entity.HomeListModel;
import com.samnative.pineapple.main.R;
import com.samnative.pineapple.utils.GlideRequestOptions;
import com.samnative.pineapple.value.Config;

/**
 * 电影详情——演职员
 */
public class MoviedetalYanyuanAdapter extends BaseQuickAdapter<HomeListModel.PlotsWordsBean, BaseViewHolder> {

    public MoviedetalYanyuanAdapter(@LayoutRes int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, final HomeListModel.PlotsWordsBean item) {

        ImageView iv_image = helper.getView(R.id.iv_image);
        TextView tv_title = helper.getView(R.id.tv_title);
        tv_title.setText(item.getActorRole() + "：" + item.getAutor());
        Glide.with(mContext)
                .load(Config._API_URL + item.getImgUrl())//图片地址
                .apply(GlideRequestOptions.OPTIONS_400_400)
                .into(iv_image);

    }

}
