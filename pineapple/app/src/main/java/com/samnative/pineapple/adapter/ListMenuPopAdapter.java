package com.samnative.pineapple.adapter;

import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.samnative.pineapple.main.R;


/**
 * 通用的下拉菜单适配器
 */
public class ListMenuPopAdapter extends BaseQuickAdapter<String, BaseViewHolder> {

    private int currentChecked = -1;

    public void setCurrentChecked(int currentChecked) {
        this.currentChecked = currentChecked;
        notifyDataSetChanged();
    }

    public int getCurrentChecked() {
        return currentChecked;
    }

    public ListMenuPopAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {

        TextView tv_name = helper.getView(R.id.tv_name);
        ImageView iv_img = helper.getView(R.id.iv_img);

        tv_name.setText(item);

        if (currentChecked == helper.getAdapterPosition()) {
            iv_img.setVisibility(View.VISIBLE);
            tv_name.setTextColor(ContextCompat.getColor(mContext, R.color.main_color));
        } else {
            iv_img.setVisibility(View.GONE);
            tv_name.setTextColor(ContextCompat.getColor(mContext, R.color.main_tab_text_n));
        }


    }
}