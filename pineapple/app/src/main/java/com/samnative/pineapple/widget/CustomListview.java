package com.samnative.pineapple.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

/**
 * 自定义ListView 适用于ScrollView
 * 
 * @author WangPeng
 * 
 */
public class CustomListview extends ListView {

	public CustomListview(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CustomListview(Context context) {
		super(context);
	}

	public CustomListview(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
				MeasureSpec.AT_MOST);
		super.onMeasure(widthMeasureSpec, expandSpec);
	}

}
