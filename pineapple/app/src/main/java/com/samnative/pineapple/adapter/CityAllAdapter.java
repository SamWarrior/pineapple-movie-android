package com.samnative.pineapple.adapter;

import android.support.annotation.LayoutRes;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.samnative.pineapple.entity.City;
import com.samnative.pineapple.main.R;


/**
 * 城市选择列表
 */
public class CityAllAdapter extends BaseQuickAdapter<City, BaseViewHolder> {


    /**
     * 文字内容权重
     */
    private int gravity;

    public void setGravity(int gravity) {
        this.gravity = gravity;
    }

    public CityAllAdapter(@LayoutRes int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, final City item) {

        TextView tv_city = helper.getView(R.id.tv_city);
        TextView tv_ucfirst = helper.getView(R.id.tv_ucfirst);
        tv_city.setText(item.getName());
        tv_ucfirst.setText(item.getUcfirst());


        if (gravity == 0) {
            //城市列表,有字母导航,城市名字显示在左边
            tv_city.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
            int position = helper.getLayoutPosition() - 1;

            if (position == 0) {
                tv_ucfirst.setVisibility(View.VISIBLE);
            } else {

                String oldUc = getData().get(position - 1).getUcfirst();
                if (oldUc.equals(item.getUcfirst())) {
                    tv_ucfirst.setVisibility(View.GONE);
                } else {
                    tv_ucfirst.setVisibility(View.VISIBLE);
                }


            }


        } else {
            //热门城市和搜索出来的城市,无字母,名字显示在中间
            tv_city.setGravity(gravity);
            tv_ucfirst.setVisibility(View.GONE);
        }


    }
}