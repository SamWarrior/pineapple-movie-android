package com.samnative.pineapple.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.samnative.pineapple.entity.ExpressModel;
import com.samnative.pineapple.interfaces.OnChooseExpressListener;
import com.samnative.pineapple.main.R;
import com.samnative.pineapple.utils.ViewHelper;

import java.util.ArrayList;
import java.util.List;

public class ChooseExpressPop extends PopupWindow {

	private Context context;
	private OnChooseExpressListener onExpressListener;
	private View contentView;
	private ListView mListview;
	private List<ExpressModel> mList;
	private Button mbtn;
	private LayoutInflater inflater;
	private MyBaseAdapter mBAdapter;
	/** 阴影层 */
	private View viewShade;

	public ChooseExpressPop(Context context, List<ExpressModel> mList,
                            OnChooseExpressListener onExpressListener) {
		this.context = context;
		this.onExpressListener = onExpressListener;
		this.mList = mList;
		init();

	}

	private void init() {
		// TODO Auto-generated method stub

		setWidth(LayoutParams.MATCH_PARENT);
		setHeight(LayoutParams.MATCH_PARENT);
		setFocusable(true);
		setAnimationStyle(R.style.AnimBottom);
		setBackgroundDrawable(new ColorDrawable(0x6f000000));
		setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

		inflater = LayoutInflater.from(context);
		contentView = inflater.inflate(R.layout.pop_choose_express, null);

		int screenHeigth = ViewHelper.getWindowRealize((Activity) context).x;

		LinearLayout pop_layout = (LinearLayout) contentView
				.findViewById(R.id.pop_layout);

		pop_layout.getLayoutParams().height = (int) (screenHeigth * 0.75);
		mListview = (ListView) contentView
				.findViewById(R.id.choose_bank_listview);
		mbtn = (Button) contentView.findViewById(R.id.pop_close);
		setContentView(contentView);
		mListview = (ListView) contentView
				.findViewById(R.id.choose_bank_listview);
		viewShade = contentView.findViewById(R.id.v_pop_shade);
		dataInit();

	}

	private void dataInit() {
		// TODO Auto-generated method stub

		if (mList == null) {
			mList = new ArrayList<ExpressModel>();
		}

		mBAdapter = new MyBaseAdapter();
		mListview.setAdapter(mBAdapter);
		mListview.setOnItemClickListener(new OnItemClick());
		mbtn.setOnClickListener(new OnClick());

	}

	@Override
	public void showAtLocation(View parent, int gravity, int x, int y) {
		// TODO Auto-generated method stub
		viewShade.setVisibility(View.GONE);
		super.showAtLocation(parent, gravity, x, y);
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
				anim.setDuration(200);
				viewShade.startAnimation(anim);
				viewShade.setVisibility(View.VISIBLE);
			}
		}, 300);

	}

	@Override
	public void dismiss() {
		// TODO Auto-generated method stub
		AlphaAnimation anim = new AlphaAnimation(1.0f, 0.0f);
		anim.setDuration(200);
		anim.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				viewShade.setVisibility(View.GONE);
				ChooseExpressPop.super.dismiss();
			}
		});
		viewShade.startAnimation(anim);
	}

	public class OnClick implements OnClickListener {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub

			ChooseExpressPop.this.dismiss();

		}

	}

	public class OnItemClick implements OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// TODO Auto-generated method stub

			onExpressListener.onChooseExpress(mList.get(position));

			ChooseExpressPop.this.dismiss();

		}
	}

	public List<ExpressModel> getmList() {
		return mList;
	}

	public void setmList(List<ExpressModel> mList) {
		this.mList = mList;
		mBAdapter.notifyDataSetChanged();
	}

	public class MyBaseAdapter extends BaseAdapter {

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			ViewHolder holder;
			ExpressModel model = mList.get(position);
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.item_choose_bank,
						parent, false);
				holder = new ViewHolder();
				holder.InitView(convertView);
				convertView.setTag(holder);

			} else {

				holder = (ViewHolder) convertView.getTag();
			}

			holder.item_choose_bank_img.setVisibility(View.GONE);
			holder.item_choose_bank_name.setText(model.getF_companyname());

			return convertView;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return mList.get(position);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mList.size();
		}
	};

	public class ViewHolder {

		private ImageView item_choose_bank_img;
		private TextView item_choose_bank_name;

		public void InitView(View contentView) {
			item_choose_bank_img = (ImageView) contentView
					.findViewById(R.id.item_choose_bank_img);
			item_choose_bank_name = (TextView) contentView
					.findViewById(R.id.item_choose_bank_name);
		}

	}

}
