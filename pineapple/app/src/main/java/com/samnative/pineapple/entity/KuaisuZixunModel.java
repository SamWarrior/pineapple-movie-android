package com.samnative.pineapple.entity;

/**
 * 快速咨询
 */
public class KuaisuZixunModel {

    private String title;
    private String url;

    public KuaisuZixunModel() {
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
