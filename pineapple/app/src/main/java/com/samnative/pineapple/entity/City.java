package com.samnative.pineapple.entity;

import java.util.ArrayList;

/**
 * 城市
 * 
 * @author Administrator
 */

public class City {

	private int id;
	private String name;
	private String ucfirst;
	private ArrayList<County> area;

	public City(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public City() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<County> getArea() {
		return area;
	}

	public void setArea(ArrayList<County> area) {
		this.area = area;
	}

	public String getUcfirst() {
		return ucfirst;
	}

	public void setUcfirst(String ucfirst) {
		this.ucfirst = ucfirst;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return name;
	}
}
