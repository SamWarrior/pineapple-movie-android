package com.samnative.pineapple.http;

import org.json.JSONObject;

/**
 * Created by Administrator on 2019/5/13.
 * <p>
 * HTTP请求接口返回的回调状态
 */

public abstract class JsonResult {


    /**
     * 请求失败(包含连接失败,请求错误等,code为0表示连接失败)
     */
    public void requestFailure(int code, String msg) {
    }


    /**
     * 请求结束,不论成功还是失败
     */
    public void requestFinish() {
    }


    /**
     * 请求成功 返回json
     */
    public abstract void reqestSuccess(JSONObject result);

}
