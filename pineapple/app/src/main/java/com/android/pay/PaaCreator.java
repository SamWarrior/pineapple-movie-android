package com.android.pay;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


/**
 * {"code":"200","msg":"success","data":{"param":{
 * "inputCharset":"1",
 * "pickupUrl":"",
 * "receiveUrl":"http:\/\/www.niuniuhuiapp.net:82\/Payment\/Noity\/allinpayquickAppNotifyUrl\/",
 * "version":"v1.0",
 * "language":"1",
 * "signType":"0",
 * "merchantId":"009440353114121",
 * "orderNo":"NNHSTO20170707100418973236",
 * "orderAmount":"12345600",
 * "orderCurrency":"0",
 * "orderDatetime":"20170707100418",
 * "productName":"交易订单支付",
 * "ext1":"<USER>170706935841140<\/USER>",
 * "payType":"33",
 * "signMsg":"840250926864AA03C478293CD8CDF954"}}}
 */
public class PaaCreator {


    public static JSONObject getPayStr(JSONObject paaParams) {

        String[] paaParamsArray = {
                "", "inputCharset",
                "", "pickupUrl",
                "", "receiveUrl",
                "", "version",
                "", "language",
                "", "signType",
                "", "merchantId",
                "", "orderNo",
                "", "orderAmount",
                "", "orderCurrency",
                "", "orderDatetime",
                "", "productName",
                "", "ext1",
                "", "payType",
        };

        String paaStr = "";
        for (int i = 0; i < paaParamsArray.length; i++) {
            paaStr += paaParamsArray[i + 1] + "=" + paaParamsArray[i] + "&";
            i++;
        }
        Log.d("PaaCreator", "PaaCreator " + paaStr.substring(0, paaStr.length() - 1));
        String md5Str = md5(paaStr.substring(0, paaStr.length() - 1));
        Log.d("PaaCreator", "PaaCreator md5Str " + md5Str);
        try {
            paaParams.put("signMsg", md5Str);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return paaParams;
    }

    public static String md5(String string) {
        byte[] hash;
        try {
            hash = MessageDigest.getInstance("MD5").digest(
                    string.getBytes("UTF-8"));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Huh, MD5 should be supported?", e);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Huh, UTF-8 should be supported?", e);
        }

        return hexString(hash);
    }

    public static final String hexString(byte[] bytes) {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            buffer.append(hexString(bytes[i]));
        }
        return buffer.toString();
    }

    public static final String hexString(byte byte0) {
        char ac[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        char ac1[] = new char[2];
        ac1[0] = ac[byte0 >>> 4 & 0xf];
        ac1[1] = ac[byte0 & 0xf];
        String s = new String(ac1);
        return s;
    }

//	public static String ext1FromInput() {
//	    String[] paaParamsArray = {
//				"US","ship_to_country",
//				"AL","ship_to_state",
//				"city","ship_to_city",
//				"street_1","ship_to_street1",
//				"street_2","ship_to_street2",
//				"13812345678","ship_to_phonenumber",
//				"20004","ship_to_postalcode",
//				"Smith","ship_to_firstname",
//				"Black","ship_to_lastname",
//				"abc", "registration_name",
//				"abc@gmail.com","registration_email",
//				"999-13800000000","registration_phone",
//				"200","buyerid_period",
//				"1","fnpay_mode",
//				"handon","bill_firstname",
//				"hao","bill_lastname",
//				"1919","expireddate",
//				"888","cvv2",
//				"abc@gmail.com","bill_email",
//				"US","bill_country",
//				"billaddress","bill_address",
//				"billcity", "bill_city",
//				"IL","bill_state",
//				"12345","bill_zip",
//		    };
//		    
//		    String paaStr = "";
//		    for (int i = 0; i < paaParamsArray.length; i++) {
//		    	paaStr += paaParamsArray[i];
//		        i++;
//		    }
//		    Log.d("ext1FromInput", "ext1FromInput " + paaStr);
//		    String md5Str = md5(paaStr);
//		    Log.d("ext1FromInput", "ext1FromInput md5Str " + md5Str);
//		
//		return md5Str;
//	}
}
