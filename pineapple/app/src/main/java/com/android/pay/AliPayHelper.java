package com.android.pay;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.widget.Toast;

import com.alipay.sdk.app.PayTask;

import java.util.Map;

/**
 * 支付宝支付帮助类
 */
public class AliPayHelper {

    /**
     * 上下文
     */
    private Context m_Context;
    // /** 支付宝支付需要的参数集 */
    // private AliPayParams m_AliPayParams;
    /**
     * 支付回调
     */
    private IPayCallBack m_IPayCallBack;
    /**
     * 后台传来的完整支付参数
     */
    private String payInfo;

    // private static final String ALGORITHM = "RSA";
    // private static final String SIGN_ALGORITHMS = "SHA1WithRSA";
    // private static final String DEFAULT_CHARSET = "UTF-8";
    private static final int SDK_PAY_FLAG = 1;
    private static final int SDK_CHECK_FLAG = 2;

    public AliPayHelper(Context p_Context, String payInfo,
                        IPayCallBack p_IPayCallBack) {
        m_Context = p_Context;
        this.payInfo = payInfo;
        m_IPayCallBack = p_IPayCallBack;
    }

    // /**
    // * 支付宝支付帮助类构造函数
    // *
    // * @param p_Context
    // * 上下文对象
    // * @param p_WXPayParams
    // * 支付回调参数
    // *
    // * @param p_IPayCallBack
    // * 支付回调参数
    // * */
    // public AliPayHelper(Context p_Context, AliPayParams p_AliPayParams,
    // IPayCallBack p_IPayCallBack) {
    // m_Context = p_Context;
    // m_AliPayParams = p_AliPayParams;
    // m_IPayCallBack = p_IPayCallBack;
    // }

    /**
     * 支付结果回调处理
     */
    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SDK_PAY_FLAG: {
                    int m_result = 0;
                    PayResult payResult = new PayResult(
                            (Map<String, String>) msg.obj);
                    // 支付宝返回此次支付结果及加签，建议对支付宝签名信息拿签约时支付宝提供的公钥做验签
                    String resultInfo = payResult.getResult();
                    String resultStatus = payResult.getResultStatus();
                    // 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
                    if (TextUtils.equals(resultStatus, "9000")) {
                        Toast.makeText(m_Context, "支付成功", Toast.LENGTH_SHORT)
                                .show();
                    } else {
                        // 判断resultStatus 为非“9000”则代表可能支付失败
                        // “8000”代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
                        if (TextUtils.equals(resultStatus, "8000")) {
                            Toast.makeText(m_Context, "支付结果确认中", Toast.LENGTH_SHORT)
                                    .show();
                            m_result = -1;
                        } else {
                            if (TextUtils.equals(resultStatus, "6001")) {
                                // 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
                                Toast.makeText(m_Context, "您取消了支付",
                                        Toast.LENGTH_SHORT).show();
                                m_result = -2;
                            } else {
                                // 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
                                Toast.makeText(m_Context, "支付失败",
                                        Toast.LENGTH_SHORT).show();
                                m_result = -1;
                            }
                        }
                    }
                    if (m_IPayCallBack != null) {
                        m_IPayCallBack.payCallBack(m_result);
                    }
                    break;
                }
                case SDK_CHECK_FLAG: {
                    Toast.makeText(m_Context, "检查结果为：" + msg.obj,
                            Toast.LENGTH_SHORT).show();
                    break;
                }
                default:
                    break;
            }

        }

        ;
    };

    /**
     * 开始支付
     */
    public void startPay() {
        // // 订单
        // String m_OrderInfo = getOrderInfo();
        // // 对订单做RSA 签名
        // String m_Sign = sign(m_OrderInfo);
        // if (m_Sign == null) {
        // Toast.makeText(m_Context, "支付参数填写有误", Toast.LENGTH_SHORT).show();
        // return;
        // }
        // try {
        // // 仅需对sign 做URL编码
        // m_Sign = URLEncoder.encode(m_Sign, "UTF-8");
        // } catch (UnsupportedEncodingException e) {
        // e.printStackTrace();
        // }
        // 完整的符合支付宝参数规范的订单信息
        // final String payInfo = m_OrderInfo + "&sign=\"" + m_Sign + "\"&"
        // + getSignType();
        Runnable payRunnable = new Runnable() {
            @Override
            public void run() {
                // 构造PayTask 对象
                PayTask alipay = new PayTask((Activity) m_Context);
                // 调用支付接口，获取支付结果
                Map<String, String> result = alipay.payV2(payInfo, true);
                Message msg = new Message();
                msg.what = SDK_PAY_FLAG;
                msg.obj = result;
                mHandler.sendMessage(msg);
            }
        };
        // 必须异步调用
        Thread payThread = new Thread(payRunnable);
        payThread.start();
    }

    // /**
    // * 获取订单信息
    // *
    // * @return 信息拼接字符串
    // */
    // private String getOrderInfo() {
    // StringBuilder m_StrBuilder = new StringBuilder();
    // // 签约合作者身份ID
    // m_StrBuilder.append(String.format("partner=\"%s\"",
    // m_AliPayParams.getPartner()));
    // // 签约卖家支付宝账号
    // m_StrBuilder.append(String.format("&seller_id=\"%s\"",
    // m_AliPayParams.getSellerId()));
    // // 商户网站唯一订单号
    // m_StrBuilder.append(String.format("&out_trade_no=\"%s\"",
    // m_AliPayParams.getOrderNo()));
    // // 商品名称
    // m_StrBuilder.append(String.format("&subject=\"%s\"",
    // m_AliPayParams.getProductName()));
    // // 商品详情
    // m_StrBuilder.append(String.format("&body=\"%s\"",
    // m_AliPayParams.getDesc()));
    // // 商品金额
    // m_StrBuilder.append(String.format("&total_fee=\"%s\"",
    // m_AliPayParams.getTotalMoney()));
    // // 服务器异步通知页面路径
    // m_StrBuilder.append(String.format("&notify_url=\"%s\"",
    // m_AliPayParams.getNotifyUrl()));
    // // 服务接口名称， 固定值
    // m_StrBuilder.append(String.format("&service=\"%s\"",
    // "mobile.securitypay.pay"));
    // // 支付类型， 固定值
    // m_StrBuilder.append(String.format("&payment_type=\"%s\"", "1"));
    // // 参数编码， 固定值
    // m_StrBuilder.append(String.format("&_input_charset=\"%s\"", "utf-8"));
    // // 设置未付款交易的超时时间
    // // 默认30分钟，一旦超时，该笔交易就会自动被关闭。
    // // 取值范围：1m～15d。
    // // m-分钟，h-小时，d-天，1c-当天（无论交易何时创建，都在0点关闭）。
    // // 该参数数值不接受小数点，如1.5h，可转换为90m。
    // m_StrBuilder.append(String.format("&it_b_pay=\"%s\"", "30m"));
    // // 支付宝处理完请求后，当前页面跳转到商户指定页面的路径，可空
    // if (!StringHelper.isEmpty(m_AliPayParams.getReturnUrl())) {
    // m_StrBuilder.append(String.format("&return_url=\"%s\"",
    // m_AliPayParams.getReturnUrl()));
    // }
    // return m_StrBuilder.toString();
    // }
    //
    // /**
    // * 签名
    // *
    // * @param content
    // * 已拼接的订单信息
    // * @return 签名字符串
    // */
    // public String sign(String content) {
    // try {
    // PKCS8EncodedKeySpec priPKCS8 = new PKCS8EncodedKeySpec(
    // Base64Helper.decode(m_AliPayParams.getRSAPRIVATE()));
    // KeyFactory keyf = KeyFactory.getInstance(ALGORITHM);
    // PrivateKey priKey = keyf.generatePrivate(priPKCS8);
    // java.security.Signature signature = java.security.Signature
    // .getInstance(SIGN_ALGORITHMS);
    // signature.initSign(priKey);
    // signature.update(content.getBytes(DEFAULT_CHARSET));
    // byte[] signed = signature.sign();
    // return Base64Helper.encode(signed);
    // } catch (Exception e) {
    // e.printStackTrace();
    // }
    // return null;
    // }
    //
    // /**
    // * get the sign type we use. 获取签名方式
    // *
    // */
    // private String getSignType() {
    // return "sign_type=\"RSA\"";
    // }
    //
    // /**
    // * 支付宝支付参数Model
    // *
    // * @author WangPeng
    // *
    // */
    // public static class AliPayParams extends PayParams {
    // private String partner;
    //
    // /** 设置支付宝合作者身份ID */
    // public void setPartner(String p_Partner) {
    // this.partner = p_Partner;
    // }
    //
    // /** 获取支付宝合作者身份ID */
    // public String getPartner() {
    // return this.partner;
    // }
    //
    // private String seller_id;
    //
    // /** 设置卖家支付宝账号 */
    // public void setSellerId(String p_SellerId) {
    // this.seller_id = p_SellerId;
    // }
    //
    // /** 获取卖家支付宝账号 */
    // public String getSellerId() {
    // return this.seller_id;
    // }
    //
    // private String return_url = "";
    //
    // /** 设置支付宝处理完请求后，当前页面跳转到商户指定页面的路径 */
    // public void setReturnUrl(String p_ReturnUrl) {
    // this.return_url = p_ReturnUrl;
    // }
    //
    // /** 获取支付宝处理完请求后，当前页面跳转到商户指定页面的路径 */
    // public String getReturnUrl() {
    // return this.return_url;
    // }
    //
    // private String rsa_private;
    //
    // /** 设置商户私钥 */
    // public void setRSAPRIVATE(String p_RSAPRIVATE) {
    // this.rsa_private = p_RSAPRIVATE;
    // }
    //
    // /** 获取商户私钥 */
    // public String getRSAPRIVATE() {
    // return this.rsa_private;
    // }
    //
    // private String rsa_public;
    //
    // /** 设置支付宝公钥 */
    // public void setRSAPUBLIC(String p_RSAPUBLIC) {
    // this.rsa_public = p_RSAPUBLIC;
    // }
    //
    // /** 获取支付宝公钥 */
    // public String getRSAPUBLIC() {
    // return this.rsa_public;
    // }
    // }
}
