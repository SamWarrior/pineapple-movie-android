package com.android.pay;

import android.content.Context;

import com.blankj.utilcode.util.ToastUtils;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.samnative.pineapple.value.Config;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 微信支付帮助类
 *
 * @author WangPeng
 */
public class WXPayHelper {

    /**
     * 上下文
     */
    private Context m_Context;
    // /** 支付需要的参数 */
    // private WXPayParams m_WXPayParams;
    /**
     * 后台传来的支付参数集
     */
    private String jsonPay;
    /**
     * 微信支付参数集
     */
    private PayReq m_PayReq;
    /**
     * 支付对象
     */
    final IWXAPI m_WXApi;
    // /** 生成预付单结果 */
    // Map<String, String> m_UnifiedOrderResult;
    /**
     * 回调
     */
    public static IPayCallBack m_IPayCallBack;

    /**
     * 微信支付帮助类构造函数
     *
     * @param p_Context      上下文对象
     * @param p_WXPayParams  微信支付参数
     * @param p_IPayCallBack 微信结果回调函数
     */
    public WXPayHelper(Context p_Context, String jsonPay,
                       IPayCallBack p_IPayCallBack) {
        this.m_Context = p_Context;
        this.jsonPay = jsonPay;
        m_IPayCallBack = p_IPayCallBack;
        m_PayReq = new PayReq();
        m_WXApi = WXAPIFactory.createWXAPI(p_Context, null);
        m_WXApi.registerApp(Config.wechatId);
    }

    // /**
    // * 微信支付帮助类构造函数
    // *
    // * @param p_Context
    // * 上下文对象
    // * @param p_WXPayParams
    // * 微信支付参数
    // * @param p_IPayCallBack
    // * 微信结果回调函数
    // * */
    // public WXPayHelper(Context p_Context, WXPayParams p_WXPayParams,
    // IPayCallBack p_IPayCallBack) {
    // m_Context = p_Context;
    // m_IPayCallBack = p_IPayCallBack;
    // m_WXPayParams = p_WXPayParams;
    // m_PayReq = new PayReq();
    // m_WXApi = WXAPIFactory.createWXAPI(p_Context, null);
    // m_WXApi.registerApp(ConstsObject.WETCHAT_APP_ID);
    // }

    /**
     * 获取微信支付回调函数
     *
     * @return
     */
    public static IPayCallBack getIPayCallBack() {
        return m_IPayCallBack;
    }

    // /**
    // * 发起获取预支付订单id任务请求
    // */
    // public void startPay() {
    // GetPrepayIdTask m_GetPrepayId = new GetPrepayIdTask();
    // m_GetPrepayId.execute();
    // }
    //
    // /**
    // * 获取预支付订单id任务
    // *
    // * @author WangPeng
    // *
    // */
    // private class GetPrepayIdTask extends
    // AsyncTask<Void, Void, Map<String, String>> {
    //
    // @Override
    // protected void onPreExecute() {
    //
    // }
    //
    // @Override
    // protected void onPostExecute(Map<String, String> result) {
    // m_UnifiedOrderResult = result;
    // if (result.containsKey("return_code")) {
    // if (TextUtils.equals(result.get("return_code"), "SUCCESS")) {
    // genPayReq();
    // sendPayReq();
    // } else {
    // Toast.makeText(m_Context, result.get("return_msg"),
    // Toast.LENGTH_SHORT).show();
    // }
    // }
    //
    // }
    //
    // @Override
    // protected void onCancelled() {
    // super.onCancelled();
    // }
    //
    // @Override
    // protected Map<String, String> doInBackground(Void... params) {
    // String url = String
    // .format("https://api.mch.weixin.qq.com/pay/unifiedorder");
    // String entity = genProductArgs();
    // Log.e("orion", entity);
    // byte[] buf = UtilHelper.httpPost(url, entity);
    // String content = new String(buf);
    // Log.e("orion", content);
    // Map<String, String> xml = UtilHelper.decodeXml(content);
    // return xml;
    // }
    // }
    //
    // /**
    // * 设置微信支付参数集
    // */
    // private void genPayReq() {
    // m_PayReq.appId = ConstsObject.WETCHAT_APP_ID;
    // m_PayReq.partnerId = m_WXPayParams.getMchId();
    // m_PayReq.prepayId = m_UnifiedOrderResult.get("prepay_id");
    // m_PayReq.packageValue = "prepay_id=" +
    // m_UnifiedOrderResult.get("prepay_id");
    // m_PayReq.nonceStr = genNonceStr();
    // m_PayReq.timeStamp = String.valueOf(genTimeStamp());
    // // 设置需要加签的参数
    // List<NameValuePair> signParams = new LinkedList<NameValuePair>();
    // signParams.add(new BasicNameValuePair("appid", m_PayReq.appId));
    // signParams.add(new BasicNameValuePair("noncestr", m_PayReq.nonceStr));
    // signParams.add(new BasicNameValuePair("package", m_PayReq.packageValue));
    // signParams.add(new BasicNameValuePair("partnerid", m_PayReq.partnerId));
    // signParams.add(new BasicNameValuePair("prepayid", m_PayReq.prepayId));
    // signParams.add(new BasicNameValuePair("timestamp", m_PayReq.timeStamp));
    // // 设置签名
    // m_PayReq.sign = genAppSign(signParams);
    // Log.e("orion", signParams.toString());
    //
    // }

    /**
     * 设置微信支付参数集
     */
    private void genPayReq() {
        try {
            JSONObject json = new JSONObject(jsonPay);
            m_PayReq.appId = Config.wechatId;
            m_PayReq.partnerId = json.optString("partnerid");
            m_PayReq.prepayId = json.optString("prepayid");
            m_PayReq.packageValue = json.optString("package");
            m_PayReq.nonceStr = json.optString("noncestr");
            m_PayReq.timeStamp = json.optString("timestamp");
            m_PayReq.sign = json.optString("sign");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ToastUtils.showShort("微信支付参数错误");
        }
    }

    /**
     * 发送微信支付请求
     */
    public void sendPayReq() {
        genPayReq();
        m_WXApi.registerApp(Config.wechatId);
        m_WXApi.sendReq(m_PayReq);
    }

    // /**
    // * 获取产品参数
    // *
    // * @return
    // */
    // private String genProductArgs() {
    // StringBuffer xml = new StringBuffer();
    // try {
    // String nonceStr = genNonceStr();
    // xml.append("</xml>");
    // List<NameValuePair> packageParams = new LinkedList<NameValuePair>();
    // packageParams.add(new BasicNameValuePair("appid",
    // ConstsObject.WETCHAT_APP_ID));
    // packageParams.add(new BasicNameValuePair("body", m_WXPayParams
    // .getProductName()));
    // packageParams.add(new BasicNameValuePair("mch_id", m_WXPayParams
    // .getMchId()));
    // packageParams.add(new BasicNameValuePair("nonce_str", nonceStr));
    // packageParams.add(new BasicNameValuePair("notify_url",
    // m_WXPayParams.getNotifyUrl()));
    // packageParams.add(new BasicNameValuePair("out_trade_no",
    // m_WXPayParams.getOrderNo()));
    // // packageParams.add(new BasicNameValuePair("attach",
    // // m_WXPayParams.getAttach()));
    // packageParams.add(new BasicNameValuePair("spbill_create_ip",
    // "127.0.0.1"));
    // packageParams.add(new BasicNameValuePair("total_fee", m_WXPayParams
    // .getTotalMoney()));
    // packageParams.add(new BasicNameValuePair("trade_type", "APP"));
    // String sign = genPackageSign(packageParams);
    // packageParams.add(new BasicNameValuePair("sign", sign));
    // String xmlstring = UtilHelper.toXml(packageParams);
    // return new String(xmlstring.toString().getBytes(), "ISO8859-1");// 解决中文乱码
    // } catch (Exception e) {
    // e.printStackTrace();
    // return null;
    // }
    //
    // }
    //
    // /**
    // * 获取随数
    // *
    // * @return
    // */
    // private String genNonceStr() {
    // Random random = new Random();
    // return MD5Helper.getMessageDigest(String.valueOf(random.nextInt(10000))
    // .getBytes());
    // }
    //
    // /**
    // * 获取时间戳
    // *
    // * @return
    // */
    // private long genTimeStamp() {
    // return System.currentTimeMillis() / 1000;
    // }
    //
    // /**
    // * 生成包签名
    // */
    // private String genPackageSign(List<NameValuePair> params) {
    // StringBuilder sb = new StringBuilder();
    // for (int i = 0; i < params.size(); i++) {
    // sb.append(params.get(i).getName());
    // sb.append('=');
    // sb.append(params.get(i).getValue());
    // sb.append('&');
    // }
    // sb.append("key=");
    // sb.append(m_WXPayParams.getApiKey());
    // String packageSign = MD5Helper.getMessageDigest(
    // sb.toString().getBytes()).toUpperCase();
    // Log.e("orion", packageSign);
    // return packageSign;
    // }
    //
    // /**
    // * 获取APP签名
    // *
    // * @param params
    // * @return
    // */
    // private String genAppSign(List<NameValuePair> params) {
    // StringBuilder sb = new StringBuilder();
    // for (int i = 0; i < params.size(); i++) {
    // sb.append(params.get(i).getName());
    // sb.append('=');
    // sb.append(params.get(i).getValue());
    // sb.append('&');
    // }
    // sb.append("key=");
    // sb.append(m_WXPayParams.getApiKey());
    // String appSign = MD5Helper.getMessageDigest(sb.toString().getBytes());
    // Log.e("orion", appSign);
    // return appSign;
    // }
    //
    // /**
    // * 获取商户订单号
    // *
    // * @return
    // */
    // private String genOutTradNo() {
    // Random random = new Random();
    // return MD5Helper.getMessageDigest(String.valueOf(random.nextInt(10000))
    // .getBytes());
    // }
    //
    // /**
    // * 微信支付参数Model
    // *
    // * @author WangPeng
    // *
    // */
    // public static class WXPayParams extends PayParams {
    // private String mchId;
    //
    // /** 设置微信支付商户ID */
    // public void setMchId(String p_MchId) {
    // this.mchId = p_MchId;
    // }
    //
    // /** 获取微信支付商户ID */
    // public String getMchId() {
    // return this.mchId;
    // }
    //
    // private String apiKey;
    //
    // /** 设置微信支付商户API密钥 */
    // public void setApiKey(String p_ApiKey) {
    // this.apiKey = p_ApiKey;
    // }
    //
    // /** 获取微信支付商户API密钥 */
    // public String getApiKey() {
    // return this.apiKey;
    // }
    // }
}
