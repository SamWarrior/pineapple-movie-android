package com.alibaba.sdk.android.oss;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;

import com.alibaba.sdk.android.oss.callback.OSSCompletedCallback;
import com.alibaba.sdk.android.oss.callback.OSSProgressCallback;
import com.alibaba.sdk.android.oss.model.PutObjectRequest;
import com.alibaba.sdk.android.oss.model.PutObjectResult;
import com.blankj.utilcode.util.LogUtils;
import com.shashank.sony.fancytoastlib.FancyToast;
import com.samnative.pineapple.entity.AliPostImginfoModel;
import com.samnative.pineapple.main.R;
import com.samnative.pineapple.widget.LoadingDialog;

import java.io.File;

/**
 * 文件上传工具类
 * Created by Administrator on 2019/5/27.
 */
public class PutFileUtils {

    /**
     * 发起图片上传
     *
     * @param activity activity对象
     * @param aliModel 文件上传参数信息
     * @param file     要上传的文件
     * @param suffix   文件后缀
     * @param callback 结果回调 200成功 400失败
     */
    public static void putFile(Activity activity, AliPostImginfoModel aliModel, File file, String suffix, Handler.Callback callback) {
        // TODO Auto-generated method stub
        LoadingDialog.getInstance(activity).setMessage(activity.getString(R.string.putfileing)).show();
        new PutObjectSamples(activity, aliModel.getAccessid(), aliModel.getDir() + suffix, file.getPath()).asyncPutObjectFromLocalFile(
                new OSSProgressCallback<PutObjectRequest>() {

                    @Override
                    public void onProgress(
                            PutObjectRequest request,
                            long currentSize, long totalSize) {
                        // TODO Auto-generated method stub
                        LogUtils.e("上传进度 currentSize: "
                                + currentSize + " totalSize: "
                                + totalSize);
                    }
                },
                new OSSCompletedCallback<PutObjectRequest, PutObjectResult>() {
                    @Override
                    public void onSuccess(
                            PutObjectRequest request,
                            PutObjectResult result) {
                        LogUtils.e("上传成功");
                        activity.runOnUiThread(new Runnable() {
                            public void run() {
                                Message msg = Message.obtain();
                                msg.what = 200;
                                msg.obj = aliModel;
                                callback.handleMessage(msg);
                                try {
                                    LoadingDialog.dismissDialog();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }

                    @Override
                    public void onFailure(
                            PutObjectRequest request,
                            ClientException clientExcepion,
                            ServiceException serviceException) {
                        LogUtils.e("上传失败");
                        activity.runOnUiThread(new Runnable() {
                            public void run() {
                                Message msg = Message.obtain();
                                msg.what = 400;
                                msg.obj = aliModel;
                                callback.handleMessage(msg);
                                try {
                                    LoadingDialog.dismissDialog();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                // 请求异常
                                if (clientExcepion != null) {
                                    // 本地异常如网络异常等
                                    clientExcepion.printStackTrace();

                                    FancyToast.showToast(activity,"上传失败,本地文件异常", FancyToast.ERROR,false);
                                }
                                if (serviceException != null) {
                                    // 服务异常
                                    serviceException.printStackTrace();

                                    FancyToast.showToast(activity,"上传失败,服务器异常!", FancyToast.ERROR,false);
                                }
                            }
                        });

                    }
                }

        );

    }
}
